// CHECK IF A CHARACTER IS PRESENT IN A GIVEN SENTENCE

var sentence = 'It s peanut butter jelly time'
var target = 't';

function findChar(sentence,target){

    var chars = sentence.split("");
  
    var filteredChar = chars.filter(function(char){
        return char === target;

    })

    return filteredChar;

}

console.log(findChar(sentence,target));