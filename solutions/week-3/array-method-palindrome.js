// array method to CHECK IF A GIVEN WORD IS A PALINDROME
var word = 'racecar';

console.log(checkPalindrome(word));
console.log(checkPalindrome('banana'));
console.log(checkPalindrome('ana'));


function checkPalindrome(word){

    return word.split("").reverse().join() == word.split("");
    
}