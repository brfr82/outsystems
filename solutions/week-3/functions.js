// atempt 3: with operators

var fnMultiply = multiply(10,5);
var fnDivide = divide(30,3);

function multiply(x,y) {
	return x*y ;
}

function divide( x,y) {
	return x/y;
}

function sumTimes(fn, times) {
	var result = 0
	for (var i = 0; i < times; i++) {
		result = result  + fn; 
	}
	return result
}

console.log(sumTimes(fnMultiply, 3));
console.log(sumTimes( fnDivide, 5));


// attempt 2:

var fnHello = sayHello('John');
var fnGoodBye = sayGoodBye('Paul');

function sayHello(person) {
	return 'Hello '+person;
}

function sayGoodBye(person) {
	return 'Goodbye '+ person;
}

function callTimes(fn, times) {
	var result = [] 
	for (var i = 0; i < times; i++) {
		result.push(fn); 
	}
	return result
}

console.log(callTimes(fnHello, 2));
console.log(callTimes( fnGoodBye, 3));



// attempt 2:

var fnSumTwoNumbers = sumTwoNumbers(5,5);
var fnDivide = divide(5,3);

function sumTwoNumbers(x,y) {
	return x+y ;
}

function divide( x,y) {
	return x*y;
}

function callTimes(fn, times) {
	var result = [] 
	for (var i = 0; i < times; i++) {
		result.push(fn); 
	}
	return result
}

console.log(callTimes(fnSumTwoNumbers, 2));
console.log(callTimes( var fnDivide, 3));


