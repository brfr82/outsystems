// WIN AT FIZZBUZZ  

var input = [3, 5, 10, 11, 12, 15, 16]
var fizz = 3
var buzz = 5

console.log(fizzBuzz(input, fizz, buzz))

function fizzBuzz(input, fizz, buzz) {

  var output = input.reduce(function (acc, number) { 

    //switch + return -> does not seem to work inside reduce
    
    if (number % fizz == 0 && number % buzz != 0) {
      return acc.concat('fizz');
    }

    if (number % buzz == 0 && number % fizz != 0) {
      return acc.concat('buzz');
    }

    if (number % fizz == 0 && number % buzz == 0) {
      return acc.concat('fizzbuzz');
    }

    if (number % fizz != 0 && number % buzz != 0) {
      return acc.concat(number);
    }


  }, [])

  return output;

}

