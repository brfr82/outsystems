// COUNT THE NUMBER OF VOWELS IN A GIVEN SENTENCE

var sentence = 'AAsupercalifragilisticexpialidocious'

function countVowels(sentence) {

    return sentence.split("").reduce(function (acc, char) {

        if ('aeiouAEIOU'.includes(char)) {
            acc++;

        }
        return acc;

    }, 0)
}

console.log(countVowels(sentence));