// FIND THE CLOSEST NEIGHBOURS IN A GIVEN LIST

function findClosestNeighbours(numbers) {

    var mapAbsDifferences = numbers.map(function (currentValue, index, array) {

        if (index == 0) {
            return Infinity;
        }

        else {
            return Math.abs(array[index - 1] - array[index]);
        }
    });


    var smallestDifference = mapAbsDifferences.slice().sort(function (a, b) {  // slice().sort will not affect the original array!
        return a - b;
    })[0]

    return [numbers[mapAbsDifferences.indexOf(smallestDifference) - 1], numbers[mapAbsDifferences.indexOf(smallestDifference)]];

}

console.log(findClosestNeighbours([10, 15, 45, 42, 50])); // [ 45, 42 ]
console.log(findClosestNeighbours([0, 11, 20, 22, 18, -2, -3])); // [ -2, -3  ]

