function countNumberOfBombs(grid) {

    var numberOfBombs = grid
        .reduce(function (prev, next) {
            return prev.concat(next);
        })
        .reduce(function (accumulator, actualValue) {
            return accumulator + actualValue;

        }, 0)

    return numberOfBombs;
}

var grid = [
    [0, 1, 1],
    [1, 0, 0],
    [0, 0, 1],
];

var anotherGrid = [
    [0, 1, 1, 1],
    [1, 0, 0, 1],
    [0, 0, 1, 0],
    [0, 1, 1, 1],
];

console.log(countNumberOfBombs(grid)); // 4
console.log(countNumberOfBombs(anotherGrid)); // 9

