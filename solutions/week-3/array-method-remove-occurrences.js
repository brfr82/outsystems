// REMOVE ALL OCCURRENCES OF A NUMBER FROM A LIST

var list = [1, 5, 24, -1, 8, 5];
var target = 5;

function removeOcurrences(list, target) {

    var cleanList = list.filter(function (number) {

        return number !== target;
    })

    return cleanList;

}

console.log(removeOcurrences(list, target));

