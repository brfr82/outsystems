// FIND THE DUPLICATED CHARACTERS IN A GIVEN SENTENCE

var sentence = 'And if you did not know, now you know'
console.log(findDuplicateChars(sentence));

function findDuplicateChars(sentence) {

    chars = sentence.trim().split("").sort();

    duplicatedChars = chars.filter(function (char, index) {
        return chars.indexOf(char) !== index;
    });

    uniqueDuplicatedChars = duplicatedChars.filter(function (char, index) {
        return duplicatedChars.indexOf(char) === index;
    });

    return uniqueDuplicatedChars.join("").trim();
}

