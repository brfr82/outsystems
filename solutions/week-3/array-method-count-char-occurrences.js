function countCharOccurrences(sentence) {


    var occurrencesByLetter = sentence.split("").reduce(function (acc, char) {
        if (typeof acc[char] == 'undefined') {
          acc[char] = 1;
        } else {
          acc[char] += 1;
        }
      
        return acc;
      }, {});



    return occurrencesByLetter;
}

console.log(countCharOccurrences("banana")); // { b: 1, a: 3, n: 2 }
console.log(countCharOccurrences("potato")); // { p: 1, o: 2, t: 2, a: 1 }
