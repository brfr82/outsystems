
// FIND THE FIRST NON-CONSECUTIVE NUMBER IN A GIVEN LIST OF (at least 2) NUMBERS

var listA = [11, 13, 13, 17, 16]; //17
var listB = [3, 1, 0, -1, -3]; // [2, -3]
var listC = [-2, -1, 0, 4, 3]; // 4
var listD = [1, 2, 3, 4, 3, 7]; // 7
var listE = [-1, -2, -3, -4, -3, -5, -6]; //  -5
var listF = [-1, -2, -8, -4, -6, -7]; // -6


function checkNonConsecutives(numbers) {

    var nonConsecutives = numbers.map(function (currentValue, index, array) {  // replace with reduce with:  if (Math.abs(array[index - 1] - array[index]) > 1)

        if (index == 0) {
            return "ok"
        }

        else {
            if (Math.abs(array[index - 1] - array[index]) > 1){
                return currentValue;
            }
            else{
                return "ok"
            }
            
        }
    });

    return nonConsecutives;


}

console.log(checkNonConsecutives(listA));
console.log(checkNonConsecutives(listB));
console.log(checkNonConsecutives(listC));
console.log(checkNonConsecutives(listD));
console.log(checkNonConsecutives(listE));
console.log(checkNonConsecutives(listF));