var fullSentence = 'This is a potato and a salad another potato';
var forbiddenWords = ['potato', 'salad'];

console.log(filterForbiddenWords(fullSentence, forbiddenWords));

function filterForbiddenWords(fullSentence, forbiddenWords) {

    // compact version ---return fullSentence.split(" ").filter ( ... ).join(" ")

    var words = fullSentence.split(" ");
   
    var newSentence = words.filter(function(word){

        if(!forbiddenWords.includes(word)){
            return word;
        }                    
    })

    return newSentence.join(" ");
}


