
var queue = [

    { name: "John", age: 36 },
    { name: "Alice", age: 24 },
    { name: "Anna", age: 14 },
    { name: "Rachel", age: 28 },
    { name: "Alex", age: 50 },
    { name: "Phil", age: 18 }
];


console.log(listGuests(filterAdults(queue, 18)));

function filterAdults(people, minAge) {

    var adults = people.filter(function (person) {

        return person.age >= minAge
    })

    return adults;
}

function listGuests(list) {
    var guests = list.map(function (person) {
        return person.name
    })

    return guests;
}


