
var deathList = [

    { description: 'VERNITA GREEN', importance: 2 },
    { description: 'BILL', importance: 5 },
    { description: 'O REN ISHI', importance: 1 },
    { description: 'ELLE DRIVER', importance: 4 },
    { description: 'BUDD', importance: 3 }
];


console.log(sortByImportance(deathList, 'asc'));
console.log(sortByImportance(deathList, 'desc'));

function sortByImportance(list, order) {

    var sortedList = list.slice().sort(function (a, b) {  //  keep original order (as in closest-neighbours.js), corrected with slice()

        if (order === 'asc') {

            return a.importance - b.importance
        }

        if (order === 'desc') {

            return b.importance - a.importance

        }

    })
        .map(function (person) {
            return person.description
        })

    return sortedList;

}