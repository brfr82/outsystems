var expect = require('expect.js');
var errorAnswers = require('../questions/02-errors');

describe('errors', function () {

    it('you should be able to catch an exception and return the error message when calling a function', function () {

        var errorMessage = 'an error message';
        var called = false;
        var result = errorAnswers.callIt(function () {
            called = true;
            throw new Error(errorMessage);
        });

        expect(result).to.equal(errorMessage);
        expect(called).to.be(true);

        called = false;
        result = errorAnswers.callIt(function () {
            called = true;
            return 'ok';
        });

        expect(result).to.equal('ok');
        expect(called).to.be(true);
    });

    it('you should be able to throw an error object with an enclosed message', function () {

        expect(errorAnswers.assertEqual('abc', 'abc')).to.equal(true);
        expect(errorAnswers.assertEqual(1, 1)).to.equal(true);
        expect(errorAnswers.assertEqual(0, 0)).to.equal(true);

        expect(errorAnswers.assertEqual).withArgs('abc', 'cba').to.throwException();
        expect(errorAnswers.assertEqual).withArgs(0, '0').to.throwException();
        expect(errorAnswers.assertEqual).withArgs(0, 1).to.throwException();

        expect(errorAnswers.assertEqual).withArgs(null, undefined).to.throwException(function (error) {
            expect(error).to.be.an(Error);
            expect(error.message).to.be.a('string');
        });
    });

});
