var expect = require('expect.js');
var functionsAnswers = require('../questions/01-functions');

describe('functions', function () {

    var say = function (greeting, name, punctuation) {
        called = true;
        return greeting + ', ' + name + (punctuation || '!');
    };

    beforeEach(function () {
        called = false;
    });

    it('you should be able to return a string prefixer', function () {
        expect(functionsAnswers.stringPrefixer('Hello, ')('world')).to.equal('Hello, world');
        expect(functionsAnswers.stringPrefixer('Hey, ')('can I have a function?')).to.equal('Hey, can I have a function?');
    });

    it('you should be able to use closures to create an array of result producing functions', function () {

        var arr = [Math.random(), Math.random(), Math.random(), Math.random()];
        var square = function (x) {
            return x * x;
        };

        var funcs = functionsAnswers.makeResultFunctions(arr, square);
        expect(funcs).to.have.length(arr.length);

        arr.forEach(function (value, index) {
            expect(funcs[index]()).to.equal(square(value));
        });
    });

    it('you should be able to create a partial function', function () {
        var partial = functionsAnswers.createWrapperFunction(say, 'Hello', 'AC');
        expect(partial('!!!')).to.equal('Hello, AC!!!');
        expect(called).to.be(true);
    });

});
