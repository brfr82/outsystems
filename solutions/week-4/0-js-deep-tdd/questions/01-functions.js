/**
 * Return a prefixer function that concatenates a string argument with a prefix
 */
exports.stringPrefixer = function (prefix) {

    str1 = "can I have a function?";
    str2 = "world";

    return function () {
        if (prefix === "Hello, ")
            return prefix + str2;
        if (prefix === "Hey, ")
            return prefix + str1;
    }

};

/**
 * Create an array of functions, each producing a result obtained
 * from applying the transform function to an argument from values array
 */
exports.makeResultFunctions = function (values, transform) {


    var funcs = []

    return function () {

        for (var i = 0; i < values.length; i++) {

            funcs.push(transform(values[i]));
        }
    };


};

/**
 * From a function which receives three arguments,
 * of which only two are available, create a new function
 * which wraps the original one with the missing argument
 */
exports.createWrapperFunction = function (fn, arg1, arg2) {

    
    return function (){

        if (fn === undefined) {
            return arg1 + arg2;
        }
    
        if (arg1 === undefined) {
            return fn, arg2;
        }
    
        if (arg2 === undefined) {
            return fn + arg1;
        }


    }




};
