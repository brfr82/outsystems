const box2 = document.getElementsByClassName("box2");
const box1 = document.getElementsByClassName("box1");
const h1 = document.getElementsByTagName("h1");
const button = document.getElementsByTagName("button");
const percentageComplete = 0

button[0].addEventListener("click", start);  // prevent re-clicking before finish

function start() {

    function restart() {

        box2[0].style.width = "50vw";
        box1[0].removeChild(h1[0]);
        box1[0].style.justifyContent = "right"
        button[0].textContent = "pause";
        button[0].addEventListener("click", pause);

        function pause() {

            button[0].textContent = "continue";
            button[0].addEventListener("click", continuing);

            function continuing(){
                // ToDo
	    };

        };
    };

    restart();

    for (let i = percentageComplete; i < 100; i++) {

        setTimeout(function () {

            box2[0].style.width = (99 - i) / 2 + "vw";
            console.log(box2[0].style.width);

            if (i === 99) {
                box1[0].style.justifyContent = "center"
                box1[0].insertAdjacentHTML("afterbegin", "<h1 color=>completed!<h1>")
                button[0].textContent = "restart"; // Todo
            };

        }, 50 * i);
    };
};




