
const http = require('http');
const fs = require('fs');
const PORT = 8080;
const server = http.createServer(handler)
const mime = require('mime-types')

server.listen(PORT);
console.log('server listening @port' + PORT);

const routes = [{ method: 'GET', path: /.*/, handler: handleGetRequest },
{ method: 'POST', path: '/', handler: handlePostRequest }
]

function getRouteHandler(method, url) {
    return routes.find((route) => (route.method === method && url.match(route.path))).handler;
}

function handleGetRequest(request, response) {

    let url = request.url;
    let method = request.method;
    console.log('REQUEST URL:' + url + '  REQUEST METHOD:' + method);
    const filePath = url.slice(1);

    // let contentTypeObj = { mp4: "video/mp4", mp3: "audio/mpeg", png: "image/png", html: "text/html", css:"text/css", js:"text/javascript"};
    let mimeType = mime.lookup(url.split('.').pop());
    console.log(mimeType);

    if (url !== '/index.html' && url !== '/script.js' && url !== '/style.css' && url!=='/dom-manipulation.js' && url!=='/chat-messages.txt') { // use includes for more files


        response.writeHead(404);
        response.write('page unavailable');
        response.end();

    }
    else {
        fs.readFile(filePath, function (error, content) {
            // include error handling here

            response.writeHead(200, { 'Content-Type': mimeType });

            response.write(content);
            response.end();
        });
    }
}

function handlePostRequest(request, response) {

    // response 204 
    let url = request.url;
    let method = request.method;
    console.log('REQUEST URL:' + url + '  REQUEST METHOD:' + method);
    const filePath = url.slice(1);

    // let contentTypeObj = { mp4: "video/mp4", mp3: "audio/mpeg", png: "image/png", html: "text/html", css:"text/css", js:"text/javascript"};
    let mimeType = mime.lookup(url.split('.').pop());
    console.log(mimeType);

    let payload = ""
    request.on('data', function (chunk) { payload = payload.concat(chunk) })
    console.log("post handler invoked");
    request.on("end", function () {

        const message = "[" + new Date().toLocaleTimeString([], { date: "YY/MM/DD", hour: '2-digit', minute: '2-digit' })
            + "]" + " " + payload.replace(/=/g, " ").replace("&", " --> ").replace(" ", ": ")
    
    
        const messages = { message } // push more messages to object
        JSON.stringify(messages);
        console.log(messages);

        response.write(

            JSON.stringify(messages)

            // ToDO: How TO DOM manipulation server side?
        )

        console.log("[" + new Date().toLocaleTimeString([], { date: "YY/MM/DD", hour: '2-digit', minute: '2-digit' })
            + "]" + " " + payload.replace(/=/g, " ").replace("&", " --> ").replace(" ", ": "))
        response.writeHead(304, { Location: "/index.html" });


        const data = JSON.stringify(messages);
        const filePath ="chat-messages.txt";
        
        
         const appendPromise = (filePath,data) => {  
            return new Promise((resolve, reject) => {
                
              fs.appendFile(filePath, data, (err) => {      
                
        
                if (err) return reject(err)        
                
                resolve(data)
              })
            })
          }        
        
          appendPromise(filePath,data)                   
          .then(data => {`${data} written in file ${filePath}`})
          .catch(err => {console.log(err)}) 

        response.end();
    })

}


function handler(request, response) {

    const { method, url } = request;
    const routeHandler = getRouteHandler(method, url);
    routeHandler(request, response);
}




