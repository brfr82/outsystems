
const colors = ["red", "blue", "yellow", "green", "brown", "white"];

const click = document.getElementsByClassName("click");
const button = document.getElementById("button");
const columns = document.getElementsByClassName("columns");

function activateEvents() {
    for (let i = 0; i < click.length; i++) {
        click[i].addEventListener('click', function () {

            color = colors[i];
            console.log(colors[i]);

        });
    }


    for (let i = 0; i < columns.length; i++) {
        columns[i].addEventListener("click", function () {
            columns[i].style.backgroundColor = color;
        });
    }

}

activateEvents();

button.addEventListener("click", resetStyle);

function resetStyle() {

    for (let i = 0; i < columns.length; i++) {

        columns[i].style.backgroundColor = "lavender";
    }

    activateEvents();
}
