// COUNT THE NUMBER OF VOWELS IN A GIVEN SENTENCE

var sentence = 'AAsupercalifragilisticexpialidocious'

const countVowels = (sentence) => sentence.split("").reduce((acc, char) => {
    if ('aeiouAEIOU'.includes(char)) {
        acc++;

    }
    return acc;
}, 0)

console.log(countVowels(sentence));