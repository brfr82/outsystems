// FIND THE DUPLICATED CHARACTERS IN A GIVEN SENTENCE

var sentence = 'And if you did not know, now you know'


const findDuplicateChars = (sentence) => {
    chars = sentence.trim().split("").sort();

    duplicatedChars = chars.filter((char, index) => chars.indexOf(char) !== index);

    uniqueDuplicatedChars = duplicatedChars.filter((char, index) => duplicatedChars.indexOf(char) === index);

    return uniqueDuplicatedChars.join("").trim();
}

console.log(findDuplicateChars(sentence));

