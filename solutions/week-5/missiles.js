/* Our nuclear missile manager system is coded in Node.js.
We are currently testing the system and it seams that the launchAll function does not work as expected.
It should launch 5 missiles each 1 second apart. The current code tries to launch the missile #5 five times...

Can you fix this for us? You know, it's pretty critical code...

Note: There are 5 missiles labeled i which is a number in {0, 1, 2, 3, 4}. The missile i should be launched after i seconds.*/

// check class-repo

// solutions with setInterval require clearInterval !

// https://www.toptal.com/javascript/interview-questions  

// --->> GOOGLE RIGHT WAY TO SEARCH: javascript "interview questions" setTimeout OR closures OR whatever
// --->> GOOGLE RIGHT WAY TO SEARCH: javascript "interview challenges" setTimeout OR closures OR whatever




/* var missiles = [0, 1, 2, 3, 4]

var nrMissiles = missiles.length

launchAll(missiles, nrMissiles);


function launchAll(missiles, nrMissiles) {

    for (let i = 0; nrMissiles; i++) {


        function launchEach() {

            missiles.shift(); // if i!=0

            return console.log(missiles[0]);

        }


        launchEach();  // test no timeout
        //setTimeout(launchEach, 1000);  

    }
} */


/*

What will be the output of the following code:

for (var i = 0; i < 5; i++) {
	setTimeout(function() { console.log(i); }, i * 1000 );
}
Explain your answer. How could the use of closures help here?

Hide answer
answer badge
The code sample shown will not display the values 0, 1, 2, 3, and 4 as might be expected; rather, it will display 5, 5, 5, 5, and 5.

The reason for this is that each function executed within the loop will be executed after the entire loop has completed and all will therefore reference the last value stored in i, which was 5.

Closures can be used to prevent this problem by creating a unique scope for each iteration, storing each unique value of the variable within its scope, as follows:

for (var i = 0; i < 5; i++) {
    (function(x) {
        setTimeout(function() { console.log(x); }, x * 1000 );
    })(i);
}
This will produce the presumably desired result of logging 0, 1, 2, 3, and 4 to the console.

In an ES2015 context, you can simply use let instead of var in the original code:

for (let i = 0; i < 5; i++) {
	setTimeout(function() { console.log(i); }, i * 1000 );
}

*/





/* var arr = [0,1,2,3,4];
var index = 0;
var interval = setInterval(function(){
     console.log(arr[index++]);
     if(index == arr.length){
        clearInterval(interval);
     }
}, 1000) */





