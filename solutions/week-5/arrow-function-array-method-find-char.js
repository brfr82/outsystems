// CHECK IF A CHARACTER IS PRESENT IN A GIVEN SENTENCE

var sentence = 'It s peanut butter jelly time'
var target = 'w';

const findChar = (sentence, target) => {
    var chars = sentence.split("");

    var filteredChar = chars.filter((char) => char === target)

    if (filteredChar[0] != undefined) {
        return target + ": present";

    }
    return target + ": not present";
}

console.log(findChar(sentence, target));