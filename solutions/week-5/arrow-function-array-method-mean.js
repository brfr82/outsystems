// CALCULATE THE MEAN OF A GIVEN LIST OF NUMBERS


var list = [20, 21, 29, 23];

var listB = [-20, 20, -40, 0];

const getMean = (list) => {
    var mean = list.reduce((acc, number) => {
        acc = acc + number / list.length; return acc;
    }, 0);

    return mean;
}

console.log(getMean(list));
console.log(getMean(listB));

