// ADD ALL THE EVEN NUMBERS IN A GIVEN LIST OF NUMBERS 

var listA = [1, 2, 4, 5, 6];
var listB = [-13, 25, -45, 0, -63];

const addEvens = (list) => {
    var evens = list.filter((number) => number % 2 == 0)

    return evens;
}

console.log(addEvens(listA));
console.log(addEvens(listB));




