// array method to CHECK IF A GIVEN WORD IS A PALINDROME
var word = 'racecar';

const checkPalindrome = (word) => word.split("").reverse().join() == word.split("")

console.log(checkPalindrome(word));
console.log(checkPalindrome('banana'));
console.log(checkPalindrome('ana'));


