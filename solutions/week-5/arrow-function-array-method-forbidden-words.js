var fullSentence = 'This is a potato and a salad another potato';
var forbiddenWords = ['potato', 'salad'];

// console.log(filterForbiddenWords(fullSentence, forbiddenWords));  // no call before function

var filterForbiddenWords = (fullSentence, forbiddenWords) => {
    var words = fullSentence.split(" ");

    var newSentence = words.filter((word) => {
        if (!forbiddenWords.includes(word)) {
            return word;   // it does not work, needs a return here
        }
    })

    return newSentence.join(" ");
}

console.log(filterForbiddenWords(fullSentence, forbiddenWords));


