/**
 * Invoke the callback and return the amount of time in miliseconds it took to execute
 */
exports.profileFunc = function (cb) {

      // https://blog.da2k.com.br/2015/01/29/javascript-usando-temporizadores-like-a-ninja/

    function getExecTime(cb){


        var startTime = new Date().getTime();       


        cb();  // executar a função call back


        var endTime = new Date().getTime();


        var execTime   = endTime - startTime;


        return execTime;

    }


    return getExecTime(cb);


};


/**
 * Invoke the async callback with the provided value after some delay
 */
exports.returnWithDelay = function(value, delay, cb) {


    // https://www.w3schools.com/jsref/met_win_settimeout.asp

setTimeout(cb(value), delay);






};

/**
* Invoke the async callback with an error after some delay
*/
exports.failWithDelay = function(delay, cb) {


setTimeout()

try{

    function cb(delay)
    throw new Error("message error")


}
catch{

    console.log(Error.message)
}


