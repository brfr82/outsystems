/**
 * Return a stop watch object with the following API:
 * getTime - return number of seconds elapsed
 * start - start counting time
 * stop - stop counting time
 * reset - sets seconds elapsed to zero
 */
exports.createStopWatch = function () {
    
    var stopWatch = {}
    var startTime = null; 
    var stopTime = null; 
    var running = false;
    fnUserInput();
    
    
    stopWatch = { getTime:getTime(), start:start(),  stop:stop(), reset:reset() } //  ??

    

    getTime(fn);

    function getTime(fn) {

        reset();

        start();

        fnUserInput (); // do something, call a function, wait for user input

        stop();

        return stopTime - startTime;     


    }

    function start() {

        running = true;
        startTime = new Date().getTime();

        return startTime;

    }

    function stop() {

        stopTime = new Date().getTime();
        running = false;    

        return stopTime;

    }

    function reset() {

        startTime = null; 
        stopTime = null; 
        running = false; 

    }

};
