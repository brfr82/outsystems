var expect = require("expect.js");
var sinon = require("sinon");
var callbacksAnswers = require("../questions/01-callbacks");

describe("callbacks", function () {
    var clock;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it("you should understand how to use callbacks synchronously", function () {
        var called = false;
        var testDelay = 10000;

        var result = callbacksAnswers.profileFunc(function () {
            clock.tick(testDelay);
            called = true;
        });

        expect(called).to.equal(true);
        expect(result).to.equal(testDelay);
    });
});
