form.addEventListener('submit', (event) => {
    const login = document.getElementById("login").value;
    console.log(login);
    event.preventDefault();
    console.log("prevented default behaviour");
    searchUsers(login);

});

async function fetchGitHub(login) {

    const api = `https://api.github.com/search/users?q=${login}+in%3Afullname+sort%3Aid&type=Users`;
    const response = await fetch(api);
    // sort by ID is not available in V3 of Github API  

    if (!response.ok) {
        throw new Error(body.message); 
    }   

    return response.json();    
}

async function searchUsers(login) {

    const tbody = document.getElementsByTagName("tbody")[0];
    tbody.textContent="";
    const p = document.getElementsByTagName("p")[0];
    p.textContent="";

    try {
        let data = await fetchGitHub(login);
        const properties = Object.keys(data);  
        const dataObj = JSON.parse(JSON.stringify(data)); 

        tbody.insertAdjacentHTML('beforeend', "<tr><th>Login Name</th><th>ID</th></tr>")
        
        for (let i = 0; i < 10; i++) {

            tbody.insertAdjacentHTML('beforeend', "<tr>" + "<td>" + "#"+(i+1)+": "+ dataObj.items[i].login +  "</td>"
            + "<td>" + dataObj.items[i].id + "</td>" +"</tr>" )
                           
        }
      
    } catch (err) {
        err.message = "Type Error: failed to fetch";
        p.style.color = "red"
        p.insertAdjacentHTML('beforeend',  err.message )
        console.log(err.message);
    }


}
