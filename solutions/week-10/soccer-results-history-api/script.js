// https://www.thesportsdb.com/api.php
// https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=England
// https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League
// https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=${team1}_vs_${team2}
// ToDO: Stats, advanced search/aggregations, add the Logo of teams, etc.


const dropDown1 = document.getElementsByClassName("drop-down1")[0]

dropDown1.addEventListener('mouseover', (event) => {

    event.preventDefault();
    console.log("prevented default behaviour");
    const league1 = document.getElementById("firstLeague").value;
    const list1 = document.getElementById('firstList');
    list1.textContent = "";
    listTeams(league1);

});


const dropDown2 = document.getElementsByClassName("drop-down2")[0]

dropDown2.addEventListener('mouseover', (event) => {

    event.preventDefault();
    console.log("prevented default behaviour");
    const league2 = document.getElementById("secondLeague").value;
    const list2 = document.getElementById('secondList');
    list2.textContent = "";
    listTeams(league2);

});


async function getTeams(league) {

    const api = `https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=${league}`;
    const response = await fetch(api);

    if (!response.ok) {
        throw new Error(body.message);
    }
    return response.json();
}

async function listTeams(league) {

    const tbody = document.getElementsByTagName("tbody")[0];
    tbody.textContent = "";
    const p = document.getElementsByTagName("p")[0];
    p.textContent = "";

    try {
        let data = await getTeams(league);
        const clubs = JSON.parse(JSON.stringify(data));


        const team1 = document.getElementById('firstList').value;
        const team2 = document.getElementById('secondList').value;
        const league1 = document.getElementById("firstLeague").value;
        const league2 = document.getElementById("secondLeague").value;

        console.log(clubs.teams[0].strTeam, " ", league, " ", league1, " ", league2, team1, " ", team2, " ");


        if (team1 === "" && league1 === league) { // create a function: this is repetead task

            const list1 = document.getElementById('firstList');
     

            for (let i = 0; i < Object.keys(clubs.teams).length; i++) {
                let club = clubs.teams[i].strTeam;
                let el = document.createElement("option");
                el.textContent = club;
                el.value = club;
                list1.appendChild(el);

            }
        }

        if (team2 === "" && league2 === league) { // create a function: this is repetead task

            const list2 = document.getElementById('secondList');
 

            for (let i = 0; i < Object.keys(clubs.teams).length; i++) {
                // list 2 should no have selected club in list 1 // check dependable drop down lists


                let club = clubs.teams[i].strTeam;
                let el = document.createElement("option");
                el.textContent = club;
                el.value = club;
                list2.appendChild(el);
            }
        }
    } catch (err) {
        err.message = "Type Error: failed to fetch";
        p.style.color = "red"
        p.insertAdjacentHTML('beforeend', err.message)
        console.log(err.message);
    }
}

form.addEventListener('submit', (event) => {
    const team1 = document.getElementById("firstList").value;
    const team2 = document.getElementById("secondList").value;
    console.log(`${team1}_vs_${team2}`);
    event.preventDefault();
    console.log("prevented default behaviour");
    searchGames(team1, team2);

});

async function fetchGames(team1, team2) {

    const api = `https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=${team1}_vs_${team2}`;
    const response = await fetch(api);

    if (!response.ok) {
        throw new Error(body.message);
    }
    return response.json();
}

async function searchGames(team1, team2) {

    const tbody = document.getElementsByTagName("tbody")[0];
    tbody.textContent = "";
    const p = document.getElementsByTagName("p")[0];
    p.textContent = "";

    try {
        let data = await fetchGames(team1, team2);
        const properties = Object.keys(data);
        const dataObj = JSON.parse(JSON.stringify(data));

        tbody.insertAdjacentHTML('beforeend', "<tr><th>#nr</th><th>Home Team</th><th>Away Team</th><th>Result</th><th>Competion</th><th>Season</th></tr>")

        for (let i = 0; i < Object.keys(dataObj.event).length; i++) {

            tbody.insertAdjacentHTML('beforeend', "<tr>"
                + "<td>" + "#" + (i + 1) + "</td>"
                + "<td>" + dataObj.event[i].strHomeTeam + "</td>"
                + "<td>" + dataObj.event[i].strAwayTeam + "</td>"
                + "<td>" + dataObj.event[i].intHomeScore + ":" + dataObj.event[i].intAwayScore + "</td>"
                + "<td>" + dataObj.event[i].strLeague + "</td>"
                + "<td>" + dataObj.event[i].strSeason + "</td>"
                + "</tr>")
        }

    } catch (err) {
        err.message = "Type Error: failed to fetch";
        p.style.color = "red"
        p.insertAdjacentHTML('beforeend', err.message)
        console.log(err.message);
    }

}

