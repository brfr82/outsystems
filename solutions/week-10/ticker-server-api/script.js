
form.addEventListener('submit', (event) => {
    const EUR = document.getElementById('currency').value;
    console.log(EUR)
    event.preventDefault();
    console.log("prevented default behaviour");
    postData();
});


async function getData() {

    const api = `https://api.exchangeratesapi.io/latest`;
    const response = await fetch(api);

    if (!response.ok) {
        throw new Error(body.message); 
    }   

    return response.json();    
}


async function postData(){
  
        const p = document.getElementsByTagName("p")[0];
        p.textContent = ""

        try {
            let data = await getData();
            const properties = Object.keys(data);  
            const dataObj = JSON.parse(JSON.stringify(data)); 
            const rate = dataObj.rates["USD"];
            const EUR = document.getElementById('currency').value;
            const EURUSD = EUR * rate;
            
            p.insertAdjacentHTML('beforeend', EUR +" EUR = " + EURUSD + " USD") 

          
        } catch (err) {
            err.message = "Type Error: failed to fetch"
            p.insertAdjacentHTML('beforeend', err.message) 
        } 
}

