async function fetchData(api) { // link = > get leagues, get clubs, get player, get results (club Vs club)

    const response = await fetch(api);

    if (!response.ok) {
        throw new Error(body.message);
    }
    return response.json();
}

async function listData(api, objectMain, id, nameOf, description, image, page) {
    try {
        let data = await fetchData(api);

        const dataObj = JSON.parse(JSON.stringify(data, [objectMain, id, nameOf, description, image, page]));
        dataFiltered = dataObj[objectMain]
            .map(function (el) {
                return { id: el[id], nameOf: el[nameOf], description: el[description], image: el[image], page: el[page], } // map the JSON
            })
        return dataFiltered

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}
let currentView = 0 // home page
let sport = "Soccer"
let selectedAPI = ""
let selectedAPI2 = "" // for club vs club
const API = [
    ` https://www.thesportsdb.com/api/v1/json/1/all_countries.php`,

    ` https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=`, // leagues 
    ` https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=`, // teams from each league
    ` https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?t=`,  // *** PATREON ONLY *** players from eahc team

    ` https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=`,
    ` https://www.thesportsdb.com/api/v1/json/1/lookupplayer.php?id=`, // player detail by ID
]
const listView = ["Countries", "Leagues", "Clubs", "Players", "Results"]
const detailsViewDescription = ["CountryDetails", "LeagueDetails", "ClubDetails", "PlayerDetails", "ResultsDetails"]
const homePageLink = "index.html"
const pageTitle = ["Countries", `${selectedAPI} Soccer Leagues`, `Teams from ${selectedAPI}`, `Player from ${selectedAPI}`]
const pageDescription = [
    "all countries VIEW description",
    "top leagues from country VIEW description",
    "league all clubs VIEW description ",
    "club players VIEW description - not available *** PATREON ONLY ***  ", // patreon
    "players detais VIEW description - not available *** PATREON ONLY *** ", // patreon
    "club results VIEW description ",
]
const objectMain = ["countries", "countrys", "teams", "", "", ""] // arrays can be an object
const id = ["name_en", "idLeague", "idTeam", "", "", ""]
const nameOf = ["name_en", "strLeague", "strTeam", "", "", ""] // countries is incomplete data
const description = ["name_en", "strDescriptionEN", "strDescriptionEN", "", "", ""]
const image = ["name_en", "strLogo", "strTeamLogo", "", "", ""]
const page = ["name_en", "idLeague", "idTeam", "", "", ""] // move ...

async function galleryView(currentView, selectedAPI) {
    console.log(selectedAPI)
    console.log(API[currentView] + selectedAPI)
    console.log(objectMain[currentView])
    const elements = await listData(API[currentView] + selectedAPI,
        objectMain[currentView],
        id[currentView],
        nameOf[currentView],
        description[currentView],
        image[currentView],
        page[currentView])

    const galleryPage = `<h1>${pageTitle[currentView]}</h1>
                        <p>${pageDescription[currentView]}</p>
                        <h2>List of ${listView[currentView]}</h2>
                        <ul {display:inline;}>${pageList()}</ul>`

    function pageList() {


        let list = "";
        for (element in elements) {
            list = list + `<li {display:inline;}><a href=${elements[element].page}>
        <u id= ${elements[element].id}>${elements[element].nameOf}</u></a></li>
        <img src=${elements[element].image} alt=${elements[element].nameOf}>`;
        };
        return list;
    }
    render(galleryPage, selectedAPI)
}


async function detailsView(currentView, selectedAPI, element) { // current DETAILS views: 0=league, 1=clubs, 2=players, 3=results

    console.log(selectedAPI)
    console.log(API[currentView] + selectedAPI)


    const elements = await listData(API[currentView] + selectedAPI,// this can be an object call instead of multiple arrays
        objectMain[currentView],
        id[currentView],
        nameOf[currentView],
        description[currentView],
        image[currentView],
        page[currentView]);

    if (objectMain[currentView] == "countrys") { items = "leagues" } else { items = objectMain[currentView] } // change name of countrys to leagues

    let viewPage = `
    <ul>
        <li><a href=${homePageLink}><u>List of ${items}</u></a></li> 
      </ul>
      <h1><a href=${elements[element].nameOf}><u id="drill-down">${elements[element].nameOf}</u></a></h1>
      <img src=${elements[element].image} alt=${elements[element].nameOf}>
      <p>${elements[element].description}</p>
    `
    selectedAPI2 = elements[element].nameOf.toString()
    console.log(selectedAPI)
    console.log(viewPage)
    render(viewPage, selectedAPI)
}

async function render(view, selectedAPI) {

    const root = document.getElementById("root");
    root.textContent = "";
    root.insertAdjacentHTML('beforeend', view);
    const a = document.getElementsByTagName("a");

    const elements = await listData(API[currentView] + selectedAPI,// called too much times!  
        objectMain[currentView],
        id[currentView],
        nameOf[currentView],
        description[currentView],
        image[currentView],
        page[currentView])

    for (let i = 0; i < a.length; i++) {
        a[i].addEventListener('click', (event) => {
            event.preventDefault();

            for (element in elements) {
                console.log(elements[element].id + " == " + event.target.id)
                if (elements[element].id == event.target.id) {
                    detailsView(currentView, selectedAPI, element);
                }
                else if (event.target.id == "drill-down") {

                    selectedAPI = selectedAPI2;
                    console.log(selectedAPI)
                    event.target.id = "8888888888"
                    currentView = currentView + 1;
                    galleryView(currentView, selectedAPI); // drill down
                }
                else if (event.target.id == "") {
                    event.target.id = "8888888888"
                    galleryView(currentView, selectedAPI); // and or go back one level
                }
            }
        });
    }
}

galleryView(currentView, selectedAPI);
