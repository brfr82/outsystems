
// Router for pages, prevent event handler
// render homePage

// https://rickandmortyapi.com/api/character

// MAP/Filter to characters {}

// id = 1 (where name=Ricky)
// id = 2 (where name=Morty)
// more will be added
// results > 0 > id
// results > 0 > name
// results > 0 > image


const seriesDescription = `Rick and Morty is an american animated television series created by...` // API (filtered array)
const homePageLink = "index.html" // prevent default -> render home page

const characters = {
    ricky: {
        id: 1,
        page: `ricky-profile.html`, // auto generate this link using the name, prevent default -> render ricky page
        image: `https://rickandmortyapi.com/api/character/avatar/1.jpeg`, // API (filtered array) or Maybe concat link+"id"+".jpeg"
        name: `Rick Sanchez`, // get from API (filtered array)
        description: `    Rick Sanchez, also know as Rick-137...<br>
        He is a genius scientist...<br>
        ...their son, <a href="morty-profile.html"><u id="2">Morty</u></a>. 
        He is voiced by Justin Roland` // Check if API has description in other JSONs
    },
    morty: {
        id: 2,
        page: `morty-profile.html`, // auto generate this link using the name, prevent default -> render ricky page
        image: `https://rickandmortyapi.com/api/character/avatar/2.jpeg`, // API (filtered array) or Maybe concat link+"id"+".jpeg"
        name: `Morty Smith`, // get from API (filtered array)
        description: `    Mortimer "Morty" Smith Sr is one of the...<br>
        He is a grandson of <a href="ricky-profile.html"><u id="1">Rick</u></a>
        and is often...<br>
        Morty attends Harry Herpson` // Check if API has description in other JSONs
    },
    SummerSmith: {
        id: 3,
        page: `summer-profile.html`, // auto generate this link using the name, prevent default -> render SUMMER page
        image: `https://rickandmortyapi.com/api/character/avatar/3.jpeg`, // API (filtered array) or Maybe concat link+"id"+".jpeg"
        name: `Summer Smith`, // get from API (filtered array)
        description: `    Female character `  // Check if API has description in other JSONs
    },
    BettySmith: {
        id: 4,
        page: `betty-profile.html`, // auto generate this link using the name, prevent default -> render SUMMER page
        image: `https://rickandmortyapi.com/api/character/avatar/4.jpeg`, // API (filtered array) or Maybe concat link+"id"+".jpeg"
        name: `Betty Smith`, // get from API (filtered array)
        description: `   Another Female character `  // Check if API has description in other JSONs
    },
};


/*
Cannot access 'characters' before initialization: 
initialize then add parameters, programatically (page from Name, image from API, name from API)

...their son, <a href=${characters[1].page}><u>Morty</u></a>.
He is a grandson of <a href=${characters[0].page}><u>Rick</u></a>
*/

const homePage = `<h1>Rick and Morty</h1>
<p>${seriesDescription}</p>
<h2>List of Characters</h2>
<ul {display:inline;}>${homePageList()}</ul>`

function homePageList() {

    let list = "";
    for (character in characters) {
        list = list + `<li {display:inline;}><a href=${characters[character].page}>
        <u id= ${characters[character].id}>${characters[character].name}</u></a></li>
        <img src=${characters[character].image} alt=${characters[character].name}>`;
    };
    return list;
}

function characterPage(character) {

    let page = `
    <ul>
        <li><a href=${homePageLink}><u>List of Characters</u></a></li>
      </ul>
      <h1>${characters[character].name}</h1>
      <img src=${characters[character].image} alt=${characters[character].name}>
      <p>${characters[character].description}</p>
    `
    return page;
}


function render(page) {

    const root = document.getElementById("root");
    root.textContent = "";
    root.insertAdjacentHTML('beforeend', page);

    const a = document.getElementsByTagName("a");
    //console.log(a.length);
    // for each add event listener

    for (let i = 0; i < a.length; i++) {

        a[i].addEventListener('click', (event) => {

            event.preventDefault();

            for (character in characters) {

                console.log(character + ":" + characters[character].id + " == " + event.target.id)

                if (characters[character].id == event.target.id) {
                    render(characterPage(character));
                }
                else if (event.target.id == "") {
                    render(homePage);

                }
            }

        });
    }
}

// start 
render(homePage);





