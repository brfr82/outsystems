
async function getItemList(API_ENDPOINT, API_LINK) {

    try {
        console.log(`${API_ENDPOINT}${API_LINK}`)
        const response = await fetch(`${API_ENDPOINT}${API_LINK}`)

        if (!response.ok) {
            throw new Error(window.message);
        }
        return response.json();

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

export { getItemList }