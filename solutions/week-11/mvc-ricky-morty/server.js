const http = require('http');
const fs = require('fs');
const PORT = 8088;
const server = http.createServer(handler)

server.listen(PORT);
console.log('server listening @port' + PORT);

function handler(request, response) {

    let url = request.url;
    const idCharacter = url.split('_')[0].substring(1)
    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method + "IDCharacter" + idCharacter);

    let contentTypeObj = { ttf:"font/ttf", json: "application/json", js: "text/javascript", png: "image/png", html: "text/html", css: "text/css" };
    let mimeType = contentTypeObj[url.split('.').pop()];
    let fileNames = fs.readdirSync('./'); // add folders / implement matching + routing

    if (url === "/index.html") {
        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });
    }
    else if (fileNames.includes(url.split('/').pop())) {
        fs.readFile(url.split('/').pop(), function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });
    }
    else if (idCharacter < 672) {
    
        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        })
    }
    else {
        response.writeHead(404);
        response.write('page unavailable'); // Create view for 404 unavailable
        response.end();
    }
}









