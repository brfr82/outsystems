import { list } from "./list.js";
import { loadSearchForm } from "./form-search.js";
import { gridView } from "./view-grid.js";
import { characterPage } from "./view-detail.js";

let API_LINK = 1;

export default function listView(item) {

    const view = (window.location.href.split("/")[3]).split("_")[0]
    console.log(view)

    const searchchar = document.getElementById("search-page");
    searchchar.addEventListener('click', loadSearchForm);

    const index = document.getElementById("search-page");
    index.addEventListener('click', gridView(item));

    //const contact = document.getElementById("contact-page");
    //contact.addEventListener('click', loadContactForm());

    if (view > 0) {
        characterPage(view - 1);
    }
    else {

        const root = document.getElementById("root");
        let container = document.createElement('section');
        let node_div = document.createElement('div');
        node_div.appendChild(gridView(item));
        container.appendChild(node_div);
        root.appendChild(container);
    }

    const a = document.getElementsByTagName("a");

    for (let i = 0; i < a.length; i++) {

        a[i].addEventListener('click', (event) => {

            event.preventDefault();

            for (let j = 0; j < (a.length / 2); j++) {

                if (item[j].id == event.target.id) {

                    history.pushState(item[j].id, `Details from ${item[j].name}`, a[i].href)
                    root.textContent = ""
                    characterPage(event.target.id - 1);
                }
                else if (event.target.id == "") {

                    history.pushState(item[j].id, `Details from ${item[j].name}`, "index.html")
                    root.textContent = ""
                    root.appendChild(container)
                }
            }
        });
    }
}

window.addEventListener("scroll", function () {

    if ((window.innerHeight + window.pageYOffset + 1) >= document.body.offsetHeight) {

        API_LINK = API_LINK + 1;
        const charName = document.getElementById("name");

        window.removeEventListener("scroll", function () { console.log("remove scroll listener") })

        if (charName) {
            getSearch(API_LINK);
        }

        else if (API_LINK < 35) {
            list(API_LINK);
        }
    }
})

function getSearch(API_LINK) {

    const charName = document.getElementById("name");
    const charSpecies = document.querySelector('input[name="species"]:checked')
    const charGender = document.querySelector('input[name="gender"]:checked')
    const charStatus = document.querySelector('input[name="status"]:checked')

    // checkboxes // unavailable feature in API --> internal implemention: call API multiple times
    /* var checkboxes = document.getElementsByName('species');
    for (var checkbox of checkboxes) {
      if (checkbox.checked)
        //console.log(checkbox.value)
        charSpecies = charSpecies + ',' + checkbox.value.toString();
    } */ 

    const el_31 = document.createElement('div');
    el_31.textContent = "SEARCH RESULTS - loading setimetout image here"
    const API_SEARCH = `${API_LINK}&name=${charName.value}&gender=${charGender.value}
                        &species=${charSpecies.value}&status=${charStatus.value}`
    console.log(API_SEARCH);

    list(API_SEARCH);
}

export { getSearch }




