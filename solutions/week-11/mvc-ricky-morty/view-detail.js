import { detail } from "./list.js";


async function characterPage(character) {   

    const homePageLink = "index.html"    

    let detailPage = Math.ceil((character + 1) / 20)
    character = character - (detailPage - 1) * 20

    let item = await detail(detailPage)
    root.textContent = ""

    let container = document.createElement('ul');
    let node_2 = document.createElement('li');
    container.appendChild(node_2);

    let node_3 = document.createElement('a');
    node_3.setAttribute('href', homePageLink);
    container.appendChild(node_3);

    let node_4 = document.createElement('u');
    node_4.textContent = "Return to List of Characters"
    node_3.appendChild(node_4);

    let node_5 = document.createElement('h1');
    node_5.textContent = item[character].name
    container.appendChild(node_5);

    let node_6 = document.createElement('img');
    node_6.setAttribute('id', 'zoom');
    node_6.setAttribute('src', item[character].image);
    node_6.setAttribute('alt', item[character].name);
    container.appendChild(node_6);

    let node_77 = document.createElement('p');
    node_77.textContent = "Gender: " + item[character].gender
    container.appendChild(node_77);

    let node_7 = document.createElement('p');
    node_7.textContent = "Species: " + item[character].species
    container.appendChild(node_7);

    let node_8 = document.createElement('p');
    node_8.textContent = "Location: " + item[character].location
    container.appendChild(node_8);

    let node_9 = document.createElement('p');
    node_9.textContent = "Status: " + item[character].status
    container.appendChild(node_9);

    root.appendChild(container);
}

export { characterPage }