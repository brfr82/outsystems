
function gridView(item) {

    const container = document.createElement('section');

    for (let i = 0; i < item.length; i++) {
      
        let node_1 = document.createElement('div');
        container.appendChild(node_1);

        let link = item[i].id + "_" + item[i].name + ".html"

        let node_4 = document.createElement('a');
        node_4.setAttribute('href', link);
        node_1.appendChild(node_4);

        let node_5 = document.createElement('img');
        node_5.setAttribute('id', item[i].id);
        node_5.setAttribute('src', item[i].image);
        node_5.setAttribute('alt', item[i].name);
        node_4.appendChild(node_5);

        let node_2 = document.createElement('a');
        link = link.replace(" ", "_")
        node_2.setAttribute('href', link);
        node_1.appendChild(node_2);

        let node_3 = document.createElement('u');
        node_3.textContent = item[i].name
        node_3.setAttribute('id', item[i].id);
        node_2.appendChild(node_3);
    }
  
    return container;
}

export { gridView }
