
import itemListController from "./controller/list.js"

const ROUTES = {
    LIST:  '/',
}


// FROM CHAT SERVER: HOW TO POST:
//const routes = [{ method: 'GET', path: '/messages', handler: listMessages },
//{ method: 'POST', path: '/messages', handler: saveMessage },
//{ method: 'GET', path: /.*/, handler: serveWebResource }
//]

const routes = [ 
    {path: /^\/?$/, init: itemListController}  // match path for app route + init page from module controller
]

// used in render(routes.list)

function getRoute(path) {
    return routes.find (function (route){
        return path.match(route.path); // protect array data from outside the module, encapsulated
    })
} 


export {ROUTES, getRoute}