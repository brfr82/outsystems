// import //  get the data from services, normally api.js 

import { getItemList } from "../service/api.js" // folder in /controller
import listView from "../view/list.js";
//populate dom
//import getCharacterList from "../view.js"


async function list() {
    // loading view , until fulfilled
    const data = await getItemList();
    // manipulate data, then send it to view
    // 
    //console.log(data)
    const dataObj = JSON.parse(JSON.stringify(data, ['teams', 'strTeam', 'strStadium', 'intStadiumCapacity', 'intFormedYear']));
    const dataFiltered = dataObj.teams
        .map(function (club) {
            return { strTeam: club.strTeam, strStadium: club["strStadium"], intFormedYear: club["intFormedYear"], intStadiumCapacity: club["intStadiumCapacity"] } // WORKING
        })
    //console.log(dataFiltered)
    // abscraction to clean the JSON eg: [objectMain, id, nameOf, description, image, page]
    // console.log(data)
    //console.log(dataFiltered)
    listView(dataFiltered);
}

export default list;