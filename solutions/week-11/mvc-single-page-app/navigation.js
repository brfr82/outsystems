import {ROUTES, getRoute} from "./routes.js"



export default function goTo(path) {
    // invoke init. path should be determined based in location

    // create history for that path: push state, replace state
 
    //getRoute("/").init() // access each location instead of /  for view pass character

    const location = "/"

    const route = getRoute(location);

    if (!route){
        goTo(ROUTES.LIST);
        return;
    }  
        let params;
        // const params = getURLParams(); 
            //input: localhost:8000/characters?name=morty
            //ouput: {name: "morty"}
        route.init(params)

}