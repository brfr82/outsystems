// rename view.js = > view/list.js


const root = document.getElementById("root");

export default function listView(item) {
    //console.log(item)

    root.textContent = ""

    const formView = `<form action="http://localhost:5500/" method="POST" id="form">

    <label for="capacity">Stadium capacity is more than:</label>
    <input id="capacity" name="capacity" type="text" pattern="[0-9]*" minlength="5" maxlength="6"/>
    <label for="year-formed"> && / OR(leave blank) Club was founded after (year)</label>
    <input id="year-formed" name="year-formed" placeholder="year(YYYY)" 
    type="text" pattern="[0-9]*" minlength="4" maxlength="4"/>
    <input type="submit" value="List Clubs" />
    <br>    
    <table class="blueTable" id="table">
        <tr></tr>
    </table>     
</form>
<p></p>
    `
    //console.log(formView)
    root.insertAdjacentHTML('afterbegin', formView)

    const tbody = document.getElementsByTagName("tbody")[0];
    tbody.textContent = "";
    const p = document.getElementsByTagName("p")[0];
    p.textContent = "";


    tbody.insertAdjacentHTML('beforeend', "<tr><th>Team</th><th>Stadium</th><th>FormedYear</th><th>StadiumCapacity</th></tr>")

    console.log(item)


    let dataFiltered = item
    console.log(Object.keys(dataFiltered).length)

    for (let i = 0; i < Object.keys(dataFiltered).length; i++) {
        tbody.insertAdjacentHTML('beforeend', "<tr>"
            + "<td>"
            + `<a href=${dataFiltered[i].strTeam}><u id= ${dataFiltered[i].strTeam}>${dataFiltered[i].strTeam}</u></a>`
            + "</td>"
            + "<td>"
            + `<a href=${dataFiltered[i].strStadium}><u id= ${dataFiltered[i].strStadium}>${dataFiltered[i].strStadium}</u></a>`
            + "</td>"
            + "<td>" + dataFiltered[i].intFormedYear + "</td>"
            + "<td>" + dataFiltered[i].intStadiumCapacity + "</td>"
            + "</tr>")
    }


    form.addEventListener('submit', (event) => {
        event.preventDefault();
        console.log("prevented default behaviour");
        const capacity = document.getElementById('capacity').value;
        console.log(capacity)
        const yearFormed = document.getElementById('year-formed').value;
        console.log(yearFormed)
        const tbody = document.getElementsByTagName("tbody")[0];
        tbody.textContent = "";
        const p = document.getElementsByTagName("p")[0];
        p.textContent = "";
        tbody.insertAdjacentHTML('beforeend', "<tr><th>Team</th><th>Stadium</th><th>FormedYear</th><th>StadiumCapacity</th></tr>")


        console.log(item)


        let dataFiltered = item
            .filter(function (club) {
                const yearFormed = document.getElementById('year-formed').value;
                const capacity = document.getElementById('capacity').value;
                return club.intFormedYear > yearFormed && club.intStadiumCapacity > capacity // filter capacity && year
            });

        console.log(Object.keys(dataFiltered).length)

        for (let i = 0; i < Object.keys(dataFiltered).length; i++) {
            tbody.insertAdjacentHTML('beforeend', "<tr>"
                + "<td>"
                + `<a href=${dataFiltered[i].strTeam}><u id= ${dataFiltered[i].strTeam}>${dataFiltered[i].strTeam}</u></a>`
                + "</td>"
                + "<td>"
                + `<a href=${dataFiltered[i].strStadium}><u id= ${dataFiltered[i].strStadium}>${dataFiltered[i].strStadium}</u></a>`
                + "</td>"
                + "<td>" + dataFiltered[i].intFormedYear + "</td>"
                + "<td>" + dataFiltered[i].intStadiumCapacity + "</td>"
                + "</tr>")
        }
    });

}


