form.addEventListener('submit', (event) => {
    event.preventDefault();
    console.log("prevented default behaviour");
    const capacity = document.getElementById('capacity').value;
    console.log(capacity)
    const yearFormed = document.getElementById('year-formed').value;
    console.log(yearFormed)
    listClubs();
});

async function fetchClubs() {
    const api = `https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League`;
    const response = await fetch(api);

    if (!response.ok) {
        throw new Error(body.message);
    }
    return response.json();
}

async function listClubs(capacity, yearFormed) {

    const tbody = document.getElementsByTagName("tbody")[0];
    tbody.textContent = "";
    const p = document.getElementsByTagName("p")[0];
    p.textContent = "";

    try {
        let data = await fetchClubs();
        const dataObj = JSON.parse(JSON.stringify(data, ['teams', 'strTeam', 'strStadium', 'intStadiumCapacity', 'intFormedYear']));
        console.log(dataObj)

        dataFiltered = dataObj.teams
            .filter(function (club) {
                const yearFormed = document.getElementById('year-formed').value;
                const capacity = document.getElementById('capacity').value;
                return club.intFormedYear > yearFormed && club.intStadiumCapacity > capacity // filter capacity && year
            })
            .map(function (club) {
                return { strTeam: club.strTeam, strStadium: club["strStadium"], intFormedYear: club["intFormedYear"], intStadiumCapacity: club["intStadiumCapacity"] } // WORKING
            })
        console.log(dataFiltered)
        tbody.insertAdjacentHTML('beforeend', "<tr><th>Team</th><th>Stadium</th><th>FormedYear</th><th>StadiumCapacity</th></tr>")

        for (team in dataFiltered) {
            tbody.insertAdjacentHTML('beforeend', "<tr>"
                + "<td>" + dataFiltered[team].strTeam + "</td>"
                + "<td>" + dataFiltered[team].strStadium + "</td>"
                + "<td>" + dataFiltered[team].intFormedYear + "</td>"
                + "<td>" + dataFiltered[team].intStadiumCapacity + "</td>"
                + "</tr>")
        }
    } catch (err) {
        err.message = "Type Error: failed to fetch";
        p.style.color = "red"
        p.insertAdjacentHTML('beforeend', err.message)
        console.log(err.message);
    }
}
