USE outsystems;

DROP TABLE IF EXISTS equipment;


-- ERROR 1217 (23000): Cannot delete or update a parent row: a foreign key constraint fails



CREATE TABLE IF NOT EXISTS equipment (
equip_id int(5) NOT NULL AUTO_INCREMENT,
type varchar(50) DEFAULT NULL,
install_date DATE DEFAULT NULL,
color varchar(20) DEFAULT NULL,
working bool DEFAULT NULL,
location varchar(250) DEFAULT NULL,
PRIMARY KEY(equip_id)
);

DELETE FROM sailors WHERE id = 1;
-- cannot delete because it is referenced by other table

-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails (`outsystems`.`boats`, CONSTRAINT `boats_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `sailors` (`id`))


SELECT * FROM sailors;

SELECT * from equipment;
