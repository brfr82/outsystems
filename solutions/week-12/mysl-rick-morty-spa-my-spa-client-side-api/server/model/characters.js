//// Step 1:  write characters from mySQL in "client/service/characters.js"

fs = require('fs')
const mysql = require("mysql2/promise");


const connection = mysql.createPool({
  host: "localhost",
  user: "newuser",
  password: "password",
  database: "rick_morty", 
  //port: "3306"
});


(async () => {
    try {
      var sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters`; // all characters
      var [data] = await connection.query(sql);
      
      const dataFiltered = JSON.parse(JSON.stringify(data, ['id', 'name', 'status', 'species', 'gender', 'from', 'lastSeenOn', 'image']));
      //console.log(dataFiltered);
      fs.writeFile("client/service/characters.json", JSON.stringify(dataFiltered),  function (err) {
  if (err) return console.log(err);
  console.log('data written to file client/service/characters.json ');
});
      
      
    } catch(err) {
      console.log(err);
      
    }
})();




// continue with processing:






const characters = require('./data/characters');


console.log("characters:...", characters); // [AsyncFunction: run]




/**
 * Adds a character
 *
 * @param {object} character new character data
 * @returns {number} the new character id
 */
function add(character) {
    const nextId = characters.length + 1;
    characters.push({ ...character, id: nextId });
    return nextId;
}

/**
 * Gets a character by its ID
 * Returns null if not found
 *
 * @param {number} id character's id
 * @returns {?object} the character
 */
function get(id) {
    return characters.find((character) => character.id === id);
}

/**
 * Lists all characters
 *
 * @param {object} [filter] optional filters
 * @returns {Array<object>} all characters
 */
function list(filter = {}) {
    const { name } = filter;

    if (name) {
        return characters.filter((char) =>
            char.name.match(new RegExp(name, 'i'))
        );
    }

    return characters;
}

module.exports = {
    add,
    get,
    list
};
