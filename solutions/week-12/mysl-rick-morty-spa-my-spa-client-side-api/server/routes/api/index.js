const listCharacters = require('./list-characters');
const getCharacter = require('./get-character');

module.exports = [
    {
        url: /\/api\/character\/(?<id>\d+)\/?/,
        method: 'GET',
        handler: getCharacter
    },
    {
        url: '/api/character',
        method: 'GET',
        handler: listCharacters
    }
];
