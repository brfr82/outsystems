// npm mysql2


import { getItemList } from "../service/api.js" // folder in /controller
import listView from "../view/view.js";


/*
new API for list and details
*/


async function list(apiPage) {
    // loading view , until fulfilled

    try {
        
	/*let characterIdStart = ( apiPage-1 ) * 20 + 1
	let characterIdEnd = characterIdStart + 20
	console.log(apiPage + "---" + characterIdStart + "---" + characterIdEnd);*/	
 
	 
	if (apiPage == undefined) { apiPage = 0 };
       	const API_ENDPOINT = "service/characters.json";

	let characterIdStart = ( apiPage ) * 20 + 1
	let characterIdEnd = characterIdStart + 20
	console.log(apiPage + "---" + characterIdStart + "---" + characterIdEnd);

	
	apiPage = ""; //correction, if undefined
        const data = await getItemList(API_ENDPOINT, apiPage);

        const dataObj = JSON.parse(JSON.stringify(data, ['id', 'name', 'image', 'from']));

        const dataFiltered = dataObj
	    .filter (function(el) {
		if (el.id >= characterIdStart && el.id < characterIdEnd){
			return { id: el.id, name: el.name, image: el.image, location: el.from }			
		}
	    }) // filter 20 characters only based on apiPage
            .map(function (el) {
                return { id: el.id, name: el.name, image: el.image, location: el.from }
            })

        if (document.getElementById('location')) {

            console.log(dataFiltered)
            const dataFiltered2 = dataObj.results
                .filter(function (el) {
                    const charLocationValue = document.getElementById('location').value
                    if (charLocationValue === "") {
                        return el.location.name
                    }
                    else {
                        return el.location.name === charLocationValue
                    }

                })
                .map(function (el) {
                    return { id: el.id, name: el.name, image: el.image }
                })

            console.log(dataFiltered2)
            listView(dataFiltered2);
        }
        else {
            listView(dataFiltered);
        }

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

async function detail(apiPage) {
    // loading view , until fulfilled
    try {
        const API_ENDPOINT = "https://rickandmortyapi.com/api/character?page=";

        const data = await getItemList(API_ENDPOINT, apiPage);

        const dataObj = JSON.parse(JSON.stringify(data, ['results', 'id', 'name', 'image', 'species', 'location', 'gender', 'status']));
        const dataFiltered = dataObj.results
            .map(function (el) {
                return { id: el.id, name: el.name, image: el.image, species: el.species, location: el.location.name, gender: el.gender, status: el.status }
            })

        return dataFiltered;

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}


async function location(apiPage) {
    // loading view , until fulfilled
    try {
        const API_ENDPOINT = "https://rickandmortyapi.com/api/location/?page="; // location is a variable...-> single function

        const data = await getItemList(API_ENDPOINT, apiPage);

        const dataObj = JSON.parse(JSON.stringify(data, ['results', 'name']));
        const dataFiltered = dataObj.results
            .map(function (el) {
                return { name: el.name }
            })
            .sort(function(a, b) {
                var nameA = a.name.toUpperCase(); //  - https://developer.mozilla.org/
                var nameB = b.name.toUpperCase(); 
                if (nameA < nameB) {
                  return -1;
                }
                if (nameA > nameB) {
                  return 1;
                }              
                // names must be equal - https://developer.mozilla.org/
                return 0;
              });

        return dataFiltered;

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

export { list, detail, location };