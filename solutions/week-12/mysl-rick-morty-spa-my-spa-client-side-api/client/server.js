const http = require('http');
const fs = require('fs');
const PORT = 8080;
const server = http.createServer(handler)

server.listen(PORT);
console.log('server listening @port' + PORT);

function handler(request, response) {

    let url = request.url;
    const idCharacter = url.split('_')[0].substring(1)
    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method + "IDCharacter" + idCharacter);

    let contentTypeObj = { ttf: "font/ttf", json: "application/json", js: "text/javascript", png: "image/png", html: "text/html", css: "text/css" };
    let mimeType = contentTypeObj[url.split('.').pop()];
    let fileNames = fs.readdirSync('./').concat(fs.readdirSync('./view')).concat(fs.readdirSync('./form'))
        .concat(fs.readdirSync('./service')).concat(fs.readdirSync('./controller')).concat(fs.readdirSync('./font')); 

    //console.log(url, url.split('/').pop(), mimeType, fileNames, fileNames.includes(url.split('/').pop()));
    if (url === "/index.html" || url === "/home.html") {
        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });
    }
    else if (fileNames.includes(url.split('/').pop())) {
        fs.readFile(url.substring(1), function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });
    }
    else if (idCharacter < 672 || url === "/search.html" || url === "/contact.html") { // match to list all pages

        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        })
    }
    else {
        response.writeHead(404);
        response.write('page unavailable'); // Create view for 404 unavailable
        response.end();
    }
}









