1st: Get SCHEMA:
https://transform.tools/json-to-mysql 

2nd: import:
https://dev.mysql.com/doc/mysql-shell/8.0/en/mysql-shell-utilities-json.html

https://mysqlserverteam.com/import-json-to-mysql-made-easy-with-the-mysql-shell/

https://www.digitalocean.com/community/tutorials/working-with-json-in-mysql

Previously, if you wanted to import data that was held by JSON documents to a MySQL database, then you had to implement a script to parse the JSON documents and generate the appropriate INSERT statements, or convert the JSON documents to CSV files to be able to execute a LOAD DATA INFILE statement, or find some other third-party tool that could help you achieve your goal. That could be a bit tedious, right?… Well, not anymore!

Now, you can import JSON documents to a MySQL database in a single operation using the MySQL Shell. Let’s jump into a quick example to show you how fast and simple it is.

	
$ mysqlsh root@localhost:33300/test --import /path_to_file/zips.json
Creating a session to 'root@localhost:33300/test'
Please provide the password for 'root@localhost:33300': 
Fetching schema names for autocompletion... Press ^C to stop.
Your MySQL connection id is 21 (X protocol)
Server version: 8.0.13 MySQL Community Server - GPL
Default schema `test` accessible through db.
Importing from file "/path_to_file/zips.json" to collection `test`.`zips` in MySQL Server at localhost:33300
 
.. 29353.. 29353
Processed 3.15 MB in 29353 documents in 1.2058 sec (24.34K documents/s)
Total successfully imported documents 29353 (24.34K documents/s)

In this example, all the JSON documents from file “zips.json” were imported to the ‘zips‘ collection in the ‘test’ schema, automatically creating the collection since it didn’t exist. By default, the name of the file without the extension is used as the target name, but you can specify a different one if you want. The JSON data can also be imported to an existing table and a specific column (by default, the column named ‘doc‘ is used if none is specified). Cool, right?