
// Install it in your Node application like this

//npm i node-fetch --save

//then put the line below at the top of the files where you are using the fetch API:

import { createRequire } from 'module';
const require = createRequire(import.meta.url);

const fetch = require("node-fetch");



async function getItemList(API_ENDPOINT, apiPage) {

    try {
        console.log(`${API_ENDPOINT}${apiPage}`)
        const response = await fetch(`${API_ENDPOINT}${apiPage}`)

        if (!response.ok) {
            throw new Error(window.message);
        }
        return response.json();

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

export { getItemList }