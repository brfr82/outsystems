
// convert and send data from mutiple json files (promise.all) to mysql

import { detail } from "../controller/list.js";


import { createRequire } from 'module';
const require = createRequire(import.meta.url);

const mysql = require("mysql2/promise");


const connection = mysql.createPool({
  host: "localhost",
  user: "newuser",
  password: "password",
  database: "rick_morty", 
  //port: "3306"
});

// DROP TABLE IF EXISTS characters;


let tblCharacters = `
CREATE TABLE IF NOT EXISTS characters (
id INT NOT NULL AUTO_INCREMENT,
name varchar(50),
status varchar(50),
species varchar(50),
gender varchar(50),
origin varchar(50), 
lastSeenOn varchar(50),
image varchar(100),
PRIMARY KEY(id)
);
`
// note: SELECT origin AS 'from' FROM characters


async function createTable(table) {
  const [data] = await connection.query(table 
  );

  console.log(data);
}

createTable(tblCharacters);



async function insert(values) {
  const [data] = await connection.query(
    `INSERT INTO characters values(${values})` //
  );

  console.log(data);
}



async function getData(apiPage) {

    let dataObj = await detail(apiPage);
    for (let i = 0; i < dataObj.length; i++) {

	// let values = dataObject[i].join("\",\""); // did not work
	let values = "\"" + [dataObj[i].id, dataObj[i].name, dataObj[i].status, dataObj[i].species, dataObj[i].gender, dataObj[i].from, dataObj[i].lastSeenOn, dataObj[i].image ].join("\",\"") + "\""; 

        console.log(i + "# iteration ### " + values);
	
	await insert(values);
        // concat data elements to single array

    }
}
(async () => {
    const promises = []
    for (let i = 1; i <= 34; i++) { // change to: 1 - 34 -> pages for API - characters
        promises.push(getData(i))
    }
    console.log(getData);
    await Promise.all(promises);
})();


/* Everything looks good, add other columns tomorrow, don't forget to always drop:

update: Done inserting all columns.




mysql> select count(*) from characters
    -> ;
+----------+
| count(*) |
+----------+
|      671 |
+----------+
1 row in set (0.00 sec)


-- describe table

mysql> desc characters
    -> ;
+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | int          | NO   | PRI | NULL    | auto_increment |
| name       | varchar(50)  | YES  |     | NULL    |                |
| status     | varchar(50)  | YES  |     | NULL    |                |
| species    | varchar(50)  | YES  |     | NULL    |                |
| gender     | varchar(50)  | YES  |     | NULL    |                |
| origin     | varchar(50)  | YES  |     | NULL    |                |
| lastSeenOn | varchar(50)  | YES  |     | NULL    |                |
| image      | varchar(100) | YES  |     | NULL    |                |
+------------+--------------+------+-----+---------+----------------+
8 rows in set (0.01 sec)


- verify data integrity (not null)

mysql> select count(*) FROM characters WHERE name IS NULL OR image IS NULL OR species IS  NULL OR lastSeenOn IS NULL OR gender IS NULL OR status IS NULL OR origin IS NULL ;
+----------+
| count(*) |
+----------+
|        0 |
+----------+
1 row in set (0.00 sec)


-- example query

mysql> select count(*) FROM characters WHERE name like "%morty%"
    -> ;
+----------+
| count(*) |
+----------+
|       54 |
+----------+
1 row in set (0.00 sec)




mysql> select * FROM characters WHERE id > 160 AND id <201;


+-----+---------------------+---------+-----------------------+---------+--------------------------------------+--------------------------------------+-----------------------------------------------------------+
| id  | name                | status  | species               | gender  | origin                               | lastSeenOn                           | image                                                     |
+-----+---------------------+---------+-----------------------+---------+--------------------------------------+--------------------------------------+-----------------------------------------------------------+
| 161 | Hydrogen-F          | Alive   | Alien                 | Female  | Alphabetrium                         | Alphabetrium                         | https://rickandmortyapi.com/api/character/avatar/161.jpeg |
| 162 | Ice-T               | Alive   | Alien                 | Male    | Alphabetrium                         | Alphabetrium                         | https://rickandmortyapi.com/api/character/avatar/162.jpeg |
| 163 | Ideal Jerry         | Dead    | Mythological Creature | Male    | Nuptia 4                             | Nuptia 4                             | https://rickandmortyapi.com/api/character/avatar/163.jpeg |
| 164 | Insurance Rick      | unknown | Human                 | Male    | unknown                              | Citadel of Ricks                     | https://rickandmortyapi.com/api/character/avatar/164.jpeg |
| 165 | Investigator Rick   | Dead    | Human                 | Male    | unknown                              | Citadel of Ricks                     | https://rickandmortyapi.com/api/character/avatar/165.jpeg |
| 166 | Invisi-trooper      | Alive   | Human                 | Male    | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/166.jpeg |
| 167 | Izzy                | Alive   | Animal                | unknown | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/167.jpeg |
| 168 | Jackie              | Alive   | Alien                 | Female  | Gazorpazorp                          | Gazorpazorp                          | https://rickandmortyapi.com/api/character/avatar/168.jpeg |
| 169 | Jacob               | Alive   | Human                 | Male    | Earth (C-137)                        | Earth (C-137)                        | https://rickandmortyapi.com/api/character/avatar/169.jpeg |
| 170 | Jacqueline          | Alive   | Human                 | Female  | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/170.jpeg |
| 171 | Jaguar              | Alive   | Human                 | Male    | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/171.jpeg |
| 172 | Jamey               | Alive   | Human                 | Male    | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/172.jpeg |
| 173 | Jan-Michael Vincent | Alive   | Human                 | Male    | unknown                              | Interdimensional Cable               | https://rickandmortyapi.com/api/character/avatar/173.jpeg |
| 174 | Jerry 5-126         | Alive   | Human                 | Male    | Earth (5-126)                        | Jerryboree                           | https://rickandmortyapi.com/api/character/avatar/174.jpeg |
| 175 | Jerry Smith         | Alive   | Human                 | Male    | Earth (C-137)                        | Earth (C-137)                        | https://rickandmortyapi.com/api/character/avatar/175.jpeg |
| 176 | Celebrity Jerry     | Alive   | Human                 | Male    | Earth (C-500A)                       | Earth (C-500A)                       | https://rickandmortyapi.com/api/character/avatar/176.jpeg |
| 177 | Jerry Smith         | Alive   | Human                 | Male    | Earth (Evil Rick's Target Dimension) | Earth (Evil Rick's Target Dimension) | https://rickandmortyapi.com/api/character/avatar/177.jpeg |
| 178 | Jerry's Mytholog    | Dead    | Mythological Creature | Male    | Nuptia 4                             | Nuptia 4                             | https://rickandmortyapi.com/api/character/avatar/178.jpeg |
| 179 | Jessica             | Alive   | Cronenberg            | Female  | Earth (C-137)                        | Earth (C-137)                        | https://rickandmortyapi.com/api/character/avatar/179.jpeg |
| 180 | Jessica             | Alive   | Human                 | Female  | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/180.jpeg |
| 181 | Jessica's Friend    | Alive   | Human                 | Female  | Earth (C-137)                        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/181.jpeg |
| 182 | Jim                 | Alive   | Human                 | Male    | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/182.jpeg |
| 183 | Johnny Depp         | Alive   | Human                 | Male    | Earth (C-500A)                       | Earth (C-500A)                       | https://rickandmortyapi.com/api/character/avatar/183.jpeg |
| 184 | Jon                 | Alive   | Alien                 | Male    | Gazorpazorp                          | Interdimensional Cable               | https://rickandmortyapi.com/api/character/avatar/184.jpeg |
| 185 | Joseph Eli Lipkip   | Alive   | Human                 | Male    | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/185.jpeg |
| 186 | Joyce Smith         | Alive   | Human                 | Female  | Earth (C-137)                        | Earth (C-137)                        | https://rickandmortyapi.com/api/character/avatar/186.jpeg |
| 187 | Juggling Rick       | unknown | Human                 | Male    | unknown                              | Citadel of Ricks                     | https://rickandmortyapi.com/api/character/avatar/187.jpeg |
| 188 | Karen Entity        | Alive   | Alien                 | Female  | Unity's Planet                       | Unity's Planet                       | https://rickandmortyapi.com/api/character/avatar/188.jpeg |
| 189 | Katarina            | Dead    | Human                 | Female  | Earth (Replacement Dimension)        | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/189.jpeg |
| 190 | Keara               | Alive   | Alien                 | Female  | Krootabulon                          | Earth (Replacement Dimension)        | https://rickandmortyapi.com/api/character/avatar/190.jpeg |
| 191 | Kevin               | Dead    | Alien                 | Male    | unknown                              | Zigerion's Base                      | https://rickandmortyapi.com/api/character/avatar/191.jpeg |
| 192 | King Flippy Nips    | Alive   | Alien                 | Male    | Pluto                                | Pluto                                | https://rickandmortyapi.com/api/character/avatar/192.jpeg |
| 193 | King Jellybean      | Dead    | Mythological Creature | Male    | Fantasy World                        | Fantasy World                        | https://rickandmortyapi.com/api/character/avatar/193.jpeg |
| 194 | Kozbian             | Alive   | Alien                 | unknown | unknown                              | Planet Squanch                       | https://rickandmortyapi.com/api/character/avatar/194.jpeg |
| 195 | Kristen Stewart     | Alive   | Human                 | Female  | Earth (C-500A)                       | Earth (C-500A)                       | https://rickandmortyapi.com/api/character/avatar/195.jpeg |
| 196 | Krombopulos Michael | Dead    | Alien                 | Male    | unknown                              | unknown                              | https://rickandmortyapi.com/api/character/avatar/196.jpeg |
| 197 | Kyle                | Dead    | Humanoid              | Male    | Zeep Xanflorp's Miniverse            | Kyle's Teenyverse                    | https://rickandmortyapi.com/api/character/avatar/197.jpeg |
| 198 | Lady Katana         | Dead    | Humanoid              | Female  | unknown                              | Dorian 5                             | https://rickandmortyapi.com/api/character/avatar/198.jpeg |
| 199 | Larva Alien         | Alive   | Alien                 | unknown | Larva Alien's Planet                 | Planet Squanch                       | https://rickandmortyapi.com/api/character/avatar/199.jpeg |
| 200 | Lawyer Morty        | unknown | Human                 | Male    | unknown                              | Citadel of Ricks                     | https://rickandmortyapi.com/api/character/avatar/200.jpeg |
+-----+---------------------+---------+-----------------------+---------+--------------------------------------+--------------------------------------+-----------------------------------------------------------+
40 rows in set (0.00 sec)


*/





