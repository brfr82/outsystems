import { getItemList } from "../service/api.js" 


async function detail(apiPage) {
    // loading view , until fulfilled
    try {
        const API_ENDPOINT = "https://rickandmortyapi.com/api/character?page=";

        const data = await getItemList(API_ENDPOINT, apiPage);

        const dataObj = JSON.parse(JSON.stringify(data, ['results', 'id', 'name', 'status', 'species', 'gender', 'origin', 'location', 'image']));
        const dataFiltered = dataObj.results
            .map(function (el) {
                return { id: el.id, name: el.name, status: el.status, species: el.species, gender: el.gender, from: el.origin.name, lastSeenOn: el.location.name, image: el.image }
            })

        return dataFiltered;

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

export { detail };


/* 

// the class-repo  app uses this structure
module.exports = [
    {
        id: 1,
        name: 'I am using data from the Server',
        status: 'Alive',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (C-137)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg'
    },

*/