// npm install mysql2 
// added port // changed port to 3306 or 3307
// created new user (access denied for root) : OK NOW!
/*
mysql>     CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
Query OK, 0 rows affected (0.13 sec)

mysql> GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';
Query OK, 0 rows affected (0.19 sec)

mysql>     FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.05 sec)

mysql> 
*/



// note: testing this file with "library" and "outsystems" (boat reservation) ---> databases available in ubuntu

const mysql = require("mysql2/promise");

// the database used and query performed will make no sense on your machines obviously,
// you would have to create a corresponding one on your own that mimics mine
//this is merely an example of syntax!!

const connection = mysql.createPool({
  host: "localhost",
  user: "newuser",
  password: "password",
  database: "library", // change here "library" or "outsystems",
  //port: "3306"
});

/*
connection
  .query("SELECT * FROM table1 LEFT JOIN table2 ON table1.id = table2.t1_id")
  .then(console.log);
*/

async function test() {
  const [data] = await connection.query(
    "SELECT name as \"users who borrowed books published before 1974\" FROM users WHERE id in (SELECT DISTINCT user_id FROM reservations WHERE book_id in (SELECT id FROM books WHERE year < \"1974-01-01\"));" // change this query to exercise 1 queries
  );

  console.log(data);
}

test();

/*

[
  TextRow { 'users who borrowed books': 'Bruno' },
  TextRow { 'users who borrowed books': 'Maria' }
]

*/
