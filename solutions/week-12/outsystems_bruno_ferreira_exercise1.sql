
-- ToDos: 
-- use JOINs instead of WHERE, justify (eg. see data from +2 tables), compare (eg. efficiency)
-- use triggers to UPDATE status (available/loaned/mantainance) of a single book, based on reservations inserts/updates

-- Note: The following queries are algebraically equivalent inside MySQL and will have the same execution plan.
-- SELECT * FROM A, B WHERE A.ID = B.ID;
-- SELECT * FROM A JOIN B ON A.ID = B.ID;
-- SELECT * FROM A JOIN B USING(ID);

-- Note2: joins are usefull when client wants new reports (eg: show all customers, including those who have no activity)

-- ToDOs DB further design/client requirements (experimental  behind the 4 questions): 

-- book: ISBN (references Publishers and other Data); 
-- manages multiple physical copies of same title (internal library ref) 
-- ... or columns:nr.copies available, nr.copies loaned (triggered from reservations)
-- multiple Authors of same Book; 
-- multiple publishers of same book;
-- author's can have one publisher(contract) or more (even if no books yet published);
-- library employees (responsible for book loans)
-- logins for users / logins for employees
-- create a history of reservations (vs the last reservations - current design)

-- experimental: use selects for updates/creates/inserts/deletes

DROP DATABASE IF EXISTS library;

CREATE DATABASE library;
USE library;


-- (for test purpose) if database was not deleted, this is the correct table drop order:
DROP TABLE IF EXISTS reservations;

DROP TABLE IF EXISTS books;

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS publishers;



-- users: id, name
CREATE TABLE IF NOT EXISTS users (
id INT NOT NULL AUTO_INCREMENT,
name varchar(50) NOT NULL,
PRIMARY KEY(id)
);

-- authors: id, author
CREATE TABLE IF NOT EXISTS authors (
id INT NOT NULL AUTO_INCREMENT,
author varchar(50) NOT NULL,
PRIMARY KEY(id)
);

-- publisher: id, company_name
CREATE TABLE IF NOT EXISTS publishers (
id INT NOT NULL AUTO_INCREMENT,
company_name varchar(50) NOT NULL,
PRIMARY KEY(id)
);

-- books: id, title, year, author_id (FK author.id), publisher_id (FK publisher.id)  , status (available, out on loan : triggered on reservations or MANUAL return)
CREATE TABLE IF NOT EXISTS books (
id INT NOT NULL AUTO_INCREMENT,
title varchar(100) NOT NULL,
year DATE NOT NULL,
author_id INT NOT NULL,
publisher_id INT NOT NULL,
status BOOLEAN NOT NULL DEFAULT TRUE,
PRIMARY KEY(id),
FOREIGN KEY (author_id) REFERENCES authors(id),
FOREIGN KEY (publisher_id) REFERENCES publishers(id)
);

-- reservations: week(PK: book+week), book_id (FK), user_id(FK)
CREATE TABLE IF NOT EXISTS reservations (
loan_date DATE NOT NULL,
book_id INT NOT NULL,
user_id INT NOT NULL,
predicted_return_date DATE NOT NULL,
effective_return_date DATE NOT NULL DEFAULT "2099-01-01",
PRIMARY KEY(book_id, effective_return_date),
FOREIGN KEY (book_id) REFERENCES books(id),
FOREIGN KEY (user_id) REFERENCES users(id)
);


-- the book cannot be re-loan before it is returned, Pk CONSTRAINT. / TRIgger

--
SHOW TABLES;
DESC authors, 
DESC books;
DESC publishers;
DESC reservations;
DESC users;


INSERT INTO users values(1,"Bruno");
INSERT INTO users values(2,"Maria");
INSERT INTO users values(3, "José");
INSERT INTO authors values(1,"Tolstoy");
INSERT INTO authors values(2,"Exupery");
INSERT INTO publishers values(1,"Almedina");
INSERT INTO publishers values(2,"Porto Editora");
INSERT INTO books values(1,"Ana Karenina","1929-12-12",1,1,1);
INSERT INTO books values(2,"Princepezinho","1945-01-01",2,2,1);

INSERT INTO reservations values("2020-01-01",1,1, "2020-01-10", DEFAULT);
-- returned book: this makes the PK available / book available to loan again 
UPDATE reservations SET effective_return_date = "2020-01-10" WHERE book_id=1 AND effective_return_date = DEFAULT; 
INSERT INTO reservations values("2020-01-11",2,2, "2020-01-30", DEFAULT);
INSERT INTO reservations values("2020-01-01",2,1, "2020-01-10", DEFAULT);


SELECT * FROM users;
SELECT * FROM authors;
SELECT * FROM publishers;
SELECT * FROM books;
SELECT * FROM reservations;

ALTER TABLE books ADD year DATE NULL AFTER title;
UPDATE books SET year="1945-1-1" WHERE title="Princepezinho";
UPDATE books SET year="1928-1-1" WHERE title="Ana Karenina";

INSERT INTO authors values(3,"Eça de Queiroz");

INSERT INTO books values(3,"O Primo Basílio","1878-01-01",3,2,1);

COMMIT;

-- QUERIES 1st design


-- Do we have any books by Tolstoi
SELECT title as "books from Tolstoi" FROM books WHERE author_id = (SELECT id FROM authors WHERE author LIKE "Tolstoi");
SELECT title as "books from Tolstoi"  FROM books WHERE author_id = (SELECT id FROM authors WHERE author LIKE "Tolst%");

-- How many books are out on loan
SELECT COUNT(book_id) as "total loaned books" FROM reservations WHERE effective_return_date = "2099-01-01";

-- Which company published O Primo Basílio
SELECT company_name as "Publisher of Primo Basilio" FROM publishers WHERE id = (SELECT publisher_id FROM books WHERE title LIKE "O Primo Basílio");

-- Which users have borrowed books published before 1974
-- steps:
SELECT id FROM books WHERE year < "1974-01-01";

SELECT DISTINCT user_id FROM reservations WHERE user_id in (SELECT id FROM books WHERE year < "1974-01-01");


-- complete solution:
SELECT name as "users who borrowed books" FROM users WHERE id in (SELECT DISTINCT user_id FROM reservations WHERE book_id in (SELECT id FROM books WHERE year < "1974-01-01"));



-- TRY to loan a book that is not returned yet:

INSERT INTO reservations values("2020-01-11",2,3, "2020-01-30", DEFAULT);

-- book 3 is not yet returned --> trigger status...




