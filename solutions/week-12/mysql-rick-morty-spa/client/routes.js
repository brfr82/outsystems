import characterList from './controller/list.js';
import characterDetails from './controller/details.js';

const ROOT_ELEMENT_ID = 'root-container';

const ROUTES = {
    CHARACTER_LIST: '/',
    DETAILS: '/details'
};

const routes = [
    { path: /^\/details\/(?<id>\d+)\/?$/, init: characterDetails },
    { path: /^\/?$/, init: characterList }
];

function render(route, args) {
    const rootEl = document.getElementById(ROOT_ELEMENT_ID);
    const page = document.createElement('div');
    page.className = 'page';

    rootEl.innerHTML = '';
    rootEl.appendChild(page);

    route.init(page, args);
}

function getRoute(path) {
    return routes.find(function (route) {
        return path.match(route.path);
    });
}

export { render, getRoute, ROUTES };
