import container from '../components/container.js';
import image from '../components/image.js';

const LOADING_IMG = '../../assets/portal.png';
const LOADING_TXT = 'Loading...';

export default function loading(rootEl) {
    const containerEl = container('loading content');
    containerEl.className = 'loading';
    containerEl.appendChild(image(LOADING_IMG, LOADING_TXT));
    containerEl.appendChild(loadingMessage());

    rootEl.appendChild(containerEl);
}

function loadingMessage() {
    const element = document.createElement('h1');
    element.className = 'loading-message';
    element.innerText = LOADING_TXT;

    return element;
}
