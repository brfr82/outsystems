import banner from '../components/banner.js';
import container from '../components/container.js';
import goBackButton from '../components/go-back-button.js';

const BANNER_IMG = '../../assets/sunset.png';
const DEFAULT_IMAGE =
    'https://rickandmortyapi.com/api/character/avatar/19.jpeg';

export default function (rootEl, character) {
    rootEl.classList.add('bg-blue');

    const { image, name } = character;
    const bannerEl = banner(BANNER_IMG, name, image || DEFAULT_IMAGE);
    const contentEl = container('content');

    contentEl.appendChild(details(character));
    contentEl.appendChild(goBackButton());
    rootEl.appendChild(bannerEl);

    rootEl.appendChild(contentEl);
}

function details(character) {
    const { species, status, from, lastSeenOn } = character;
    const detailsEl = container('details');
    const detailFields = [
        detail('Species', species),
        detail('Status', status),
        detail('From', from),
        detail('Last seen on', lastSeenOn)
    ];

    detailFields.forEach((element) => detailsEl.appendChild(element));
    return detailsEl;
}

function detail(name, value) {
    const detailEl = container('detail');
    const detailTitle = paragraph(`${name}:`, 'title');
    const detailValue = paragraph(value, 'value');

    detailEl.appendChild(detailTitle);
    detailEl.appendChild(detailValue);

    return detailEl;
}

function paragraph(content, className) {
    const element = document.createElement('p');
    element.innerText = content;
    element.className = className;

    return element;
}
