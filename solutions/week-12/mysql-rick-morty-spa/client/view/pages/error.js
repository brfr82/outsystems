import banner from '../components/banner.js';
import container from '../components/container.js';
import goBackButton from '../components/go-back-button.js';

const ERROR_IMG = '../../assets/rick-mugshot.png';

export default function error(rootEl, message) {
    rootEl.appendChild(banner(ERROR_IMG));
    rootEl.appendChild(errorMessage(rootEl, message));
    rootEl.appendChild(goBackButton());
}

function errorMessage(rootEl, message) {
    const content = container('error content');
    const text = document.createElement('h1');
    text.textContent = 'Error:';

    const errorDescription = document.createElement('h3');
    errorDescription.textContent = message || 'unknown error';

    content.appendChild(text);
    content.appendChild(errorDescription);
    rootEl.appendChild(content);

    return content;
}
