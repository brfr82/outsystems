function itemGrid(items) {
    const container = document.createElement('ul');
    container.className = 'grid';

    items.forEach(function (item) {
        container.appendChild(createItem(item));
    });

    return container;
}

function createItem(element) {
    const itemWrapper = document.createElement('li');
    itemWrapper.className = 'item';

    itemWrapper.appendChild(element);
    return itemWrapper;
}

export default itemGrid;
