import container from '../components/container.js';
import image from '../components/image.js';

const LOADING_IMG = '../../assets/portal.png';
const LOADING_TXT = 'Loading...';

function loader(parent) {
    const loaderEl = container(
        'full-width full-height loading flex column center'
    );
    loaderEl.appendChild(image(LOADING_IMG, LOADING_TXT, 'rotate'));
    loaderEl.appendChild(loadingMessage());

    parent.appendChild(loaderEl);

    return () => {
        loaderEl.remove();
    };
}

function loadingMessage() {
    const element = document.createElement('h1');
    element.className = 'loading-message';
    element.innerText = LOADING_TXT;

    return element;
}

export default loader;
