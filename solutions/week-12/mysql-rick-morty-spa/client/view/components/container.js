function container(className) {
    const containerEl = document.createElement('div');
    containerEl.className = `container ${className}`;

    return containerEl;
}

export default container;
