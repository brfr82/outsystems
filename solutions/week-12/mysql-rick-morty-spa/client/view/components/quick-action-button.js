import button from './button.js';

function quickActionButton(content, onClick, className) {
    const buttonEl = button(className, content, onClick);

    buttonEl.id = 'quick-action-button';
    return buttonEl;
}

export default quickActionButton;
