function image(url, alt, className = '') {
    const image = document.createElement('img');
    image.src = url;
    image.alt = alt;

    image.className = `${className}`;

    return image;
}

export default image;
