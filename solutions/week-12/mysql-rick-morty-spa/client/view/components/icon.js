function icon(name, className = '') {
    const element = document.createElement('i');
    element.className = `${name} ${className}`;

    return element;
}

export default icon;
