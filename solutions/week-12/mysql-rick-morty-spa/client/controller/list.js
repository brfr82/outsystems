import { getCharacterList } from '../service/api.js';
import characterList from '../view/pages/character-list.js';
import { getQueryString } from '../navigation.js';
import error from '../view/pages/error.js';
import loader from '../view/components/loader.js';

export default async function (rootEl) {
    try {
        const removeLoader = loader(rootEl);
        const data = await getCharacterList(getQueryString());
        removeLoader();
        characterList(rootEl, data);
    } catch (err) {
        rootEl.innerHTML = '';
        error(rootEl, err.message);
    }
}
