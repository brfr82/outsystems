import { render, getRoute, ROUTES } from './routes.js';

function goTo(path, bypassHistory, state) {
    if (bypassHistory) {
        window.history.replaceState(state, path, path);
    } else {
        window.history.pushState(state, path, path);
    }

    const route = getRoute(document.location.pathname);

    // if path has no route, take client to character list page
    if (!route) {
        goTo(ROUTES.CHARACTER_LIST, true);
        return;
    }

    const params = getParamsFromPath(path, route);
    render(route, params);
}

function goBack() {
    window.history.back();
}

function getQueryString() {
    return document.location.search;
}

function getParamsFromPath(path, route) {
    const match = path.match(route.path);

    if (!match) {
        return {};
    }

    return match.groups || {};
}

export { goTo, goBack, getQueryString };
