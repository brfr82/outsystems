/*
const mysql = require("mysql2/promise");


const connection = mysql.createPool({
  host: "localhost",
  user: "newuser",
  password: "password",
  database: "rick_morty", 
  //port: "3306"
});

/*
module.exports = async function test ()  {
    try {
      var sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters WHERE id <21`;
      var [results] = await connection.query(sql);
      console.log(results);
      module.exports = results;
      return results;
    } catch(err) {
      console.log(err);
      return false;
    }
  }
*/






async function getCharacters(characterIdStart) {
		// using mySQL now: apiPages 1-34 
  const [mySQLdata] = await connection.query(`SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters WHERE id >= (1 + ${characterIdStart}) AND id <= (20 + ${characterIdStart});`    
  );
  //console.log(mySQLdata); 
  return mySQLdata;

}




async function listCharacters() {

  let apiPage = 1
  let characterIdStart = (apiPage - 1) * 20;
 
  const data = await getCharacters(characterIdStart);

  const dataObj = JSON.parse(JSON.stringify(data, ['id', 'name', 'status', 'species', 'gender', 'from', 'lastSeenOn', 'image']));
	//console.log("dataObj:  ", dataObj);

  return dataObj; // OK, removed key "TextRow"
 
};


module.exports = (async () => {
  const data = await listCharacters();
 
  console.log (data);
  return data;	
})();






/*
MYsql connection is OK:
{}
characters:... {}
Server started @ localhost:8000
dataObj:   [
  {
    id: 1,
    name: 'Rick Sanchez',
    status: 'Alive',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (C-137)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg'
  },
  {
    id: 2,
    name: 'Morty Smith',
    status: 'Alive',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (C-137)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/2.jpeg'
  },
  {
    id: 3,
    name: 'Summer Smith',
    status: 'Alive',
    species: 'Human',
    gender: 'Female',
    from: 'Earth (Replacement Dimension)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/3.jpeg'
  },
  {
    id: 4,
    name: 'Beth Smith',
    status: 'Alive',
    species: 'Human',
    gender: 'Female',
    from: 'Earth (Replacement Dimension)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/4.jpeg'
  },
  {
    id: 5,
    name: 'Jerry Smith',
    status: 'Alive',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (Replacement Dimension)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/5.jpeg'
  },
  {
    id: 6,
    name: 'Abadango Cluster Princess',
    status: 'Alive',
    species: 'Alien',
    gender: 'Female',
    from: 'Abadango',
    lastSeenOn: 'Abadango',
    image: 'https://rickandmortyapi.com/api/character/avatar/6.jpeg'
  },
  {
    id: 7,
    name: 'Abradolf Lincler',
    status: 'unknown',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (Replacement Dimension)',
    lastSeenOn: 'Testicle Monster Dimension',
    image: 'https://rickandmortyapi.com/api/character/avatar/7.jpeg'
  },
  {
    id: 8,
    name: 'Adjudicator Rick',
    status: 'Dead',
    species: 'Human',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'Citadel of Ricks',
    image: 'https://rickandmortyapi.com/api/character/avatar/8.jpeg'
  },
  {
    id: 9,
    name: 'Agency Director',
    status: 'Dead',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (Replacement Dimension)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/9.jpeg'
  },
  {
    id: 10,
    name: 'Alan Rails',
    status: 'Dead',
    species: 'Human',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: "Worldender's lair",
    image: 'https://rickandmortyapi.com/api/character/avatar/10.jpeg'
  },
  {
    id: 11,
    name: 'Albert Einstein',
    status: 'Dead',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (C-137)',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/11.jpeg'
  },
  {
    id: 12,
    name: 'Alexander',
    status: 'Dead',
    species: 'Human',
    gender: 'Male',
    from: 'Earth (C-137)',
    lastSeenOn: 'Anatomy Park',
    image: 'https://rickandmortyapi.com/api/character/avatar/12.jpeg'
  },
  {
    id: 13,
    name: 'Alien Googah',
    status: 'unknown',
    species: 'Alien',
    gender: 'unknown',
    from: 'unknown',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/13.jpeg'
  },
  {
    id: 14,
    name: 'Alien Morty',
    status: 'unknown',
    species: 'Alien',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'Citadel of Ricks',
    image: 'https://rickandmortyapi.com/api/character/avatar/14.jpeg'
  },
  {
    id: 15,
    name: 'Alien Rick',
    status: 'unknown',
    species: 'Alien',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'Citadel of Ricks',
    image: 'https://rickandmortyapi.com/api/character/avatar/15.jpeg'
  },
  {
    id: 16,
    name: 'Amish Cyborg',
    status: 'Dead',
    species: 'Alien',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'Earth (Replacement Dimension)',
    image: 'https://rickandmortyapi.com/api/character/avatar/16.jpeg'
  },
  {
    id: 17,
    name: 'Annie',
    status: 'Alive',
    species: 'Human',
    gender: 'Female',
    from: 'Earth (C-137)',
    lastSeenOn: 'Anatomy Park',
    image: 'https://rickandmortyapi.com/api/character/avatar/17.jpeg'
  },
  {
    id: 18,
    name: 'Antenna Morty',
    status: 'Alive',
    species: 'Human',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'Citadel of Ricks',
    image: 'https://rickandmortyapi.com/api/character/avatar/18.jpeg'
  },
  {
    id: 19,
    name: 'Antenna Rick',
    status: 'unknown',
    species: 'Human',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'unknown',
    image: 'https://rickandmortyapi.com/api/character/avatar/19.jpeg'
  },
  {
    id: 20,
    name: 'Ants in my Eyes Johnson',
    status: 'unknown',
    species: 'Human',
    gender: 'Male',
    from: 'unknown',
    lastSeenOn: 'Interdimensional Cable',
    image: 'https://rickandmortyapi.com/api/character/avatar/20.jpeg'
  }
]



*/







module.exports = [
    {
        id: 1,
        name: 'I am using data from the Server',
        status: 'Alive',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (C-137)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg'
    },
    {
        id: 2,
        name: 'Morty Smith',
        status: 'Alive',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (C-137)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/2.jpeg'
    },
    {
        id: 3,
        name: 'Summer Smith',
        status: 'Alive',
        species: 'Human',
        gender: 'Female',
        from: 'Earth (Replacement Dimension)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/3.jpeg'
    },
    {
        id: 4,
        name: 'Beth Smith',
        status: 'Alive',
        species: 'Human',
        gender: 'Female',
        from: 'Earth (Replacement Dimension)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/4.jpeg'
    },
    {
        id: 5,
        name: 'Jerry Smith',
        status: 'Alive',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (Replacement Dimension)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/5.jpeg'
    },
    {
        id: 6,
        name: 'Abadango Cluster Princess',
        status: 'Alive',
        species: 'Alien',
        gender: 'Female',
        from: 'Abadango',
        lastSeenOn: 'Abadango',
        image: 'https://rickandmortyapi.com/api/character/avatar/6.jpeg'
    },
    {
        id: 7,
        name: 'Abradolf Lincler',
        status: 'unknown',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (Replacement Dimension)',
        lastSeenOn: 'Testicle Monster Dimension',
        image: 'https://rickandmortyapi.com/api/character/avatar/7.jpeg'
    },
    {
        id: 8,
        name: 'Adjudicator Rick',
        status: 'Dead',
        species: 'Human',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'Citadel of Ricks',
        image: 'https://rickandmortyapi.com/api/character/avatar/8.jpeg'
    },
    {
        id: 9,
        name: 'Agency Director',
        status: 'Dead',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (Replacement Dimension)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/9.jpeg'
    },
    {
        id: 10,
        name: 'Alan Rails',
        status: 'Dead',
        species: 'Human',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: "Worldender's lair",
        image: 'https://rickandmortyapi.com/api/character/avatar/10.jpeg'
    },
    {
        id: 11,
        name: 'Albert Einstein',
        status: 'Dead',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (C-137)',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/11.jpeg'
    },
    {
        id: 12,
        name: 'Alexander',
        status: 'Dead',
        species: 'Human',
        gender: 'Male',
        from: 'Earth (C-137)',
        lastSeenOn: 'Anatomy Park',
        image: 'https://rickandmortyapi.com/api/character/avatar/12.jpeg'
    },
    {
        id: 13,
        name: 'Alien Googah',
        status: 'unknown',
        species: 'Alien',
        gender: 'unknown',
        from: 'unknown',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/13.jpeg'
    },
    {
        id: 14,
        name: 'Alien Morty',
        status: 'unknown',
        species: 'Alien',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'Citadel of Ricks',
        image: 'https://rickandmortyapi.com/api/character/avatar/14.jpeg'
    },
    {
        id: 15,
        name: 'Alien Rick',
        status: 'unknown',
        species: 'Alien',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'Citadel of Ricks',
        image: 'https://rickandmortyapi.com/api/character/avatar/15.jpeg'
    },
    {
        id: 16,
        name: 'Amish Cyborg',
        status: 'Dead',
        species: 'Alien',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'Earth (Replacement Dimension)',
        image: 'https://rickandmortyapi.com/api/character/avatar/16.jpeg'
    },
    {
        id: 17,
        name: 'Annie',
        status: 'Alive',
        species: 'Human',
        gender: 'Female',
        from: 'Earth (C-137)',
        lastSeenOn: 'Anatomy Park',
        image: 'https://rickandmortyapi.com/api/character/avatar/17.jpeg'
    },
    {
        id: 18,
        name: 'Antenna Morty',
        status: 'Alive',
        species: 'Human',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'Citadel of Ricks',
        image: 'https://rickandmortyapi.com/api/character/avatar/18.jpeg'
    },
    {
        id: 19,
        name: 'Antenna Rick',
        status: 'unknown',
        species: 'Human',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'unknown',
        image: 'https://rickandmortyapi.com/api/character/avatar/19.jpeg'
    },
    {
        id: 20,
        name: 'Ants in my Eyes Johnson',
        status: 'unknown',
        species: 'Human',
        gender: 'Male',
        from: 'unknown',
        lastSeenOn: 'Interdimensional Cable',
        image: 'https://rickandmortyapi.com/api/character/avatar/20.jpeg'
    }
];


