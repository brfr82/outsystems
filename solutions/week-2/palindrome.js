var word1 = 'racecar'
var word2 = 'hello'; 

console.log('Is ' , word1 , ' palindrome? ' , checkIfPalindrome(word1));
console.log('Is ' , word2 , ' palindrome? ' , checkIfPalindrome(word2));


function checkIfPalindrome(word) {
    
    var middleIndex = Math.round(word.length / 2, 0);
    var isPalindrome = true;

    for (var i = 0; i < middleIndex; i++) {

        var leftCharacter = (word[i]);
        var rightCharacter = (word[word.length - i - 1]);

        if (leftCharacter !== rightCharacter) {

            isPalindrome = false;
            break;
        }
    }

    return isPalindrome;

}

