
/* 
RULES:

p   ig - > igpay

sm   ile - >milesmay

add - > addway

else = do nothing */

var sentence = "there is a pig on the way blah";

console.log(pigLatin(sentence));
console.log(pigLatin('blah I am coding wtf'));

function pigLatin(sentence) {

    var vowels = "aeiouAEIOU";
    var words = sentence.split(" ");

    for (var word in words) {

        firstLetterIsVowel = vowels.includes(words[word][0]);
        secondLetterIsVowel = vowels.includes(words[word][1]);

        if (firstLetterIsVowel == false) {

            if (secondLetterIsVowel == false) {

                words[word] = words[word] + words[word][0] + words[word][1] + 'ay';
                words[word] = words[word].substring(2);

            }
            else {

                words[word] = words[word] + words[word][0]  + 'ay';
                words[word] = words[word].substring(1);
            }

        }

        else {

            words[word] = words[word] + 'way';
        }

    }

    return words.join(" ");
}



