/*
2 - declare a function that takes 2 arguments - ex: ('this is a potato', ['potato', 'salad']), the first one being a string and the second one an array of strings that are the forbidden words. The purpose of the function is to filter the given sentence and return a new one without any of the "forbidden words". ex: result for previous inputs should be 'this is a'
*/



// 2-

var fullSentence = 'This is a potato and a salad another potato';
var forbiddenWords = ['potato', 'salad'];

console.log(filterForbiddenWords(fullSentence, forbiddenWords));

function filterForbiddenWords(fullSentence, forbiddenWords) {
	var words = fullSentence.split(' ');
	var newSentence;
	for (var i = 0; i < words.length; i++) {
		if (!forbiddenWords.includes(words[i])) {
			newSentence = newSentence + ' ' + words[i];
		}
	}
	return newSentence;
}

