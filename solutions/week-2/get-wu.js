
/*
{ Exercise }
Can I get a Wu?
Write an algorithm with JavaScript that prints the index of the first occurrence of the character 'W' in a sentence 
You may use this snippet as a starting point  */



var sentence = "En garde, I'll let you try my Wu-Tang style.";
var character = 'W';


function getWu(str)
{
console.log('the first occurrence of the character W in "' + str + '" is ' + str.indexOf('W'));
}

getWu(sentence);

