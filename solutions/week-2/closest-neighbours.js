
var numbers = [5, 0, -2, 6, 777, 778];

closestNeighbours(numbers);
closestNeighbours([4,5,7]);

console.log("closest neighbours:" + closestNeighbours(numbers));

function closestNeighbours(numbers) {

    diference = Infinity;
    var leftNumber;
    var rightNumber;
    var absDiference;

    for (var i = 0; i < numbers.length - 1; i++) {

        absDiference = Math.abs(numbers[i] - numbers[i + 1])

        if (absDiference < diference) {

            diference = absDiference;

            leftNumber = numbers[i];

            rightNumber = numbers[i + 1];
        }

    }
    return [leftNumber, rightNumber];
}
