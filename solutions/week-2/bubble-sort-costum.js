
console.log(bubbleSortCustom([1, 3, -6, 14, 7777, -9098, 999999999]));


function bubbleSortCustom(arr) {

    for (var i = 0; i < arr.length; i++) {

        for (var j = 0; j < arr.length - i - 1; j++) {

            if (arr[j + 1] > arr[j]) {

                temp = arr[j];   

                arr[j] = arr[j+1];

                arr[j+1] = temp;

            }

        }

    }

    return arr;
}


/* // only valid if there are no zeros in the list:

var array = [ 0 ,3, 1, 4, 0 ];

array.sort(function (a,b) {
    return (a || Infinity) - (b || Infinity);
});

console.log(array); */