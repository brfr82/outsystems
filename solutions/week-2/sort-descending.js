
var numbers = [1, 3, -6, 14, 7777, -9098, 999999999];

console.log(biggestToSmallest(numbers));
console.log(biggestToSmallest([5,7,-22]));


function biggestToSmallest(numbers) {

    var sortedNumbers = [];
    var numbersLength = numbers.length;

    for (var i = 0; i < numbersLength; i++) {

        var biggestNumber = - Infinity ;

        for (var j = 0; j < numbers.length; j++) {

            if (j===0 || numbers[j] > biggestNumber) {        

                biggestNumber = numbers[j];
            }
        }
        sortedNumbers.push(biggestNumber);

        numbers.splice(numbers.indexOf(biggestNumber), 1);

    }

    return sortedNumbers;

}
