/*
3 - declare a function that takes 1 argument - ex: (7), that calculates the value of that inputed number in the Fibonacci sequence. - ex: result for previous input should be 13 (aka, the 7th number in the Fibonacci sequence is 13)*/




// 3-

var fibonacciIndex = 9

console.log(getFibonacciNumber(fibonacciIndex));

function getFibonacciNumber(fibonacciIndex) {

	var fibonacciSeq = []
	fibonacciSeq[0] = 0;
	fibonacciSeq[1] = 1;

	for (i = 2; i < fibonacciIndex + 1; i++) {

		fibonacciSeq[i] = fibonacciSeq[i - 1] + fibonacciSeq[i - 2];
	}
	return fibonacciSeq[fibonacciIndex];

}