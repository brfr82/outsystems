
import { list, detail } from "./controller/list.js";

const ROUTES = {
    LIST:  '/',
}

const routes = [ 
    {path: /^\/?$/, init: list},
    {path: /^\/?$/, init: detail} 
]


function getRoute(path) {
    return routes.find (function (route){
        return path.match(route.path); 
    })
} 


export {ROUTES, getRoute}