
import { getSearch, infiniteScroll } from "../view/view.js";
import { location } from "../controller/list.js";

function loadSearchForm() {

    window.removeEventListener("scroll", infiniteScroll)


    const apiPage = 1;  

    const root = document.getElementById("root");
    root.textContent=""
    const container = document.createElement('form');   

    function createRadioButtonElement(attributes) {

        let el = document.createElement('INPUT');
        el.setAttribute('type', 'radio');
        el.setAttribute('id', attributes[0]);
        el.setAttribute('name', attributes[1]);
        el.setAttribute('value', attributes[2]);
        el.setAttribute('required', '');
        container.appendChild(el);

        if (attributes[0] === 'all'){
            el.setAttribute('checked', 'checked');
        }

        el = document.createElement('LABEL');
        el.setAttribute('for', attributes[0]);
        el.textContent = attributes[0];
        container.appendChild(el);
    }

    container.setAttribute('action', 'http://localhost:8088/');
    container.setAttribute('method', 'GET');
    container.setAttribute('id', 'form');

    const el_1 = document.createElement('H2');
    container.appendChild(el_1);

    const el_2 = document.createElement('LABEL');
    el_2.setAttribute('for', 'name');
    el_2.textContent = "Name:                "
    container.appendChild(el_2);

    const el_4 = document.createElement('INPUT');
    el_4.setAttribute('type', 'text');
    el_4.setAttribute('id', 'name');
    el_4.setAttribute('name', 'name');
    el_4.setAttribute('pattern', '^[a-zA-Z0-9]*$');
    //el_4.setAttribute('required', '');
    el_4.setAttribute('minlength', '3');
    el_4.setAttribute('maxlength', '15');
    el_4.value = ""
    container.appendChild(el_4);

    const el_5 = document.createElement('BR');
    container.appendChild(el_5);

    const el_7 = document.createElement('LABEL');
    el_7.setAttribute('for', '');
    el_7.setAttribute('gender', '');
    el_7.textContent = "Gender:                "
    container.appendChild(el_7);

    createRadioButtonElement(['all', 'gender', '']);
    createRadioButtonElement(['male', 'gender', 'male']);
    createRadioButtonElement(['female', 'gender', 'female']);
    createRadioButtonElement(['genderless', 'gender', 'genderless']);
    createRadioButtonElement(['unknown', 'gender', 'unknown']);

    const el_a23 = document.createElement('BR');
    container.appendChild(el_a23);

    const el_a7 = document.createElement('LABEL');
    el_a7.setAttribute('for', '');
    el_a7.setAttribute('status', '');
    el_a7.textContent = "Status:                "
    container.appendChild(el_a7);

    createRadioButtonElement(['all', 'status', '']);
    createRadioButtonElement(['alive', 'status', 'alive']);
    createRadioButtonElement(['dead', 'status', 'dead']);
    createRadioButtonElement(['unknow', 'status', 'unknow']);

    const el_14 = document.createElement('BR');
    container.appendChild(el_14);

    const el_16 = document.createElement('LABEL');
    el_16.setAttribute('for', '');
    el_16.setAttribute('species', '');
    el_16.textContent = "Species:                "
    container.appendChild(el_16);

    createRadioButtonElement(['all', 'species', '']);
    createRadioButtonElement(['human', 'species', 'human']);
    createRadioButtonElement(['humanoid', 'species', 'humanoid']);
    createRadioButtonElement(['alien', 'species', 'alien']);
    createRadioButtonElement(['robot', 'species', 'robot']);

    const el_23 = document.createElement('BR');
    container.appendChild(el_23);

    const el_25 = document.createElement('LABEL');
    el_25.setAttribute('for', 'location');
    el_25.textContent = "Location:"
    container.appendChild(el_25);

    const el_27 = document.createElement('SELECT');
    el_27.setAttribute('id', 'location');
    el_27.setAttribute('name', 'location');
    container.appendChild(el_27);

    const el_28 = document.createElement("option");
    el_28.setAttribute('id', 'option');
    el_28.textContent = "#Any location#";
    el_28.value = "";
    el_27.appendChild(el_28);

    async function getLocations(apiPage) {

        let locationObj = await location(apiPage);
        for (let i = 0; i < locationObj.length; i++) {

            let loc = locationObj[i].name
            let el = document.createElement("option");
            el.setAttribute('id', 'option');
            el.textContent = loc;
            el.value = loc;
            el_27.appendChild(el);
        }
    }

    (async () => {
        const promises = []
        for (let i = 1; i <= 6; i++) { // 6 pages for API - locations
            promises.push(getLocations(i))
        }
        console.log(getLocations);
        await Promise.all(promises);
    })()


    const el_29 = document.createElement('P');
    el_29.value = " "
    container.appendChild(el_29);

    const el_233 = document.createElement('BR');
    container.appendChild(el_233);
    const el_2333 = document.createElement('BR');
    container.appendChild(el_2333);

    const el_2334 = document.createElement('BR');
    container.appendChild(el_2334);

    const el_30 = document.createElement('INPUT');
    el_30.setAttribute('id', 'submit');
    el_30.setAttribute('type', 'submit');
    el_30.setAttribute('value', '                Search                ');
    
    const el_31 = document.createElement("div");
    el_31.setAttribute('id', 'submitdiv');
    el_31.appendChild(el_30);
    
    container.appendChild(el_31);

    root.appendChild(container);

    form.addEventListener('submit', (event) => {
        event.preventDefault();   
        getSearch(apiPage)
    });

}

export { loadSearchForm }