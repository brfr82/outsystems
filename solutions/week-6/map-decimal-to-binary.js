
var numbers = [4, 5, 6, 15];

var base = 2;

function convertDecimalToBinary(numbers, base) {

    var binaries = numbers.map(function (number) {
        return number.toString(base)
    });


    return binaries;
}

console.log(convertDecimalToBinary(numbers, base));
