// ToDO servir qualquer tipo de ficheiro, 
    //sugestão1: criar objecto de extensões, {ext:"mime-type content",...}
    // sugestão2: criar pastas para organizar: public/video, public/image,

// ToDO servir um HTML fornecido---mostrar imagem ou permitir upload /test/styles.png- > require filesystem ('fs') - ver slides - SYNC cs ASYNC

// requere NPM install

// aceder no browser a http://127.0.0.1:8080/  --- > ver "developer tools no browser > sources > inspect"


const http = require('http'); // http já existe no NPM por defeito
const fs = require('fs');
const PORT = 8088;  // CTRL+C to KILL server ->Error: listen EADDRINUSE: address already in use :::8000 !!! -- > verify port state;  implement Error handling ??

const server = http.createServer(handler) // ver documentação


server.listen(PORT); // colocar server à escuta no respectivo porto, "vai ficar a aguardar um pedido"

console.log('server listening @port' + PORT);



function handler(request, response) {  // recebe dois objectos, função responsavel por tratar do pedido

    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method); // ver a request na consolsa

    let url = request.url; // lógica if-else url !== '/'
    let method = request.method; // ver esta lógica

    if (url !== '/') {

        response.writeHead(404);
        response.write('page unavailable');
        response.end();

    }

    else {
        fs.readFile("index.html", function (error, content){  // ASYNC Method
            // include error handling here
            response.writeHead(200, { 'Content-Type': 'text/html' });
            response.write("[ASYNC] content: " + content);  // códigos de reposta do servidor + conteúdo da resposta
            response.end(); // delimitador , pacote de bytes acabou, v/keep alive
        });
 

    }
}





