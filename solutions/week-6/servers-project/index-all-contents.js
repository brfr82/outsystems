// ToDO servir qualquer tipo de ficheiro,  --> var i = 0 (audio), i= 1(imagem), i=2 (html), i=3 (video)
var i = 3;

//sugestão1: criar objecto de extensões, {ext:"mime-type content",...}
// sugestão2: criar pastas para organizar: public/video, public/image,

// ToDO servir um HTML fornecido---mostrar imagem ou permitir upload /test/styles.png- > require filesystem ('fs') - ver slides - SYNC cs ASYNC

// requere NPM install

// aceder no browser a http://127.0.0.1:8080/  --- > ver "developer tools no browser > sources > inspect"


const http = require('http'); // http já existe no NPM por defeito
const fs = require('fs');
const PORT = 8088;  // CTRL+C to KILL server ->Error: listen EADDRINUSE: address already in use :::8000 !!! -- > verify port state;  implement Error handling ??

const contentTypeObj = { mp4: "video/mp4", mp3: "audio/mpeg", png: "image/png", html: "text/html" };

const fileNames = fs.readdirSync('./public');


function getContentTypes(fileNames) {

    var fileExtensions = []; // not used
    var contentTypes = [];

    for (var i = 0; i < fileNames.length; i++) { // should use MAP method, did not work

        var fileName = fileNames[i];

        fileExtensions.push(fileName.split('.').pop()); // not used 
        contentTypes.push(contentTypeObj[fileName.split('.').pop()])


    }

    return contentTypes;

}

var contentTypes = getContentTypes(fileNames);

// or use for to iterate through files, 
var fileName = 'public/' + fileNames[i]; // fileNames[i]; // should iterate through array of fileNames[i]
var contentType = contentTypes[i];   // should use filter functionin contentTypeObj[j]


const server = http.createServer(handler) // ver documentação

server.listen(PORT); // colocar server à escuta no respectivo porto, "vai ficar a aguardar um pedido"

console.log('server listening @port' + PORT);


function handler(request, response) {  // recebe dois objectos, função responsavel por tratar do pedido

    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method); // ver a request na consolsa
    console.log('ficheiro:' + fileName + ' tipo:' + contentType);
    let url = request.url; // lógica if-else url !== '/'
    let method = request.method; // ver esta lógica

    if (url !== '/') {

        response.writeHead(404);
        response.write('page unavailable');
        response.end();

    }

    else {

        // for (var// iterar pelos ficheiros da pasta  /public, salvando em fileName, o nome do ficheiro e em fileExtensio - > usar função .map ?     



        fs.readFile(fileName, function (error, content) {  // ASYNC Method , substituir string pela variavel file
            // include error handling here         

            response.writeHead(200, { 'Content-Type': contentType });  // implementar aqui a variável mimetype em função da extensão
            response.write(content);  // códigos de reposta do servidor + conteúdo da resposta
            response.end(); // delimitador , pacote de bytes acabou, v/keep alive
        });


    }
}
