
const fs = require('fs');

const contentTypeObj = { mp4: "video/mp4", mp3: "audio/mpeg", png: "image/png", html: "text/html" };


const fileNames = fs.readdirSync('./public');
console.log(fileNames);

//const fileExtensions = fileNames.map(function (fileName) {fileName.split('.').pop()});  - map file extensions, WIP
// console.log(fileExtensions);

function showType(fileNames) {

  var fileExtensions = [];
  var contentTypes = [];

  for (var i = 0; i < fileNames.length; i++) {

    var fileName = fileNames[i];

    fileExtensions.push(fileName.split('.').pop());

    contentTypes.push(contentTypeObj[fileName.split('.').pop()])

  }

  return contentTypes;

}

console.log(showType(fileNames));


