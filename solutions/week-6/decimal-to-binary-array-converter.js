

var numbers = [4, 5, 6, 15];

function convertDecimalToBinary(numbers){

    var binaries = [];

    for (var i in numbers){

        var binary = "";

        var j = numbers[i];
     
        while(j > 0){
            if(j % 2 == 0){
                binary = "0" + binary;
            }
            else {
                binary = "1" + binary;
            }
    
            j = Math.floor(j / 2);
        }

        binaries.push(binary); 


    }


    return binaries;
}

console.log(convertDecimalToBinary(numbers));