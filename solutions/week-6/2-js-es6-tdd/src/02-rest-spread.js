/**
 * Return an array containing the function arguments
 */
exports.argsAsArray = function () {

    var args = [];

    for (var i = 0; i < arguments.length; i++) {

        args[i] = arguments[i];
    }


    return args.sort();
};

/**
 * Return an array containing the function arguments,
 * but discarding the first two
 */
exports.lastArgs = function () {

    function remove(...toRemove) {
        toRemove.forEach(item => {
            var index = arr.indexOf(item);
            if (index != -1) {
                arr.splice(index, 1);
            }
        })
    }

    return arguments;
};

/**
 * Return a function which applies the provided transform function
 * on all arguments and returns an array with the results
 */
exports.transformArgs = function (transform) {

    return arguments.map(function (number) {
        transform(number);
    })


};

/**
 * Invoke the callback function passing each character
 * from the provided string as an argument
 */
exports.spreadChars = function (str, cb) {


    const slicedStr = [...str];
    return slicedStr.map(function (char) {
        cb(char);

    })
};

/**
 * Concatenate the provided arrays using the spread operator
 */
exports.mergeArrays = function (arr1, arr2) {

    return [...arr1, ...arr2];


};

/**
 * Return array of chars from the provided array of strings
 * using the spread operator
 */
exports.spreadArrayStrings = function (arr) {

    return arr.map(function (str) {
        [...str];

    })

};

/**
 * Flatten an array of arrays using the spread operator
 */
exports.flattenArray = function (arr) {

    return [].concat(...array);

};

