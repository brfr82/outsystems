const http = require('http');
const fs = require('fs');
// const mime = require('mime-types') // future install

const PORT = 8088;
const server = http.createServer(handler)

server.listen(PORT);
console.log('server listening @port' + PORT);

function handler(request, response) {

    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method);

    let url = request.url;
    //let method = request.method; //unused

    let contentTypeObj = { mp4: "video/mp4", mp3: "audio/mpeg", png: "image/png", html: "text/html" };
    let mimeType = contentTypeObj[url.split('.').pop()];

    let fileNames = fs.readdirSync('./').concat(fs.readdirSync('./public'));
    console.log(url, url.split('/').pop(), mimeType,fileNames, fileNames.includes(url.split('/').pop()));

    if (url === "/") {
        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        });
    }
    else if (fileNames.includes(url.split('/').pop())) {
        fs.readFile(url.substring(1), function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });

    }
    else {
        response.writeHead(404);
        response.write('page unavailable');
        response.end();
    }


}



/*

TypeError [ERR_INVALID_ARG_TYPE]: The first argument must be of type string or an instance of Buffer or Uint8Array. Received undefined
    at write_ (_http_outgoing.js:661:11)
    at ServerResponse.write (_http_outgoing.js:629:15)
    at ReadFileContext.callback (C:\Users\home\outsystems\outsystems\solutions\week-6\rick-morty-html-server\index.js:35:22)
    at FSReqCallback.readFileAfterOpen [as oncomplete] (fs.js:261:13) {
  code: 'ERR_INVALID_ARG_TYPE'

*/






