
var people = [
    {
        firstName: "Sónia",
        lastName: "Rosa",
        yearOfBirth: 1982,
        isMarried: true,
        pets: ["Vivi"],
    },
    {
        firstName: "Inês",
        lastName: "Carvalho",
        yearOfBirth: 1998,
        isMarried: false,
        pets: [],
    },
    {
        firstName: "Icaro",
        lastName: "Alves",
        yearOfBirth: 1987,
        isMarried: true,
        pets: ["Theo"],
    },
    {
        firstName: "Miguel",
        lastName: "Moutinho",
        yearOfBirth: 1994,
        isMarried: false,
        pets: [],
    },
    {
        firstName: "Luís",
        lastName: "Agostinho",
        yearOfBirth: 1993,
        isMarried: false,
        pets: ["Orion", "Mellow"],
    },
    {
        firstName: "Joana",
        lastName: "Moura",
        yearOfBirth: 1987,
        isMarried: false,
        pets: [],
    },
    {
        firstName: "Gonçalo",
        lastName: "Vilaça",
        yearOfBirth: 2001,
        isMarried: false,
        pets: ["Mixie", "sushi", "sashimi"],
    },
    {
        firstName: "bruno",
        lastName: "ferreira",
        yearOfBirth: 1983,
        isMarried: true,
        pets: [],
    },
    {
        firstName: "Pedro",
        lastName: "Isidoro",
        yearOfBirth: 1992,
        isMarried: false,
        pets: ["Ace"],
    },
    {
        firstName: "Leandro",
        lastName: "Batistela",
        yearOfBirth: 1997,
        isMarried: false,
        pets: [],
    },
    {
        firstName: "Fábio",
        lastName: "Del Vigna",
        yearOfBirth: 1989,
        isMarried: true,
        pets: ["Panda", "Maximus"],
    },
    {
        firstName: "André",
        lastName: "Santos",
        yearOfBirth: 1991,
        isMarried: false,
        pets: ["Júlio"],
    },
    {
        firstName: "João",
        lastName: "Fabião",
        yearOfBirth: 1997,
        isMarried: false,
        pets: ["Mia", "Riscas", "Bolinhas"],
    },
    {
        firstName: "Tiago",
        lastName: "Faria",
        yearOfBirth: 1986,
        isMarried: false,
        pets: ["Gandhi", "Sushi"],
    },
];

var minTimes = 3;
var maxTimes = 14;
var randomTimes = Math.round(Math.random() * (maxTimes - minTimes)) + minTimes;

for (var i = 0; i < randomTimes; i++) {    

    setTimeout(function () { 
        randomPeopleIndex = Math.round(Math.random() * (people.length - 1), 0);
        console.log(`Hello ${people[randomPeopleIndex].firstName}`) }, 1000 * i);
};

