const fs = require ("fs") 

const data = "Hello";
const filePath ="write-file.txt";


 const writeFilePromise = (filePath,data) => {  // previous incorrect (filePath), despited tried
    return new Promise((resolve, reject) => {
        // do some async action, promise is in pending state

      fs.writeFile(filePath, data, (err) => {   // previous  incorrect , this was the main mistake! filePath , (data, err)

        // promise will be rejected

        if (err) return reject(err)

        /* everything ok */
        resolve(data)
      })
    })
  }


  writeFilePromise(filePath,data)                   // previous incorrect (filePath),, despited tried
  .then(data => {`${data} written in file ${filePath}`})
  .catch(err => {console.log(err)})     

  // 'ERR_INVALID_CALLBACK'