// changed get elementS from class, unsucessfull, to get element by ID , success

var colors = [  // using object of colors, instead of array of colors, for testing purposes_ generalize color+backgroundcolor+...

       {
              color: "pink",
       },
       {
              color: "brown",
       },
       {
              color: "orange",
       },
       {
              color: "yellow",
       },
       {
              color: "black",
       },
       {
              color: "white",
       },
       {
              color: "green",
       },
       {
              color: "red",
       },
       {
              color: "blue",
       },
];

const minTimes = 50;
const maxTimes = 100;
const numBoxColors = 6;

const randomTimes = Math.round(Math.random() * (maxTimes - minTimes)) + minTimes;


for (let i = 0; i < randomTimes; i++) {

       setTimeout(function () {

              let randomColorIndexes = [];
              let randomColorNames = [];

              for (let j = 0; j < numBoxColors; j++) {
                     randomColorIndexes[j] = Math.round(Math.random() * (colors.length - 1), 0);
                     randomColorNames[j] = colors[randomColorIndexes[j]].color;
              }

              // notice, this is a non-scalable solution here!!  iterate getElements... through an array of boxes
              // instead of creating const for each

              const box1 = document.getElementsByClassName("box box1");
              const box2 = document.getElementsByClassName("box box2");
              const box3 = document.getElementsByClassName("box box3");
              const box4 = document.getElementsByClassName("box box4");
              const box5 = document.getElementsByClassName("box box5");
              const box6 = document.getElementsByClassName("box box6");

              console.log(`round nr ${i + 1} => color list: ${randomColorNames}`)


              box2[0].style.backgroundColor = randomColorNames[1];
              box3[0].style.backgroundColor = randomColorNames[2];
              box4[0].style.backgroundColor = randomColorNames[3];
              box5[0].style.backgroundColor = randomColorNames[4];
              box6[0].style.backgroundColor = randomColorNames[5];






       }, 400 * i);

};

