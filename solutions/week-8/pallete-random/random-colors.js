
const numStripeColors = 6;
const stripe = document.getElementsByClassName("stripe");
const h1 = document.getElementsByTagName("h1");
const colorize = document.getElementById("colorize")

function stripeAll() {

       for (let i = 0; i < numStripeColors; i++) {
              let hex = "#" + Math.floor(Math.random() * 16777215).toString(16);
              stripe[i].style.backgroundColor = hex
              stripe[i].removeChild(h1[i]); // use textInsert for simple changes
              stripe[i].insertAdjacentHTML('afterbegin', "<h1>" + hex + "</h1>")
       }
}

stripeAll(); // 1st page load
colorize.addEventListener("click", stripeAll); // footer button click

// individual buttons with single event

document.getElementById("buttons").addEventListener("click", function (event) {

       for (let i = 0; i < 6; i++) {

              if (event.target.className === "buttonClass" + (i + 1)) {

                     let hex = "#" + Math.floor(Math.random() * 16777215).toString(16);
                     stripe[i].style.backgroundColor = hex
                     stripe[i].removeChild(h1[i]); //  use textInsert for simple changes
                     stripe[i].insertAdjacentHTML('afterbegin', "<h1>" + hex + "</h1>")
              }

       }
})

// individual buttons with multiple events:

/*

const buttons = document.getElementsByClassName("buttonClass");

for (let i = 0; i < buttons.length; i++) {
       buttons[i].addEventListener('click', function () {
              console.log(i);
              let hex = "#" + Math.floor(Math.random() * 16777215).toString(16);
              stripe[i].style.backgroundColor = hex
              stripe[i].removeChild(h1[i]); //  use textInsert for simple changes
              stripe[i].insertAdjacentHTML('afterbegin', "<h1>" + hex + "</h1>")
       });
}

*/



















