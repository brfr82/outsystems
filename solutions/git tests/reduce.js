/* const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// expected output: 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15 */

/* 
let total = [0, 1, 2, 3].reduce(function(acumulador, valorAtual) {
    return acumulador + valorAtual;
  }, 0)
 // total == 6

 console.log(total) */
/* 
 let reduzido = [[0, 1], [2, 3], [4, 5]].reduce(
    function(acumulador, valorAtual) {
      return acumulador.concat(valorAtual)
    },
    []
  )
  // reduzido é [0, 1, 2, 3, 4, 5]
  console.log(reduzido) */


/*   let names = ['Alice', 'Bob', 'Tiff', 'Bruce', 'Alice'];

let countedNames = names.reduce(function (allNames, name) { 
  if (name in allNames) {
    allNames[name]++;
  }
  else {
    allNames[name] = 1;
  }
  return allNames;
}, {});
// countedNames is:
// { 'Alice': 2, 'Bob': 1, 'Tiff': 1, 'Bruce': 1 }

console.log(countedNames) */

/* 
let pessoas = [
    { nome: 'Alice', idade: 21 },
    { nome: 'Max', idade: 20 },
    { nome: 'Jane', idade: 20 }
  ];
  
  function agruparPor(objetoArray, propriedade) {
    return objetoArray.reduce(function (acc, obj) {
      let key = obj[propriedade];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }
  
  let grupodePessoas = agruparPor(pessoas, 'idade');

  console.log(grupodePessoas) */
// grupodePessoas é:
// { 
//   20: [
//     { nome: 'Max', idade: 20 }, 
//     { nome: 'Jane', idade: 20 }
//   ], 
//   21: [{ nome: 'Alice', idade: 21 }] 
// }


var people = [
    {
        name: 'Anna',
        age: 22
    },
    {
        name: 'Tom',
        age: 34
    }, {
        name: 'John',
        age: 16
    },
]


var adultsAvgAge = people
    .filter(function (person) {
        return person.age > 18;
    })
/*     .reduce(function (acc, person) {
        acc = acc + person.age
        return acc
    })
 */



console.log(adultsAvgAge)

