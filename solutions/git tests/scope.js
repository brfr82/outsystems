/* var x = 10; 
function test() 
{ 
    var x = 20; 
} 
  
test(); 
console.log(x);  */



/* var x = 10; 
  
function test() 
{ 
    if (x > 20) { 
        var x = 50; 
    } 
  
    console.log(x); 
} 
  
test();    */

var x = 10;

function test(x) {

    var x = 21;

    if (x > 20) {
        var x = 50;
    }

    console.log(x);

    // var x = 21; -> different result

}
test();
test(5);
test(25);   