
//https://www.freecodecamp.org/news/write-your-own-promisify-function-from-scratch/

/* const getSumAsync = (num1, num2, callback) => {
 
    if (!num1 || !num2) {
      return callback(new Error("Missing arguments"), null);
    }
    return callback(null, num1 + num2);
  }
  getSumAsync(1, 1, (err, result) => {
    if (err){
      doSomethingWithError(err)
    }else {
      console.log(result) // 2
    }
  })
 */

// Using Node.js's util.promisify():

const { promisify } = require('util')
const getSumPromise = promisify(getSumAsync) // step 1
getSumPromise(1, 1) // step 2
    .then(result => {
        console.log(result)
    })
    .catch(err => {
        doSomethingWithError(err);
    })


