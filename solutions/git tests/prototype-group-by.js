var produtos = [
  {id: 123, nome: 'Camiseta', cor: 'preto', tamanho: 'G', categoria: 'Vestuário'},
  {id: 125, nome: 'Shorts', cor: 'preto', tamanho: 'G', categoria: 'Vestuário'},
  {id: 456, nome: 'Tênis', cor: 'preto', tamanho: '41', categoria: 'Vestuário'},
  {id: 789, nome: 'Bola', cor: 'verde', tamanho: 'Único', categoria: 'Esporte'}
]




Array.prototype.groupBy = function(prop) {
  var value = this.reduce(function(total, item) {
  var key = item[prop];
    total[key] = (total[key] || []).concat(item);

    return total;
  }, {});

  return value;
}




var produtosFiltrados = produtos.filter(function(item) {
  return item.cor == 'preto';
}).filter(function(item) {
  return item.tamanho == 'G';
}).groupBy('categoria');


console.log(produtosFiltrados)
