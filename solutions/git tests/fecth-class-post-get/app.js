
// Instantiating EasyHTTP 
const http = new EasyHTTP; 
  
// Get prototype method  
http.get('https://jsonplaceholder.typicode.com/users') 
  
    // Resolving promise for response data 
    .then(data => console.log(data)) 
  
    // Resolving promise for error 
    .catch(err => console.log(err)); 
  
// Data for post request 
const data = { 
    name: 'selmon_bhoi', 
    username: '_selmon', 
    email: 'selmonbhoi@gmail.com'
} 
  
// Post prototype method  
http.post( 
    'https://jsonplaceholder.typicode.com/users', 
    data) 
  
    // resolving promise for response data 
    .then(data => console.log(data)) 
  
    // resolving promise for error 
    .catch(err => console.log(err)); 
