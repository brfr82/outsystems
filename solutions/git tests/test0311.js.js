/* const obj = {n:1}

obj.n = 2;

obj.a = 3;



console.log(obj); */

/* const obj = {n:1}

try {
    obj.n = 2;
} catch (err) {
  console.log(err);
  // expected output: TypeError: invalid assignment to const `number'
  // Note - error messages will vary depending on browser
}

console.log(obj);
// expected output: 42
 */

/* console.log("     ")

var obj = { n: 1 };

console.log('keep', obj);

{
    const obj = { n: 2 };
    console.log(obj);

    {
        const obj = { n: 3 };
        console.log(obj);

    }
}
console.log('keep', obj);

console.log("     ")

var obj = { n: 1 };

console.log('change', obj);

{
    const obj = { n: 2 };  // changes the global scope, with cons: SyntaxError: Identifier 'obj' has already been declared
    console.log(obj);

    {
        var obj = { n: 3 }; // with VAR: error Identifier 'obj' has already been declared  (without var: Type ERROR Assignment to constant variable.)
        console.log(obj);

    }
}

console.log('change', obj);  // thread  */



// You can create a const object:
const car = {type:"Fiat", model:"500", color:"white"};

// You can change a property:
car.color = "red";
car[1] = {type:"Ford", model:"accor", color:"black"};
car['john'] = {type:"Ford", model:"accor", color:"black"};

// You can add a property:
car.owner = "Johnson";
//car[1].lease = "Bank ABC";

//const car = {type:"Fiat", model:"500", color:"white"};
//car = {type:"Volvo", model:"EX60", color:"red"};    // ERROR you can NOT reassign a constant object:


console.log(car)


// You can create a constant array:
const cars = ["Saab", "Volvo", "BMW"];

// You can change an element:
cars[0] = "Toyota";

// You can add an element:
cars.push("Audi");

//const car = { type: "Fiat", model: "500", color: "white" };
//car = { type: "Volvo", model: "EX60", color: "red" };    // ERROR you can NOT reassign a constant array:

console.log(cars)


