
// param -> auto generate HTML documentation , hover function details




/// API location : 
// window.location (where am I in the app: eg host, href),
// window.history (navigation history)


const items = ["one", "two", "three"]  // items used to make the html elements


const pages = { list: viewList, details: viewDetails };


render(pages.list);  // change this to any page, viewList


/**
 * 
 * renders a page into #root element
 * 
 * @param {Function} page
 * @param {Anything} [pageArgs] // optional , eg. IDs
 */

function render(page, pageArgs) {

    const root = document.getElementById("root");

    root.innerHTML = ""
    page(root, pageArgs)
}



/**
 *  renders the list page
 * 
 * @param {HTMLElement} root
 *
 *
 */

function viewList(root) {
    const titleEl = document.createElement("h1");
    titleEl.textContent = "list page"

    const listEl = createList(items);

    root.appendChild(titleEl)
    root.appendChild(listEl)

}

function viewDetails(root, item) {

    const titleEl = document.createElement("h1");
    titleEl.textContent = "details page"


    // change here for page details, use ITEM

    // one, two, three
    // go back link



    const listEl = createList(items);

    root.appendChild(titleEl)
    root.appendChild(listEl)

}


/**
 *  renders the DETAILS page of the provided item
 * 
 * @param {HTMLElement} root
 * @param {String} item
 *  
 *
 */



function createList(items) {

    const listEl = document.createElement("ul")

    const listItemsEls = items.map((item) => {
        const el = document.createElement("li")
        const anchorEl = document.createElement("a")
        anchorEl.textContent = item
        anchorEl.href = `/${item}`


        el.appendChild(anchorEl)
        anchorEl.addEventListener("click", (e) => {
            render(pages.details, item)
            
           /* change the href */
            history.pushState({id:item}, `Details from ${item.name}`), anchorEl.href
            e.preventDefault();
        })

        return el
    })

    listItemsEls.forEach(itemEl => {
        listEl.appendChild(itemEl);
    });


    return listEl;


}



/*

location.replace ( "http://127.0.0.1:5500/batata?name=sara") //change the current url property
location.reload() 
console.log("HOST", location.host)
console.log("HREF", location.href)
console.log("SEARCH", location.search)


// checks history, without new history creation
history.back(); // estamos a disponibilizar esta funcionalidade no browser
history.forward(); // estamos a disponibilizar esta funcionalidade no browser
history.go(0); // reload
history.go(1);
history.go(-2);


// client side routing
// push state - recent to browser - adds history to the browser (even without new server requests)
history.pushState(null, "", "/batata")
history.replaceState(null, "Title1", "/cenoura") // pops the last url and replaces it
history.state // eg, used for logins
history.state.id  //
history.pushState({id:1})


add some history then add listener
history.pushState(null, "batata", "/batata")
history.pushState(null, "cenoura", "/cenoura")
history.pushState(null, "cenoura", "/index.html")

*/



window.addEventListener("popstate", showHistory) // dispara sempre que interage com history

function showHistory (event) {
    console.log("here")
}








