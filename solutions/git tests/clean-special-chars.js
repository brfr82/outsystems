
function cleanUpSpecialChars(str)
{
    return str
        .replace(/[ÀÁÂÃÄÅ]/g,"A")
        .replace(/[àáâãäå]/g,"a")
        .replace(/[ÈÉÊË]/g,"E")
        //.... all the rest
        .replace(/[^a-z0-9]/gi,''); // final clean up
}



cleanUpSpecialChars('Olá José. Até logo João');