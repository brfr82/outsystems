// https://ui.dev/javascript-visualizer/

//The Ultimate Guide to Hoisting, Scopes, and Closures in JavaScript
//https://ui.dev/ultimate-guide-to-execution-contexts-hoisting-scopes-and-closures-in-javascript/


// http://www.phpmind.com/blog/2017/05/understanding-javascript-closure/
var globalVariable = 'Global variable.';
function outerFunction() {
  var outerFunctionVariable = 'Outer function variable.';
  
  function innerFunction () {
    var innerFunctionVariable = 'Inner function variable.';
    console.log("This is " + innerFunctionVariable);
    console.log("I can also access " + outerFunctionVariable);
    console.log("This is a " + globalVariable);
  };
  innerFunction();
};
outerFunction();



//Code – 2.11:&nbsp; Function factory  https://rahuldotout.wordpress.com/2011/05/15/professional-javascript-part-5-closures/
function makeAdder(x) {
  return function(y) { //returning a closure, where x is bound and y is free
        return x + y;
  };
}

var add5 = makeAdder(5); //add5 is a closure where x=5
var add10 = makeAdder(10); //add10 is a closure where x=10
print(add5(2));  // 7
print(add10(2)); // 12


// https://laptrinhx.com/an-introduction-to-javascript-closures-3961395194/

function counter() {
  let value = 0;
  return function(incrementer) {
    value += incrementer;
    console.log(value)
  }
}
let myCount = counter();
myCount(4); // 4
myCount(4); // 8


// https://www.kirupa.com/html5/closures_in_javascript.htm

function calculateRectangleArea(length, width) {
  return length * width;
}

var roomArea = calculateRectangleArea(10, 10);
alert(roomArea);



