
async function fetchGitHubUser(login) {
    const api = `https://api.github.com/users/${login}`;
    const response = await fetch(api);
    const body = await response.json();

    if (!response.ok) {
        throw new Error(body.message); // throwing inside async rejects the returned promise
    }

    return body;
}

async function showGitHubUser(login) {
    try {
        const user = await fetchGitHubUser(login);
        console.log(`${user.name} is from ${user.location}`);
    } catch (err) {
        console.log(err.message);
    }
}

showGitHubUser('microsoft');



/*
async function fetchGitHub(endpoint) {

    const api = `https://api.github.com/${endpoint}`;

    const response = await fetch(api);
    return response.json(); // return the promise, no need to await
}

async function showRepos(login) {

    const user = await fetchGitHub(`users/${login}`);
    const repos = await fetchGitHub(`users/${login}/repos`);

    console.log(`${user.name} repositories:`);
    repos.forEach((repo) => console.log(repo.name));
}

showRepos('microsoft');

*/

/*
{
  "login": "microsoft",
  "id": 6154722,
  "node_id": "MDEyOk9yZ2FuaXphdGlvbjYxNTQ3MjI=",
  "avatar_url": "https://avatars2.githubusercontent.com/u/6154722?v=4",
  "gravatar_id": "",
  "url": "https://api.github.com/users/microsoft",
  "html_url": "https://github.com/microsoft",
  "followers_url": "https://api.github.com/users/microsoft/followers",
  "following_url": "https://api.github.com/users/microsoft/following{/other_user}",
  "gists_url": "https://api.github.com/users/microsoft/gists{/gist_id}",
  "starred_url": "https://api.github.com/users/microsoft/starred{/owner}{/repo}",
  "subscriptions_url": "https://api.github.com/users/microsoft/subscriptions",
  "organizations_url": "https://api.github.com/users/microsoft/orgs",
  "repos_url": "https://api.github.com/users/microsoft/repos",
  "events_url": "https://api.github.com/users/microsoft/events{/privacy}",
  "received_events_url": "https://api.github.com/users/microsoft/received_events",
  "type": "Organization",
  "site_admin": false,
  "name": "Microsoft",
  "company": null,
  "blog": "https://opensource.microsoft.com",
  "location": "Redmond, WA",
  "email": "opensource@microsoft.com",
  "hireable": null,
  "bio": "Open source projects and samples from Microsoft",
  "twitter_username": "OpenAtMicrosoft",
  "public_repos": 3847,
  "public_gists": 0,
  "followers": 0,
  "following": 0,
  "created_at": "2013-12-10T19:06:48Z",
  "updated_at": "2020-09-15T23:19:31Z"
  */