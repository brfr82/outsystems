/*

***  https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=Arsenal_vs_Chelsea  ***  


table HTML : (1970-2020): [Date -  HomeTeam -  AwayTeam - HomeScore -Away Score]

table stats : total wins / draws / loss +  goals Home - goals Away

user can search select year

***  1º registo: ***  

strHomeTeam	"Arsenal"
strAwayTeam	"Chelsea"
intHomeScore	"2"
intAwayScore	"1"
dateEvent	"2020-12-26"

***  último registo: ***  

strHomeTeam	"Arsenal"
strAwayTeam	"Chelsea"
intHomeScore	"0"
intAwayScore	"3"
dateEvent	"1970-01-17"

*/