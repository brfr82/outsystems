// With ES6 Promises
function fetchUser(login) {

    const api = 'https://api.github.com/users';
  
    return fetch(`${api}/${login}`)
        .then(response => response.json())
        .then(user => ({
            name: user.name,
            location: user.location
        }));
  }
// With ES2017 async/await
async function fetchUser(login) {

    const api = 'https://api.github.com/users';
    const response = await fetch(`${api}/${login}`);
    const user = await response.json();
  
    return {
        name: user.name,
        location: user.location
    };
  }


  // We can only await on an async function inside another async function
  async function sleep(timeout) {
    return new Promise((resolve) => setTimeout(resolve, timeout));
}

(async function() {
    await sleep(1000); // wait for 1 second
    console.log('Done!');
})();



// non-Promise values are wrapped in a Promise
let value1 = (async () => { return await 42; })();
let value2 = (async () => { return 42; })();

console.log(value1 instanceof Promise); // true
console.log(value2 instanceof Promise); // true


// Mutiple promises can be executed sequentially using the await operator

async function fetchGitHub(endpoint) {

    const api = `https://api.github.com/${endpoint}`;

    const response = await fetch(api);
    return response.json(); // return the promise, no need to await
}

async function showRepos(login) {

    const user = await fetchGitHub(`users/${login}`);
    const repos = await fetchGitHub(`users/${login}/repos`);

    console.log(`${user.name} repositories:`);
    repos.forEach((repo) => console.log(repo.name));
}


//  Promise.all method can be used to fire multiple promises in parallel
async function fetchGitHub(endpoint) {

    const api = `https://api.github.com/${endpoint}`;

    const response = await fetch(api);
    return response.json(); // return the promise, no need to await
}

async function showRepos(login) {

    const [user, repos] = await Promise.all([
        fetchGitHub(`users/${login}`),
        fetchGitHub(`users/${login}/repos`)
    ]);

    console.log(`${user.name} repositories:`);
    repos.forEach((repo) => console.log(repo.name));
}

// Async functions return a rejected promise when an error is thrown

async function fetchGitHubUser(login) {
    const api = `https://api.github.com/users/${login}`;
    const response = await fetch(api);
    const body = await response.json();

    if (!response.ok) {
        throw new Error(body.message); // throwing inside async rejects the returned promise
    }

    return body;
}

// try/catch blocks can be used to handle errors in asynchronous functions

async function showGitHubUser(login) {
    try {
        const user = await fetchGitHubUser(login);
        console.log(`${user.name} is from ${user.location}`);
    } catch (err) {
        console.log(err.message);
    }
}


// Looping with async/await


async function randomNums(howMany) {
    let results = [];
    for (let i = 0; i < howMany; i++) {
        await sleep(1000);
        results.push(Math.random());
    }
    return results;
}

console.log(await randomNums(10)); // 10 random numbers after 10 seconds


//  But things do not work quite as expected inside a forEach or other array methods...


[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach(async (num) => {
    await sleep(1000)
    console.log(num)
});

console.log('Finished!');






// ES 2017 async / await  (vs Es 2016 promises)

// ES 2017 async / await  (vs Es 2016 promises)


/*

async function fetchGithub (endpoint) {

    const url = `https://api.github.com/users/${login}`;

    const api = url;

    const response  = await fetch(api);
    const user = await response.json();


    if (!response.ok) { // // SATUS CODE "OK" :  400 - 4xx...
        throw new Error(body.message); //   throwing inside async reject the returned promise
    }
    
    return {
        name: user.name,
        location: user.location,
    }
}

async function showRepos(login) {

    try {
        const [user, repos] = await Promise.All([
            fetchGithub(`users/${login}`),
            fetchGithub(`users/${login}/repos`)
        ])

    }
    catch (err){
        console.log(err.message);
    }

}

showRepos('talefe');


// { Exercise } AJAX with async/await


*/


// ToDo https://apilist.fun/

// Todo convert promises to ASync- await in past exercises

// filter API data (Json) and DOM manipulation : array methods (filter, map, reduce, foreach)

/*

FIREFOX 

https://apilist.fun/api/football-soccer-video-api-by-scorebat
https://apilist.fun/api/footystats-api-soccer-stats-data
https://apilist.fun/api/football-prediction
https://apilist.fun/api/the-sports-db


https://rapidapi.com/boggio-analytics/api/football-prediction/details


https://rapidapi.com/marketplace

https://rapidapi.com/search/SOCCER


https://www.thesportsdb.com/api.php?ref=apilist.fun

***  https://www.thesportsdb.com/api/v1/json/1/searchevents.php?e=Arsenal_vs_Chelsea  ***  


Tabela HTML : (1970-2020): [Date -  HomeTeam -  AwayTeam - HomeScore -Away Score]

Tabela resumo/estatísticas : total wins / draws / loss +  goals Home - goals Away

***  1º registo: ***  

strHomeTeam	"Arsenal"
strAwayTeam	"Chelsea"
intHomeScore	"2"
intAwayScore	"1"
dateEvent	"2020-12-26"

***  último registo: ***  

strHomeTeam	"Arsenal"
strAwayTeam	"Chelsea"
intHomeScore	"0"
intAwayScore	"3"
dateEvent	"1970-01-17"




https://www.football-data.org/index?ref=apilist.fun

Free Tier
12 competitions
Basic data (Fixtures, Results and League Tables)
10 API calls per minute

https://apifreesoccer.com/











*/
