// sort city (ascending) & then price (descending)

var homes = [
    {"h_id":"3",
     "city":"Dallas",
     "price":"162500"},
    {"h_id":"4",
     "city":"Bevery Hills",
     "price":"319250"},
    {"h_id":"6",
     "city":"Dallas",
     "price":"556699"},
    {"h_id":"5",
     "city":"New York",
     "price":"962500"}
    ];


homes.sort(
    function(a, b) {          
       if (a.city === b.city) {
          // Price is only important when cities are the same
          return b.price - a.price;
       }
       return a.city > b.city ? 1 : -1;
    });


    homes.sort(
        function(a,b){
           if (a.city==b.city){
              return (b.price-a.price);
           } else {
              return (a.city-b.city);
           }
        });

console.log(homes)