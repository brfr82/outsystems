/* const arr = [
    {
        StatusYear: 2018,
        Requests: 55,
        Approved: 33,
        NotApproved: 22,
        IP: 22,
        ELA: 0,
        CapAvoid: 0
      },
    {
      StatusYear: 2018,
      Requests: 55,
      Approved: 33,
      NotApproved: 22,
      IP: 22,
      ELA: 0,
      CapAvoid: 0
    },
    {
      StatusYear: 2019,
      Requests: 611,
      Approved: 422,
      NotApproved: 189,
      IP: 167,
      ELA: 0,
      CapAvoid: 4851118
    },
    {
      StatusYear: 2020,
      Requests: 1166,
      Approved: 724,
      NotApproved: 217,
      IP: 411,
      ELA: 219,
      CapAvoid: 16646386
    }
  ];
  
  const fin = arr.reduce(
    (acc, cur) => {
      const p = acc;
      p.Requests.push(cur.Requests);
      p.Approved.push(cur.Approved); 
      p.NotApproved.push(cur.NotApproved);
      p.StatusYear.push(cur.StatusYear);

      return p;
    },
    {
      StatusYear: [],

      Requests: [],
      Approved: [],
      NotApproved: [],

 
    }
  );
  console.log(fin); */
/* 
  arr =[{"category":"Amazon","month":"Feb","total":9.75},
  {"category":"Amazon","month":"Feb","total":9.75},
  {"category":"Amazon","month":"Mar","total":169.44},
  {"category":"Amazon","month":"Apr","total":10.69},
  {"category":"Amazon","month":"May","total":867.0600000000001},
  {"category":"Amazon","month":"Jun","total":394.43999999999994},
  {"category":"Amazon","month":"Jul","total":787.2400000000001},
  {"category":"Amazon","month":"Aug","total":1112.4400000000003},
  {"category":"Amazon","month":"Sep","total":232.86999999999998},
  {"category":"Amazon","month":"Oct","total":222.26999999999998},
  {"category":"Amazon","month":"Nov","total":306.09999999999997},
  {"category":"Amazon","month":"Dec","total":1096.2599999999998}]

  var o = arr.reduce( (a,b) => {
    a[b.category] = a[b.category] || [];
    a[b.category].push({[b.month]:b.total});
    return a;
}, {});

var a = Object.keys(o).map(function(k) {
    return {category : k, month : Object.assign.apply({},o[k])};
});

console.log(a);
   */



/* So if I did groupby Phase, I’d want to receive:

[
    { Phase: "Phase 1", Value: 50 },
    { Phase: "Phase 2", Value: 130 }
]

And if I did groupy Phase / Step, I’d receive:

[
    { Phase: "Phase 1", Step: "Step 1", Value: 15 },
    { Phase: "Phase 1", Step: "Step 2", Value: 35 },
    { Phase: "Phase 2", Step: "Step 1", Value: 55 },
    { Phase: "Phase 2", Step: "Step 2", Value: 75 }
] */





/**
 * @description
 * Takes an Array<V>, and a grouping function,
 * and returns a Map of the array grouped by the grouping function.
 *
 * @param list An array of type V.
 * @param keyGetter A Function that takes the the Array type V as an input, and returns a value of type K.
 *                  K is generally intended to be a property key of V.
 *
 * @returns Map of the array grouped by the grouping function.
 */
//export function groupBy<K, V>(list: Array<V>, keyGetter: (input: V) => K): Map<K, Array<V>> {
//    const map = new Map<K, Array<V>>();
// function groupBy(list, keyGetter) {
//     const map = new Map();
//     list.forEach((item) => {
//          const key = keyGetter(item);
//          const collection = map.get(key);
//          if (!collection) {
//              map.set(key, [item]);
//          } else {
//              collection.push(item);
//          }
//     });
//     return map;
// }


// // example usage

// const pets = [
//     {type:"Dog", name:"Spot"},
//     {type:"Cat", name:"Tiger"},
//     {type:"Dog", name:"Rover"}, 
//     {type:"Cat", name:"Leo"}
// ];
    
// const grouped = groupBy(pets, pet => pet.type);
    
// console.log(grouped.get("Dog")); // -> [{type:"Dog", name:"Spot"}, {type:"Dog", name:"Rover"}]
// console.log(grouped.get("Cat")); // -> [{type:"Cat", name:"Tiger"}, {type:"Cat", name:"Leo"}]

// const odd = Symbol();
// const even = Symbol();
// const numbers = [1,2,3,4,5,6,7];

// const oddEven = groupBy(numbers, x => (x % 2 === 1 ? odd : even));
    
// console.log(oddEven.get(odd)); // -> [1,3,5,7]
// console.log(oddEven.get(even)); // -> [2,4,6]


/* 
var DataGrouper = (function() {
    var has = function(obj, target) {
        return _.any(obj, function(value) {
            return _.isEqual(value, target);
        });
    };

    var keys = function(data, names) {
        return _.reduce(data, function(memo, item) {
            var key = _.pick(item, names);
            if (!has(memo, key)) {
                memo.push(key);
            }
            return memo;
        }, []);
    };

    var group = function(data, names) {
        var stems = keys(data, names);
        return _.map(stems, function(stem) {
            return {
                key: stem,
                vals:_.map(_.where(data, stem), function(item) {
                    return _.omit(item, names);
                })
            };
        });
    };

    group.register = function(name, converter) {
        return group[name] = function(data, names) {
            return _.map(group(data, names), converter);
        };
    };

    return group;
}());

DataGrouper.register("sum", function(item) {
    return _.extend({}, item.key, {Value: _.reduce(item.vals, function(memo, node) {
        return memo + Number(node.Value);
    }, 0)});
}); */


/* data = [ 
    { Phase: "Phase 1", Step: "Step 1", Task: "Task 1", Value: "5" },
    { Phase: "Phase 1", Step: "Step 1", Task: "Task 2", Value: "10" },
    { Phase: "Phase 1", Step: "Step 2", Task: "Task 1", Value: "15" },
    { Phase: "Phase 1", Step: "Step 2", Task: "Task 2", Value: "20" },
    { Phase: "Phase 2", Step: "Step 1", Task: "Task 1", Value: "25" },
    { Phase: "Phase 2", Step: "Step 1", Task: "Task 2", Value: "30" },
    { Phase: "Phase 2", Step: "Step 2", Task: "Task 1", Value: "35" },
    { Phase: "Phase 2", Step: "Step 2", Task: "Task 2", Value: "40" }
] */

//console.log(DataGrouper.sum(data, ["Phase"]));
//console.log(DataGrouper.sum(data, ["Phase", "Step"]));
/* 
const groupedMap = data.reduce(
    (entryMap, e) => entryMap.set(e.id, [...entryMap.get(e.id)||[], e]),
    new Map()
);

console.log(groupedMap) */
/* 
Array.prototype.groupBy = function(keyFunction) {
    var groups = {};
    this.forEach(function(el) {
        var key = keyFunction(el);
        if (key in groups == false) {
            groups[key] = [];
        }
        groups[key].push(el);
    });
    return Object.keys(groups).map(function(key) {
        return {
            key: key,
            values: groups[key]
        };
    });
};

console.log(data.groupBy(data.Step)) */

// /**
//  * @method Array.prototype.groupBy(group)
//  * Creates a dictionary grouping by an attribute or if `group` is a function, what it returns.
//  * @param {function|string} group - attribute name or function(item, index, array): string
//  * @returns {object.<array>}
//  */
// if (!Array.prototype.groupBy) {
// 	Object.defineProperty(Array.prototype, 'groupBy', {
// 		enumerable: false,
// 		value: function groupBy(group) {
// 			return this.reduce(function (hash, item, index, array) {
// 				var key = typeof group === 'function' ? group(item, index, array) : String(item[group]);

// 				if (!hash[key]) {
// 					hash[key] = [];
// 				}

// 				hash[key].push(item);

// 				return hash;
// 			}, Object.create(null));
// 		}
// 	});
// }

// const data = [{
//     name: "A",
//     id: "q1",
//     history: {
//       "1:2:3": {
//         a: 0,
//         b: 0,
//         c: 0
//       },
//       "4:5:6": {
//         a: 1,
//         b: 1,
//         c: 1
//       },
//       "7:8:9": {
//         a: 2,
//         b: 2,
//         c: 2
//       }
//     }
//   },
//   {
//     name: "B",
//     id: "q2",
//     history: {
//       "1:2:3": {
//         a: 3,
//         b: 3,
//         c: 3
//       },
//       "4:5:6": {
//         a: 4,
//         b: 4,
//         c: 4
//       },
//       "7:8:9": {
//         a: 5,
//         b: 5,
//         c: 5
//       }
//     }
//   },
//   {
//     name: "C",
//     id: "q3",
//     history: {
//       "1:2:3": {
//         a: 6,
//         b: 6,
//         c: 6
//       },
//       "4:5:6": {
//         a: 7,
//         b: 7,
//         c: 7
//       },
//       "7:8:9": {
//         a: 8,
//         b: 8,
//         c: 8
//       }
//     }
//   }
// ]

// const result = data.reduce((acc, item) => {

//   const keys = Object.keys(item.history);

//   for (let key of keys) {
//     acc[key] = {
//       ...(acc[key] || {}),
//       [item.id]: item.history[key]
//     }
//   }

//   return acc;
// }, {})

// console.log(result);


const data = [ 
    { Phase: "Phase 1", Step: "Step 1", Task: "Task 1", Value: "5" },
    { Phase: "Phase 1", Step: "Step 1", Task: "Task 2", Value: "10" },
    { Phase: "Phase 1", Step: "Step 2", Task: "Task 1", Value: "15" },
    { Phase: "Phase 1", Step: "Step 2", Task: "Task 2", Value: "20" },
    { Phase: "Phase 2", Step: "Step 1", Task: "Task 1", Value: "25" },
    { Phase: "Phase 2", Step: "Step 1", Task: "Task 2", Value: "30" },
    { Phase: "Phase 2", Step: "Step 2", Task: "Task 1", Value: "35" },
    { Phase: "Phase 2", Step: "Step 2", Task: "Task 2", Value: "40" },
]

Array.prototype.groupBy = function(keyFunction) {
    const groups = {};
    this.forEach(function(el) {
        const key = keyFunction(el);
        if (key in groups == false) {
            groups[key] = [];
        }
        groups[key].push(el);
    });
    return Object.keys(groups).map(function(key) {
        return {
            key: key,
            values: groups[key]
        };
    });
};

console.log(data.groupBy(data.Step))