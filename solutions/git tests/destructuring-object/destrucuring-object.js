//https://kula.blog/learnjs/destructuring_7/

const adminUsers = [
    {
      reallyLongExampleProperty: "foo",
      anotherLongPropertyName: 10
    }, {
      reallyLongExampleProperty: "bar",
      anotherLongPropertyName: 34
    }
  ]
  
  adminUsers.forEach((admin) => {
    //console.log(`${admin.reallyLongExampleProperty} ${admin.anotherLongPropertyName}`)
  });

  const reallyLongExampleProperty = []
  const anotherLongPropertyName = []
  // So in practice what you do is:
  adminUsers.forEach((admin) => {
    reallyLongExampleProperty.push(admin.reallyLongExampleProperty);
    anotherLongPropertyName.push(admin.anotherLongPropertyName);
 
  });


  console.log(`__keys:${reallyLongExampleProperty}  __properties: ${anotherLongPropertyName}`)
