/* var str = "Para maiores informações, veja o Capítulo 3.4.5.1, , veja o Capítulo 3.4.5.1 Para maiores informações, veja o Capítulo 3.4.5.1, Para maiores informações, veja o Capítulo 3.4.5";
var re = /(capítulo \d+(\.\d)*)/i;
var found = str.match(re);

console.log(found);

// retorna ["Capítulo 3.4.5.1", 
      /*       "Capítulo 3.4.5.1", 
            ".1",
            index: 33,
            input: "Para maiores informações, veja o Capítulo 3.4.5.1"] */

// "Capítulo 3.4.5.1" é a primeira correspondência e o primeiro valor 
//  capturado a partir de (capítulo \d+(\.\d)*).
// ".1" é o útlimo valor de (\.\d).
// A propriedade "index" (33) é o índice de base zero da correspôndencia inteira.
// A propriedade "input" é a string original que foi analisada. */

/* var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzuuuuuuuuuuuuuuuuuuBBBBBBB";
var regexp = /([A-E]|u)/gi;
var matches_array = str.match(regexp);

console.log(matches_array); 
// ['A', 'B', 'C', 'D', 'E', 'u', 'w', 'x', 'y', 'q'] */


const str = 'hello world!';
const result = /^hello/.test(str);

console.log(result); // true

