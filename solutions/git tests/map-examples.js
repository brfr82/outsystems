var numbers = [1, 4, 9];
var roots = numbers.map(Math.sqrt);
// roots é [1, 2, 3], numbers ainda é [1, 4, 9]



var map = Array.prototype.map;
var a = map.call('Hello World', function(x) { return x.charCodeAt(0); });
// a agora vale [72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100]


/* var elems = document.querySelectorAll('select option:checked');
var values = [].map.call(elems, function(obj) {
  return obj.value;
}); */


var str = '12345';
[].map.call(str, function(x) {
  return x;
}).reverse().join(''); 

// Output: '54321'
// Bonus: utilize '===' para verificar se a string original e a nova string são palíndromos