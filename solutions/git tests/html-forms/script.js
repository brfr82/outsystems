
// ToDo CHAT / twitter style webserver -> POST -> GET
/*

POST the data:
 
nick
message
submit

show messages below:

format messages with: GET requests (from where they are stored, with object or array)



[20/12/02 12:56] sara: "hello friends"

- by manipulating the  payload variable (data) with DOM (hint: adjacent HTML)

hint: use scrool box

*/


// usar um algoritmo de hashing para encriptar dados do formulario: unidirecional vs bidireccional



/* // forma antiga de guardar dados:
const username = document.getElementById("username");
console.log(username.attributes.length);
console.log(username);
console.log(username.content);
console.log(username);
*/


// copiar o servidor criado nas semanas anterior

/* criar um objecto que vai ser a nossa rout (um array) para duas rotas GET e POST */



const routes = [{ method: 'GET', path: '/', handler: handleGetRequest }, // not defined yet
{ method: 'POST', path: '/', handler: handlePostRequest } // not defined yet

]


// função para inspeccionar o URL que vem no request, aceder ao array, match mehthod & url,


function getRoutHandler(method, url) {  // opcional argument: request
    return routes.find((route) => route.method === method && route.path === url).handler;
}


// criar as 2 funções de route


function handleGetRequest(request, response){

    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method);

    let url = request.url;
    //let method = request.method; //unused
    if (url === "/") {
        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        });
    }
    else if (fileNames.includes(url.split('/').pop())) {
        fs.readFile(url.substring(1), function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType }); // remove mimetype variable => text only
            response.write(content);
            response.end();
        });

    }
    else {
        response.writeHead(404);
        response.write('page unavailable');
        response.end();
    }
    

}

function handlePostRequest(request, response){

    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method);
    
    // adicionar um evento listener = qunado houver dados executar a função: concatenar ao Payload
    
    let payload = ""
    request.on('data', function(chunk){ payload = payload.concat(chunk)}) //concat não altera variavel original
    console.log("post handler invoked")

    // ao terminar de ler os dados todos do payload:
    request.on("end", function(){console.log(payload)})
    
}


// 1.º iniciar o server no terminal ( node script.js)

// 2.º iniciar o web browser / live server ? / 


// 3.º inspectionar TAB networking da consola;


/////////////////////////////////////////////

const http = require ('http'); // check if nmp modules is correclty installed
const fs = require('fs');


const PORT = 8080;
const server = http.createServer(handler)

server.listen(PORT);
console.log('server listening @port' + PORT);

function handler(request, response) {

    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method);

    const {method, url} = request;
    const routeHandler = getRoutHandler(method, url);
    routeHandler (request, response);

}


