// https://github.com/erossignon/serialijse
//https://medium.com/better-programming/serialization-and-deserialization-ba12fc3fbe23


const usersObject = {
    "total": 2,
    "users": [
        {
            "name": "Patrick"
        },
        {
            "name": "Michael"
        }
    ]
}

console.log(JSON.stringify(usersObject))

// "{"total":2,"users":[{"name":"Patrick"},{"name":"Michael"}]}"

console.log(JSON.parse("{\"total\":2,\"users\":[{\"name\":\"Patrick\"},{\"name\":\"Michael\"}]}")); // responseFromServer



// Fetch API (client side, always available)

function getData(onSuccess, onError) {  // GET

    fetch(url)
        .then(response => response.json) // anteriormente: response.json => JSON.parse(response)
        .then(onSuccess) // load new data to page(DOM manipulation)
        .catch(onError)
}

/*  
form.addEventListener('submit', (event) => getData(loadMessagesDom, loadErrorDOM))

    event.preventDefault();
    // do something:

function loadMessagesDom(){}
function loadErrorDOM(){}



// USE: *******  prevent default ******* (don't let HTML form befault behaviour, but to consume the API)


*/





function postData(data, onSuccess, onError) { // POST
    fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    })
        .then(onSuccess) // load new data to page(DOM manipulation)
        .catch(onError)
}



// ToDO 1: EXCHANGE RATE server : optional use LIVESERVER instead of creating one.
// insert EUR: 33 ,   (Optional: convert button, dropdown list select currency)
// data validation (numbers only)
// 33 EUR = 39.3242343 USD (instead of button: use Ajax to real time change)
// ON ERROR = ERROR FECTHING RATES (RED)
// https://api.exchangeratesapi.io/latest


//ToDo 2: use github API for requests: ENDPOINTS available 
//https://docs.github.com/pt/free-pro-team@latest/rest/overview/endpoints-available-for-github-apps
//https://api.github.com/users/talefe
// Server/App. search gtihub users : [talefe]
// show list of users ordered by score (find the SCORE code in the API)
// TypeError= Failed to fectch


//Exemplo: https://gist.github.com/paulmillr/2657075
/*

githubUsers
  .filter(user => user.followers > 1000)
  .sortBy('contributions')
  .slice(0, 256)

  */



  // acces to all proprieties of object:

  const properties = Object.keys(usersObject);

  console.log(properties) // use to get data for drop down menu