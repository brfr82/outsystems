

/* 

"add event listener" / "remove  event listener"- standard method used see mdn
Get started
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Building_blocks/Events



This article offers a list of events that can be sent; 
some are standard events defined in official specifications,
 while others are events used internally by specific browsers
https://developer.mozilla.org/en-US/docs/Web/Events


WEB API - The Event() constructor creates a new Event.
https://developer.mozilla.org/en-US/docs/Web/API/Event/Event

*/



/* const container = document.querySelector("#container");
const child = document.querySelector("#child");


container.addEventListener("mouseenter", highlightContainer);
child.addEventListener("mouseenter", highlightContainer);

// https://www.w3schools.com/jsref/obj_mouseevent.asp


function highlightContainer() {
    console.log("[CONTAINER] highlighted");
    container.classList.add("highlight");
}

container.addEventListener("mouseleave", unHighlightContainer);
child.addEventListener("mouseleave", unHighlightContainer);

function unHighlightContainer() {
    console.log("[CONTAINER] un-highlighted");
    container.classList.remove("highlight");
}  */





// Event bubbling - HTML page -> event propagates -> toggle


container.addEventListener("mouseenter", toggleHighlightClass);
child.addEventListener("mouseenter", toggleHighlightClass);
container.addEventListener("mouseleave", toggleHighlightClass);
child.addEventListener("mouseleave", toggleHighlightClass);

grandchild.addEventListener("mouseenter", toggleHighlightClass);
grandchild.addEventListener("mouseleave", toggleHighlightClass);

function toggleHighlightClass(event) {
    const target = event.currentTarget;
    console.log(target);
    target.classList.toggle("highlight");

} 


// proceed to webconsole analysis: CSS style is injected



// Stop propagation use: https://developer.mozilla.org/en-US/docs/Web/API/Event/stopPropagation

/*
function toggleHighlightClass(event) {
    const target = event.target;
    Event.stopPropagation();
    console.log(target);
    target.classList.toggle("highlight");

} 
*/


// Check some way to stop propagation at certain level or class


 // https://developer.mozilla.org/en-US/docs/Web/API/Document/keypress_event
 // function printKeyDown(event) ....


 

 // Mouse position:  client.X, client.Y

 // function printPointerPosition


 // Exercice: Print the key that is pressed

 // Exercice: Print the mouse position 