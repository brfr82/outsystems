/* function logArrayElements(element, index, array) {
    console.log("a[" + index + "] = " + element);
}
[2, 5, 9].forEach(logArrayElements);
// logs:
// a[0] = 2
// a[1] = 5
// a[2] = 9


function copy(o){


    var copy = Object.create( Object.getPrototypeOf(o) );
    var propNames = Object.getOwnPropertyNames(o);

  
    propNames.forEach(function(name){

      var desc = Object.getOwnPropertyDescriptor(o, name);
      Object.defineProperty(copy, name, desc);

    });
  

    return copy;
  }


  
  var o1 = {a:1, b:2};
  var o2 = copy(o1); // o2 looks like o1 now

  console.log(o2); */

/* 
  var sandwiches = [
	'tuna',
	'ham',
	'turkey',
	'pb&j'
];

sandwiches.forEach(function (sandwich, index) {
	console.log(index);
	console.log(sandwich);
});

// returns 0, "tuna", 1, "ham", 2, "turkey", 3, "pb&j" */


var numeros = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function tabuadaDe2(item) {
    console.log(item*2);
}

numeros.forEach(tabuadaDe2)