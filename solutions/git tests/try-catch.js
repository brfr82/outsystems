function goToTheBeach(isSunny) {

    if (!isSunny) {

        throw new Error("today it is not sunny");

    }

    console.log("drive to the beach");
    console.log("get some sun");

}

/* goToTheBeach(false); 


Call Stack:

Error: today it is not sunny
    at goToTheBeach (C:\Users\user\outsystems\outsystems\solutions\git tests\throw.js:5:11)
    at Object.<anonymous> (C:\Users\user\outsystems\outsystems\solutions\git tests\throw.js:14:1) */


try {
    tellMeGoToBeach(); // send back the error to upper context....
    //goToTheBeach(false);
}

catch (error) {
    console.log(error.message);
    stayHome();
}
function stayHome() {
    console.log("watch netflix");
}


function tellMeGoToBeach() {
    goToTheBeach(false); // no try catch here
}

//tellMeGoToBeach();  // send back the error to global context....