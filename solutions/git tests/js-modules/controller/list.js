// import //  get the data from services, normally api.js 

import {getCharacterList}  from "../service/api.js" // folder in /controller

//populate dom
//import getCharacterList from "../view.js"


async function list(){
    // loading view , until fulfilled
    const data = await getCharacterList();
    // manipulate data, then send it to view
    getCharacterList(data);
}

export default list;