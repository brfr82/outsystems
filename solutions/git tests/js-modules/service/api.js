const API_ENDPOINT = "https://rickandmortyapi.com/api";

async function getCharacterList() {
    const response = await fetch(`${API_ENDPOINT}/character`)
    const data = await response.json();
    return data.result
}

export {getCharacterList}