function greetX() {
    function greet() {
        console.log('Hello world after 3 seconds');
        return 'This message is shown first';
    }

    setTimeout(greet, 3000);
    console.log('This message is shown first');
}

function showTimeX() {
    // program to display time every 3 seconds
    function showTime() {

        // return new date and time
        let dateTime = new Date();

        // returns the current local time
        let time = dateTime.toLocaleTimeString();

        console.log(time)
        //return time

        // display the time after 3 seconds
        setTimeout(showTime, 3000);
    }


    // calling the function
    showTime();
}




function increaseCountX() {
    // program to stop the setTimeout() method
    let count = 0;

    // function creation
    function increaseCount() {

        // increasing the count by 1
        count += 1;
        console.log(count)
        //return count;
    }

    let id = setTimeout(increaseCount, 3000);

    // clearTimeout
    clearTimeout(id);
    console.log('setTimeout is stopped.');
    //return 'setTimeout is stopped.'
}

export { greetX, showTimeX, increaseCountX }