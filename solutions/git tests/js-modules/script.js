// this is the index.js


import list from "./controller/list.js"
import goTo from  "./navigation.js"

goTo("/")

window.addEventListener("popstate", list)





















// MVC with modules: datamodel - > view(HTML inject) + controler(module filters) - > browser
// https://www.oreilly.com/library/view/learning-javascript-design/9781449334840/ch10s02.html
// https://smus.com/reusable-js-mvc-frameworks/
// https://www.edureka.co/blog/javascript-mvc/


// https://github.com/jstat/jstat 
//- >jStat provides native javascript implementations of statistical functions. Full details are available in the docs.

// charts
// https://www.sitepoint.com/best-javascript-charting-libraries/
// https://d3js.org/


// https://mathjs.org/docs/datatypes/matrices.html

// https://www.npmjs.com/package/matrix-multiplication


// GPU.js is a JavaScript Acceleration library for GPGPU (General purpose computing on GPUs) in JavaScript for Web 
// https://gpu.rocks/#/


/*
import {multiply, sum} from './module.js'; // if activated not all modules are available, even if declared

document.getElementById("root").textContent = sum(2,3)
document.getElementById("root").textContent = multiply(2,5) // auto inserts import,
document.getElementById("root").textContent = PI //  if deleted, no execution


import * as moduleName from './module.js';
import * as intervalModule from './interval-module.js';

document.getElementById("root").textContent = moduleName.sum(2,3)
document.getElementById("root").textContent = moduleName.multiply(2,5) // auto inserts import,
document.getElementById("root").textContent = moduleName.PI //  if deleted, no execution

document.getElementById("root").textContent = intervalModule.greetX()

document.getElementById("root").textContent = intervalModule.increaseCountX()
document.getElementById("root").textContent = intervalModule.showTimeX()


import something from './module.js'; // something => export default declared in module.js 
console.log(something); //  PI: 3.14159265
*/