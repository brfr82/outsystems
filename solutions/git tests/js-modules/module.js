const PI = 3.14159265

export function sum(a, b) { // individual export
    return a + b;
}

function square(a) {
    return a * a;
}

function multiply(a, b) {
    dontExportMe();
    return a * b;
}

function dontExportMe(){
    console.log("nope")
}


// use functions in other places

export { square, multiply, PI};

export default {PI}; // it can't be a function declaration