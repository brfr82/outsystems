
import characterListController from "./controller/list.js"

const ROUTES = {
    LIST:  '/',
}

const routes = [ 
    {path: /^\/?$/, init: characterListController}  // match path for app route + init page from module controller
]

// used in render(routes.list)

function getRoute(path) {
    return routes.find (function (route){
        return path.match(route.path); // protect array data from outside the module, encapsulated
    })
} 


export {ROUTES, getRoute}