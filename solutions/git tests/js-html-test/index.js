

window
globalThis 
Document


console.log("hello world");

const container = document.getElementById("container");
console.dir(container);
console.log(container);

container.style = "border: 2px solid black;"

container.style.backgroundColor = "green";// camelCase for CSS is valid
container.style.height = '100px' // cascading rules apply




// classes - array of HTML elements

const container2 = document.getElementById("container2");

container2.style = "border: 2px solid black;"

container2.style.backgroundColor = "red";// camelCase for CSS is valid
container2.style.height = '100px' // cascading rules apply


//ByTagName

// query selector:  #container  .container

const container3 = document.querySelector("div");


//const container3 = document.querySelectorAll;


// traverse through elements and return to original container
//....


//How To create a div element
const divOne = container.firstElementChild;
const message = document.createElement("p");

message.textContent = "Hello NEW world";
container.append(message);



// manage classes
message.setAttribute("src","hello");
message.setAttribute("id", "hello");
message.classList.add("new");
message.classList.add("old");
message.classList.remove("old");
message.classList.toggle("old"); // substituir

// ignore previous classes
message.setAttribute("class", "by")



const body = document.querySelector("body");
body.style["background-color"] = "cyan";
body.style["height"] = "100vh";


// GAME with dynamic tables: https://github.com/Adam-Forest/D3-dynamic-table-creation

// get table's DOM object .send array users to html table

// https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Traversing_an_HTML_table_with_JavaScript_and_DOM_Interfaces


//https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Manipulating_documents

//https://www.w3schools.com/jsref/met_table_insertrow.asp


//https://stackoverflow.com/questions/8302166/dynamic-creation-of-table-with-dom


var userTable = document.getElementById('users-table');

// add a new row

//var row = userTable.insertRow(users); //error

var users = [ {name: 'bruno', email: 'brf@live.com.pt'}]

//row.innerHTML


/*

https://htmldom.dev/
https://www.infoq.com/news/2020/04/howto-html-dom-vanilla-js/
https://github.com/neon010/Simple-javascript-DOM-manipulation-projects


https://www.javascripttutorial.net/dom/



https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Manipulating_documents

https://www.freecodecamp.org/news/learn-how-to-manipulate-the-dom-by-building-a-simple-javascript-color-game-1a3aec1d109a/

*/

/* References: 

https://www.sitepoint.com/dom-manipulation-vanilla-javascript-no-jquery/
https://github.com/component/dom
https://github.com/R1V3R5/Dom-Manipulation


https://github.com/neon010/Simple-javascript-DOM-manipulation-projects

108 Common DOM Tasks in Vanilla JS: the HTML DOM Project:
https://www.infoq.com/news/2020/04/howto-html-dom-vanilla-js/


https://dev.to/desoga/7-javascript-methods-that-aids-dom-manipulation-kkj


*/


/* represents a DOM- document object model, 
a web standard for API specification 
(eg. XML - extensive markup language, 
that uses costum TAGs like <name> <address>)*/

/*
terminal commands:

mkdir dom-manipulation

touch dom-manipulation/index.js
touch dom-manipulation/index.html

nvim dom-manipulation
live-server dom-manipulation/

-> serving dom-manipulation




browser console /chrome: 

console.log(document)
console.dir(document)
-> prints an object of the document element, check for specifications of the API (button, click, action, etc)

direct dom manipulation is now more used than jquery



*/