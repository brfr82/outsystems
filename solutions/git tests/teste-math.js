
var x = -4;
var y = 2;  

function xPoweredToY(x, y) {

    if (Number.isInteger(y)) {

        var result = 1; //   x ^ 0  = 1

        if (y > 0) {

            result = x;

            for (var i = 1; i < y; i++) {

                result = result * x;
            }
        }

        else if (y < 0) {
            
            for (var i = 0; i < Math.abs(y); i++) {

                result = result / x;
            }
        }

        return result;
    }

    else { return 'y is not an integer, check Math.pow result'; }
}

function calculatePowComplexNumbers(x, y) {
    var result = Math.pow(x, y);

    if (x > 0) {
        return result;
    }
    else {
        return -1 * Math.pow(-x, y);
    }
}

console.log(x, '^', y, '=', xPoweredToY(x, y));
console.log(calculatePowComplexNumbers(x,y));

