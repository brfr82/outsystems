import { infiniteScroll, goToSearch} from "../view/view.js";

function endOfSearch() {   

    window.removeEventListener("scroll", infiniteScroll);

    const scrollTop = "/search.html"    

    let container = document.createElement('ul');
    container.setAttribute('id', 'box');
    let node_2 = document.createElement('li');
    container.appendChild(node_2);

    let node_3 = document.createElement('a');
    node_3.setAttribute('href', scrollTop);
    root.appendChild(node_3);

    node_3.addEventListener('click', function (event) {
        event.preventDefault();
        goToSearch();      
        
    });

    let node_4 = document.createElement('u');
    node_4.textContent = "New Search"
    node_3.appendChild(node_4);

    let node_5 = document.createElement('h1');
    node_5.textContent = "No [more] Search Results"
    container.appendChild(node_5);
    root.appendChild(container);

}

export { endOfSearch };