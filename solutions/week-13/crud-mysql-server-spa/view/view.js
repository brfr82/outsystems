import { list, listSearch } from "../controller/list.js";
import { loadSearchForm } from "../form/form-search.js";
import { loadContactForm } from "../form/form-contact.js";
import { gridView } from "./view-grid.js";
import { characterPage } from "./view-detail.js";

let apiPage = 0;

export default function listView(item) {

    const view = (window.location.href.split("/")[3]).split("_")[0]
    console.log(view, "page", apiPage);

    // navigation -> send to navigation
    if (apiPage === 0) { //page=1 prevents multiple events
        const homePage = document.getElementById("home-page");
        homePage.addEventListener('click', function () {
            root.textContent = ""
            apiPage = 1;
            history.pushState('home', `home page`, "home.html")
            window.location.href = "home.html"
            listView(item);
        });

        const contact = document.getElementById("add-character");
        contact.addEventListener('click', function () {
            history.pushState('add character', `add character`, "add-character.html")
            root.textContent = "Work in progress";
            loadContactForm();
        });


        const searchChar = document.getElementById("search-page");
        searchChar.addEventListener('click', goToSearch);
    }


    // routing -> send to routes
    const charName = document.getElementById("name"); // if not null -> enables gridview in search

    if (view > 0) {
        characterPage(view - 1);
    }

    else if (view === "search.html" && charName == null) {
        goToSearch();

    }
    else if (view === "add-character.html") {
        loadContactForm();
    }

    else {

        const root = document.getElementById("root");
        let container = document.createElement('section');
        let node_div = document.createElement('div');
        node_div.appendChild(gridView(item));
        container.appendChild(node_div);
        root.appendChild(container);
    }

    window.addEventListener("scroll", infiniteScroll)

    const a = document.getElementsByTagName("a");

    for (let i = 0; i < a.length; i++) {

        a[i].addEventListener('click', (event) => {

            event.preventDefault();

            /* if (event.target.id == "") { // error because of a[i]
                apiPage = 1;
                history.pushState('home', `home page`, "index.html")

                root.textContent = ""
                root.appendChild(container)
            }
            else { 
                console.log(a[i].href)
                history.pushState('detail', `Details from ${a[i].href}`, a[i].href); //regex ->character name
                root.textContent = ""
                characterPage(event.target.id - 1);
            } */


            for (let j = 0; j < (a.length / 2); j++) {

                if (item[j].id == event.target.id) {


                    history.pushState(item[j].id, `Details from ${item[j].name}`, a[i].href)
                    window.location.href = a[i].href;
                    root.textContent = ""
                    characterPage(event.target.id - 1);
                }
                else if (event.target.id == "") {
                    apiPage = 1;
                    history.pushState(item[j].id, `Details from ${item[j].name}`, "index.html")

                    //window.location.ref = "index.html"
                    root.textContent = ""
                    root.appendChild(container)
                }
            }
        });
    }
}

window.addEventListener("scroll", infiniteScroll)
function infiniteScroll() {

    if ((window.innerHeight * 1.3 + window.pageYOffset) >= document.body.offsetHeight) {

        apiPage = apiPage + 1;
        const charLocation= document.getElementById("location");
        console.log(charLocation);

        window.removeEventListener("scroll", infiniteScroll)


        if (charLocation) {
            getSearch(apiPage);
        }

        else if (apiPage < 35) {
            
            list(apiPage);
        }


    }
}

function getSearch(apiPage) {

    const charName = document.getElementById("name");
    const charSpecies = document.querySelector('input[name="species"]:checked')
    const charGender = document.querySelector('input[name="gender"]:checked')
    const charStatus = document.querySelector('input[name="status"]:checked')

    // checkboxes // unavailable feature in API --> internal implemention: call API multiple times
    /* var checkboxes = document.getElementsByName('species');
    for (var checkbox of checkboxes) {
      if (checkbox.checked)
        //console.log(checkbox.value)
        charSpecies = charSpecies + ',' + checkbox.value.toString();
    } */

    //const el_31 = document.createElement('div');
    //el_31.textContent = "SEARCH RESULTS - loading setimetout image here"
    /*const API_SEARCH = `${apiPage}&name=${charName.value}&gender=${charGender.value}
                        &species=${charSpecies.value}&status=${charStatus.value}`
    console.log(API_SEARCH);*/ // for the API only

    listSearch(apiPage); // API_SEARCH (API)

}

function goToSearch() {
    history.pushState('search', `search page`, "search.html")
    root.textContent = ""
    apiPage = 1
    loadSearchForm();
}

export { goToSearch, getSearch, infiniteScroll, listView }




