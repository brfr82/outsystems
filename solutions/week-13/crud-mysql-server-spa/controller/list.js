import { getItemList } from "../service/api.js" // folder in /controller
import listView from "../view/view.js";



async function list(apiPage) {
    // loading view , until fulfilled

    console.log("list page", apiPage);

    if (apiPage == undefined) { apiPage = 0 };
    

    let characterIdStart = (apiPage) * 20 + 1
    let characterIdEnd = characterIdStart + 20
    console.log(apiPage + "---" + characterIdStart + "---" + characterIdEnd);


    apiPage = ""; //correction, if undefined

     

    try {
        let API_ENDPOINT = "service/characters.json"
   
        const data = await getItemList(API_ENDPOINT);
        console.log(data);


        const dataFiltered = data
            .filter(function (el, i) {
                if (i >= characterIdStart && i < characterIdEnd) { // scroll 20 characters only based on apiPage
                    return { id: el.id, name: el.name, image: el.image, location: el.from }
                }
            })
            .map(function (el) {
                return { id: el.id, name: el.name, image: el.image, location: el.from }
            })

        console.log(dataFiltered);


        listView(dataFiltered);


    } catch (err) {
        console.log(err.message);
        return err.message
    }
}



async function listSearch(apiPage) {
    // loading view , until fulfilled

    console.log("list page", apiPage);

    if (apiPage == undefined) { apiPage = 0 };
    

    let characterIdStart = (apiPage) * 20 - 20;
    let characterIdEnd = characterIdStart + 20
    console.log(apiPage + "---" + characterIdStart + "---" + characterIdEnd);


    apiPage = ""; //correction, if undefined

     

    try {
        let API_ENDPOINT = "service/search-characters.json"
   
        const data = await getItemList(API_ENDPOINT);
        console.log(data);


        const dataFiltered = data
            .filter(function (el, i) {
                if (i >= characterIdStart && i < characterIdEnd) { // scroll 20 characters only based on apiPage
                    return { id: el.id, name: el.name, image: el.image, location: el.from }
                }
            })
            .map(function (el) {
                return { id: el.id, name: el.name, image: el.image, location: el.from }
            })

        console.log(dataFiltered);


        listView(dataFiltered); 


    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

async function detail() {
    // loading view , until fulfilled
    try {
        const API_ENDPOINT = "service/character.json";

        const [data] = await getItemList(API_ENDPOINT);
        console.log(data);


        return data;

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}


async function location(apiPage) {
    // loading view , until fulfilled
    try {
        const API_ENDPOINT = "https://rickandmortyapi.com/api/location/?page="; // location is a variable...-> single function

        const data = await getItemList(API_ENDPOINT, apiPage);

        const dataObj = JSON.parse(JSON.stringify(data, ['results', 'name']));
        const dataFiltered = dataObj.results
            .map(function (el) {
                return { name: el.name }
            })
            .sort(function (a, b) {
                var nameA = a.name.toUpperCase(); //  - https://developer.mozilla.org/
                var nameB = b.name.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal - https://developer.mozilla.org/
                return 0;
            });

        return dataFiltered;

    } catch (err) {
        console.log(err.message);
        return err.message
    }
}

export { list, listSearch, detail, location };