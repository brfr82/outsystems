import { infiniteScroll } from "../view/view.js";
//ToDo...
function loadContactForm() {


    window.removeEventListener("scroll", infiniteScroll)


    const root = document.getElementById("root");
    root.textContent = ""



    const container = document.createElement('form');
    container.setAttribute('action', 'http://localhost:8088/');
    container.setAttribute('method', 'GET');
    container.setAttribute('id', 'form');

    const el_1 = document.createElement('LABEL');
    el_1.setAttribute('for', 'name');
    el_1.textContent = "Name:"
    container.appendChild(el_1);
    
    const el_2 = document.createElement('BR');
    container.appendChild(el_2);
    
    const el_3 = document.createElement('INPUT');
    el_3.setAttribute('name', 'name');
    el_3.setAttribute('type', 'text');
    el_3.setAttribute('value', '');
    el_3.setAttribute('required', '');
    el_3.setAttribute('pattern', '^[a-zA-Z0-9]*$');
    container.appendChild(el_3);
    
    const el_4 = document.createElement('BR');
    container.appendChild(el_4);
    
    const el_5 = document.createElement('LABEL');
    el_5.textContent = "Status:"
    el_5.setAttribute('for', 'status');
    container.appendChild(el_5);
    
    const el_6 = document.createElement('SELECT');
    el_6.setAttribute("name", "Status");
    container.appendChild(el_6);
    
    const el_7 = document.createElement('OPTION');
    el_7.setAttribute('selected', 'selected');
    el_7.setAttribute('value', 'Alive');
    el_6.appendChild(el_7);
    
    el_7.textContent = 'Alive';
    
    const el_9 = document.createElement('OPTION');
    el_9.setAttribute('value', 'Dead');
    el_6.appendChild(el_9);
    
    el_9.textContent = 'Dead';
    
    const el_11 = document.createElement('BR');
    container.appendChild(el_11);
    
    const el_12 = document.createElement('LABEL');
    el_12.setAttribute('for', 'species');
    el_12.textContent = "Species:"
    container.appendChild(el_12);
    
    const el_13 = document.createElement('SELECT');
    el_13.setAttribute("name", "Species");
    container.appendChild(el_13);
    
    const el_14 = document.createElement('OPTION');
    el_14.setAttribute('selected', 'selected');
    el_14.setAttribute('value', 'Human');
    el_13.appendChild(el_14);
    
    el_14.textContent = "Human";
    
    const el_16 = document.createElement('OPTION');
    el_16.setAttribute('value', 'Alien');
    el_13.appendChild(el_16);
    
    el_16.textContent = "Alien";
    
    const el_18 = document.createElement('BR');
    container.appendChild(el_18);
    
    const el_19 = document.createElement('LABEL');
    el_19.setAttribute('for', 'gender');
    el_19.textContent = "Gender:"
    container.appendChild(el_19);
    
    const el_20 = document.createElement('SELECT');
    el_20.setAttribute("name", "Gender");
    container.appendChild(el_20);
    
    const el_21 = document.createElement('OPTION');
    el_21.setAttribute('selected', 'selected');
    el_21.setAttribute('value', 'Male');
    el_20.appendChild(el_21);
    
    el_21.textContent = "Male";
    
    const el_23 = document.createElement('OPTION');
    el_23.setAttribute('value', 'Female');
    el_20.appendChild(el_23);
    
    el_23.textContent = "Female";
    
    const el_25 = document.createElement('BR');
    container.appendChild(el_25);
    
    const el_26 = document.createElement('LABEL');
    el_26.setAttribute('for', 'from');
    el_26.textContent= "From:"
    container.appendChild(el_26);
    
    const el_27 = document.createElement('BR');
    container.appendChild(el_27);
    
    const el_28 = document.createElement('INPUT');
    el_28.setAttribute('name', 'from');
    el_28.setAttribute('type', 'text');
    el_28.setAttribute('value', ' ');
    container.appendChild(el_28);
    
    const el_29 = document.createElement('BR');
    container.appendChild(el_29);
    
    const el_30 = document.createElement('LABEL');
    el_30.setAttribute('for', 'lastseenon');
    el_30.textContent = "Last Seen On:"
    container.appendChild(el_30);
    
    const el_31 = document.createElement('BR');
    container.appendChild(el_31);
    
    const el_32 = document.createElement('INPUT');
    el_32.setAttribute('name', 'lastseenon');
    el_32.setAttribute('type', 'text');
    el_32.setAttribute('value', ' ');
    container.appendChild(el_32);
    
    const el_33 = document.createElement('BR');
    container.appendChild(el_33);
    
    const node_1 = document.createElement('LABEL');
    node_1.textContent = "Image (valid URL)"
    node_1.setAttribute('for', 'url');
    container.appendChild(node_1);
    const el_34 = document.createElement('BR');
    container.appendChild(el_34);

    const node_2 = document.createElement('INPUT');
    node_2.setAttribute('type', 'url');
    node_2.setAttribute('name', 'image');
    node_2.setAttribute('id', 'image');
    node_2.setAttribute('placeholder', 'https://example.com');
    node_2.setAttribute('pattern', 'https://.*');
    node_2.setAttribute('required', '');
    container.appendChild(node_2);


    const node_5 = document.createElement('BR');
    container.appendChild(node_5);

    const node_3 = document.createElement('INPUT');
    node_3.setAttribute('id', 'submit');
    node_3.setAttribute('type', 'submit');
    node_3.setAttribute('value', '                Insert Character                ');
    
    const node_4 = document.createElement("div");
    node_4.setAttribute('id', 'submitdiv');
    node_4.appendChild(node_3);
    
    container.appendChild(node_4);

    root.appendChild(container);

    //console.log(container); // check the form structure;
    container.addEventListener('submit', (event) => {
        event.preventDefault();  

        async function postAddCharacter() {
            try {

                const data = new FormData(container);

                const dataJson = Object.fromEntries(data.entries());
                // all data retrieved:  {dataJson: {…}} dataJson: Gender: "Female" Species: "Alien" Status: "Dead" from: " from jupiter" lastseenon: " alas" name: "rick" url: "https://example.com"
                console.log([dataJson][0])

                

                const response = await fetch("post-add-character.html", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify([dataJson][0])
                
                });
        
                const payload = await response.json();
                console.log (payload);
                if (!response.ok) {
                    
                    throw Error(payload.error);
                }
        
                return payload;
            } catch (err) {
                throw Error(err); // Uncaught (in promise) Error: SyntaxError: Unexpected token < in JSON at position 0
                
            }
        }
        postAddCharacter();

    });

    

}

export { loadContactForm };