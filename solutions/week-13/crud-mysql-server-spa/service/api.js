import {endOfSearch} from "../view/view-not-found.js";


async function getItemList(API_ENDPOINT, apiPage) {

    apiPage ="";    

    try {
        console.log(`${API_ENDPOINT}${apiPage}`)
        const response = await fetch(`${API_ENDPOINT}${apiPage}`)

        if (!response.ok) {
            throw new Error(window.message);
        }
        return response.json();

    } catch (err) {
        console.log(err.message);
        endOfSearch();
        return err.message
    }
}

export { getItemList }