
function layout() {

    const layout = document.getElementById("layout-root");
    layout.textContent = ""

    const container = document.createElement('div');

    const el_1 = document.createElement('NAV');
    container.appendChild(el_1);

    const el_2 = document.createElement('UL');
    el_2.setAttribute('id', 'navbar');
    el_1.appendChild(el_2);

    const el_3 = document.createElement('LI');
    el_3.setAttribute('id', 'home-page');
    el_3.textContent = "Home"
    el_2.appendChild(el_3);

    const el_4 = document.createElement('LI');
    el_4.setAttribute('id', 'search-page');
    el_4.textContent = "Advanced Search"
    el_2.appendChild(el_4);

    const el_5 = document.createElement('LI');
    el_5.setAttribute('id', 'add-character');
    el_5.textContent = "Add Character"
    el_2.appendChild(el_5);

    const el_6 = document.createElement('HEADER');
    container.appendChild(el_6);

    const el_7 = document.createElement('H1');
    const rickMortyFont = new FontFace('rick-morty-font', 'url(/font/font-rick-morty.ttf)');

    function loadFont() {
        rickMortyFont.load().then(function (loadedFont) {
            document.fonts.add(loadedFont)
            el_7.style.fontSize = "275%"
            el_7.style.fontFamily = '"rick-morty-font"';
        }).catch(function (error) {
            //console.log('Failed to load font: ' + error)
        })
    }
    loadFont()


    el_7.textContent = "Rick and Morty"
    el_6.appendChild(el_7);

    const el_9 = document.createElement('P');
    el_9.textContent = " american television series "
    el_6.appendChild(el_9);

    const el_10 = document.createElement('MAIN');
    container.appendChild(el_10);

    const el_11 = document.createElement('ASIDE');
    el_10.appendChild(el_11);

    const el_12 = document.createElement('NAV');
    el_12.setAttribute('id', 'sidebar');
    el_12.setAttribute('class', 'sticky');
    el_11.appendChild(el_12);

    const el_13 = document.createElement('UL');
    el_12.appendChild(el_13);

    const el_14 = document.createElement('LI');
    el_14.textContent = "fb"
    el_13.appendChild(el_14);

    const el_15 = document.createElement('LI');
    el_15.textContent = "tw"
    el_13.appendChild(el_15);

    const el_16 = document.createElement('LI');
    el_16.textContent = "ig"
    el_13.appendChild(el_16);

    const el_17 = document.createElement('div');
    el_17.textContent = "MAIN CONTENT - loading setimetout image here"
    el_17.setAttribute('id', 'root');
    el_10.appendChild(el_17);

    const el_18 = document.createElement('FOOTER');
    el_18.textContent = "API  https://rickandmortyapi.com (©) 2020"
    container.appendChild(el_18);

    layout.appendChild(container);
}

export { layout }