const http = require('http');
const fs = require('fs');
const PORT = 8000;
const server = http.createServer(handler)

const mysql = require("mysql2/promise");

const connection = mysql.createPool({
  host: "localhost",
  user: "newuser",
  password: "password",
  database: "rick_morty", 
  //port: "3306"
});


const distructurer = ['id', 'name', 'status', 'species', 'gender', 'from', 'lastSeenOn', 'image']

server.listen(PORT);
console.log('server listening @port' + PORT);

function handler(request, response) {

    let url = request.url;
    const idCharacter = url.split('_')[0].substring(1)
    console.log('REQUEST URL:' + request.url + '  REQUEST METHOD:' + request.method + "IDCharacter" + idCharacter);

    let contentTypeObj = { ttf: "font/ttf", json: "application/json", js: "text/javascript", png: "image/png", html: "text/html", css: "text/css" };
    let mimeType = contentTypeObj[url.split('.').pop()];
    let fileNames = fs.readdirSync('./').concat(fs.readdirSync('./view')).concat(fs.readdirSync('./form'))
        .concat(fs.readdirSync('./service')).concat(fs.readdirSync('./controller')).concat(fs.readdirSync('./font')); 

    //console.log(url, url.split('/').pop(), mimeType, fileNames, fileNames.includes(url.split('/').pop()));
    if (url === "/index.html" || url === "/home.html") {

        async function selectAllCharacters(){


            try {

                const sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters WHERE id > ? AND id < ?;`;
                const [data] = await connection.query( sql, ["0","1200"]); // >671 test characters whre inserted
                 
                const dataFiltered = JSON.parse(JSON.stringify(data, distructurer));
                //console.log(dataFiltered);
  
                fs.writeFile("service/characters.json", JSON.stringify(dataFiltered),  function (err) {
                  if (err) return console.log(err);
                  console.log('data written to file: service/characters.json ');
                });  
  
  
  
                return dataFiltered; // note use convertCharacter instead (destructuring)
              } catch(err) {
                console.log(err); // use ternary instead
                
              }

              
        }

        selectAllCharacters();


        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });
    }
    else if (fileNames.includes(url.split('/').pop())) {

        // client files


        fs.readFile(url.substring(1), function (error, content) {

            response.writeHead(200, { 'Content-Type': mimeType });
            response.write(content);
            response.end();
        });
    }
    else if (idCharacter < 672 ) { 

        console.log(idCharacter)


        async function selectSingleCharacter(id){

            try {
              const sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters WHERE id = ?;`; // protects from sql injects
              const [data] = await connection.query( sql, [id]); // protects from sql injects
              
              const dataFiltered = JSON.parse(JSON.stringify(data, distructurer));
              //console.log(dataFiltered);

              fs.writeFile("service/character.json", JSON.stringify(dataFiltered),  function (err) {
                if (err) return console.log(err);
                console.log('data written to file: service/character.json ');
              });  



              return dataFiltered[0]; // note use convertCharacter instead (destructuring)
            } catch(err) {
              console.log(err); // use ternary instead
              
            }
        
        }

        selectSingleCharacter(idCharacter);

        fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        })
    }

    else if ( url === "/post-search.html" ) { 


      let payload = ""
      request.on('data', function (chunk) { payload = payload.concat(chunk) })
      console.log("post handler invoked");
      request.on("end", function () {
  
      payload = JSON.parse(payload);

      
        
      // {"name":"rick","gender":"male","status":"alive","species":"human","location":"Citadel of Ricks"}

        // convert this payload from (POST request) to SQL query string for filtering data:
    


        async function filterCharactersSql(){


          try {
              

              const sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters
               WHERE name LIKE "%${payload.name}%" AND gender = "${payload.gender}" AND status = "${payload.status}" AND species = "${payload.species}" AND origin = "${payload.location}";`; 
               // ? protects from sql injects

               console.log(sql)
              // SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters
              //WHERE name LIKE "%Rick%" AND gender LIKE "%Male%" AND status LIKE "%Alive%" AND species LIKE "%Human%" AND origin LIKE "%Earth (C-137)%";

              const [data] = await connection.query( sql); // protects from sql injects
              
              const dataFiltered = JSON.parse(JSON.stringify(data, distructurer));
              //console.log(dataFiltered);

              fs.writeFile("service/search-characters.json", JSON.stringify(dataFiltered),  function (err) {
                if (err) return console.log(err);
                console.log('data written to file: /service/search-characters.json ');
              });  

              

              return dataFiltered[0]; // note use convertCharacter instead (destructuring)
            } catch(err) {
              console.log(err); // use ternary instead
              
            }

      }

      filterCharactersSql();

        response.end();
      })



        /*fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        })*/
    }
    else if ( url === "/post-add-character.html") { 

      let payload = ""
      request.on('data', function (chunk) { payload = payload.concat(chunk) })
      console.log("post handler invoked");
      request.on("end", function () {
             
      // {"name":"rick","Status":"Alive","Species":"Human","Gender":"Male","from":" from jupiter","lastseenon":" alas","url":"https://example.com"}

        // convert this data from (POST request) to SQL query string:
        let row = JSON.parse(payload);
        console.log(row);
        
        row = `"${row.name}","${row.Status}","${row.Species}","${row.Gender}",
        "${row.from}","${row.lastseenon}","${row.image}"`
        console.log(row)

        async function insertCharacter(row){

          try {
            const sql = `INSERT INTO characters(name, status, species, gender, origin, lastSeenOn, image) values(${row})`;  
            console.log(sql);
            const [data] = await connection.query( sql);  
            console.log('data written to database');
          } catch(err) {
            console.log(err); 
            
          }

        }

        insertCharacter(row);

        response.end();
      })


        /*fs.readFile("index.html", function (error, content) {

            response.writeHead(200, { 'Content-Type': "text/html" });
            response.write(content);
            response.end();
        })*/
    }

      // UPDATE (in test phase)


  else if (idCharacter < 672 && "/post-update-character.html") {  // create this button and payload in CLIENT

    console.log(idCharacter)

    let payload = ""
    request.on('data', function (chunk) { payload = payload.concat(chunk) })
    console.log("post handler invoked");
    request.on("end", function () {

      // to be console.logged: {"name":"rick","Status":"Alive","Species":"Human","Gender":"Male","from":" from jupiter","lastseenon":" alas","url":"https://example.com"}

      // convert this data from (POST request) to SQL query string:
      let row = JSON.parse(payload);
      console.log(row);

      // important note: NOT all attributes are currently in the detail view of the client layout


      async function updateCharacter(row, id) {

        try {
          const sql = `UPDATE characters SET name="?", status="?", species="?", gender="?", origin="?", lastSeenOn="?", image WHERE id = ?`; // ?,?, ... protects from sql injects
          const [data] = await connection.query(sql, [row.name, row.Status, row.Species, row.Gender, row.from, row.lastseenon, row.image, id]); // protects from sql injects

          const dataFiltered = JSON.parse(JSON.stringify(data, distructurer));
          //console.log(dataFiltered);

          fs.writeFile("service/character.json", JSON.stringify(dataFiltered), function (err) {
            if (err) return console.log(err);
            console.log('data written to file: service/character.json ');
          });



          return dataFiltered[0]; // note use convertCharacter instead (destructuring)
        } catch (err) {
          console.log(err); // use ternary instead

        }

      }

      updateCharacter(idCharacter); // then reload view of id using id returned from this function
      response.end();
    })

    /*fs.readFile("index.html", function (error, content) {

      response.writeHead(200, { 'Content-Type': "text/html" });
      response.write(content);
      response.end();
    })*/
  }


    else {
        response.writeHead(404);
        response.write('page unavailable'); // Create view for 404 unavailable
        response.end();
    }
}









