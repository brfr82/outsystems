const url = require('url');

const { list } = require('../../model/characters');



/**
 * Lists all characters as JSON
 *
 * @param {http.IncomingMessage} request client's request
 * @param {http.ServerResponse} response server's response helper object
 */
async function handler(request, response) {
	
    const queryObject = url.parse(request.url, true).query;
console.log(queryObject);
console.log(list(queryObject)); // Promise { <pending> }

    const json = await JSON.stringify(list(queryObject));
console.log(json); // {}
    const headers = { 'Content-Type': 'application/json' };

    response.writeHead(200, headers);
    response.write(json);
    response.end();
}

module.exports = handler;
