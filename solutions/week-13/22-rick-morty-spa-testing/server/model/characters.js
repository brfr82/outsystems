
const mysql = require("mysql2/promise");


const connection = mysql.createPool({
  host: "localhost",
  user: "newuser",
  password: "password",
  database: "rick_morty", 
  //port: "3306"
});


const distructurer = ['id', 'name', 'status', 'species', 'gender', 'from', 'lastSeenOn', 'image']



/**
 * Adds a character
 *
 * @param {object} character new character data
 * @returns {number} the new character id
 */
function add(character) {
    const nextId = characters.length + 1;
    characters.push({ ...character, id: nextId });
    return nextId;
}

/**
 * Gets a character by its ID
 * Returns null if not found
 *
 * @param {number} id character's id
 * @returns {?object} the character
 */
async function get(id) {

    try {
      const sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters WHERE id = ?;`; // protects from sql injects
      const [data] = await connection.query( sql, [id]); // protects from sql injects
      
      const dataFiltered = JSON.parse(JSON.stringify(data, distructurer));
      console.log(dataFiltered);
      return dataFiltered[0]; // note use convertCharacter instead (destructuring)
    } catch(err) {
      console.log(err); // use ternary instead
      
    }

}

/**
 * Lists all characters
 *
 * @param {object} [filter] optional filters
 * @returns {Array<object>} all characters
 */



async function list(filter = {}) {

      const sql = `SELECT id, name, status, species, gender, origin AS 'from', lastSeenOn, image FROM characters WHERE id >= ? AND id <= ?;`;
      const [data] = await connection.query( sql, [1, 20]); 
      
      const dataFiltered = JSON.parse(JSON.stringify(data, distructurer));

      const characters = dataFiltered

      const { name } = filter;
    

    if (name) {
	
        return characters.filter((char) =>
            char.name.match(new RegExp(name, 'i'))
        );
    }
    console.log(characters);

    return characters;


}

module.exports = {
    add,
    get,
    list
};
