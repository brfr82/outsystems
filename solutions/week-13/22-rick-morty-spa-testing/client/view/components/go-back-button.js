import { goTo } from '../../navigation.js';
import { ROUTES } from '../../routes.js';
import quickActionButton from './quick-action-button.js';

function goBackButton() {
    const icon = document.createElement('i');
    icon.className = 'fas fa-angle-left';
    const button = quickActionButton('', function () {
        goTo(ROUTES.CHARACTER_LIST);
    });

    button.appendChild(icon);
    return button;
}

export default goBackButton;
