import { getQueryString, goTo } from '../../navigation.js';
import container from './container.js';
import icon from './icon.js';

function searchBar() {
    const query = getQueryString();
    const match = query.match(/\?name=(\w+)/);

    const wrapper = container();
    wrapper.className = 'flex center';
    wrapper.id = 'search-bar';

    const input = document.createElement('input');
    input.type = 'search';
    input.placeholder = 'Search name';
    input.autocomplete = 'off';
    input.className = 'field';
    input.style.width = '260px';
    input.style.height = '50px';

    if (match) {
        const name = match[1];
        input.value = name;
    }

    const clear = icon('fas fa-times');
    clear.style.position = 'relative';
    clear.style.right = '20px';

    wrapper.appendChild(input);
    wrapper.appendChild(clear);

    input.addEventListener('change', function (event) {
        goTo(`/?name=${event.target.value}`);
    });

    clear.addEventListener('click', function () {
        goTo('/');
    });

    return wrapper;
}

export default searchBar;
