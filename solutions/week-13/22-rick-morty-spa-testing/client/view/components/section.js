function section(className) {
    const sectionEl = document.createElement('section');
    sectionEl.className = className;

    return sectionEl;
}

export default section;
