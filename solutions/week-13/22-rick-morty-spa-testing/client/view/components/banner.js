import image from './image.js';
import section from './section.js';

function banner(imagePath, title = '', logo) {
    const sectionEl = section('banner');
    sectionEl.style.backgroundImage = `url(${imagePath})`;
    sectionEl.appendChild(createTitle(title));

    if (logo) {
        const logoEl = image(logo, title);
        logoEl.className = 'logo';
        sectionEl.appendChild(logoEl);
    }

    return sectionEl;
}

function createTitle(text) {
    const element = document.createElement('h1');
    element.className = 'title';
    element.innerText = text;

    return element;
}

export default banner;
