import createImage from './image.js';

function card(id, image, text, onClick) {
    const cardWrapper = document.createElement('div');
    cardWrapper.className = 'card';

    cardWrapper.appendChild(createImage(image, text));
    cardWrapper.appendChild(footer(text));
    cardWrapper.addEventListener('click', onClick);

    return cardWrapper;
}

function footer(content) {
    const footerEl = document.createElement('div');
    footerEl.className = 'footer';
    const textEl = document.createElement('h3');
    textEl.textContent = content;
    textEl.className = 'content';

    footerEl.appendChild(textEl);

    return footerEl;
}

export default card;
