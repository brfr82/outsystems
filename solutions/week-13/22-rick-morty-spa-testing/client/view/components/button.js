function button(classes = '', content = '', onClick) {
    const button = document.createElement('button');
    button.className = `button ${classes}`;
    const text = document.createElement('span');
    text.innerText = content;

    if (onClick) {
        button.addEventListener('click', onClick);
    }

    button.appendChild(text);

    return button;
}

export default button;
