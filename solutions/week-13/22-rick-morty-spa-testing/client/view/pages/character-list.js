import banner from '../components/banner.js';
import grid from '../components/item-grid.js';
import { goTo } from '../../navigation.js';
import { ROUTES } from '../../routes.js';
import card from '../components/card.js';
import container from '../components/container.js';
import searchBar from '../components/searchbar.js';

const TITLE = 'Rick and Morty';
const BANNER_IMG = '../../assets/sunset.png';



export default function (rootEl, characters) {
    const bannerEl = banner(BANNER_IMG, TITLE);
    const contentEl = container('content');

    contentEl.appendChild(searchBar());

console.log(characters);
    contentEl.appendChild(grid(characters.map(toCard)));

    rootEl.appendChild(bannerEl);
    rootEl.appendChild(contentEl);
}


function toCard(character) {
    const { id, image, name } = character;

    return card(id, image, name, function () {
        goTo(`${ROUTES.DETAILS}/${character.id}`);
    });
}
