import { goTo } from './navigation.js';

goTo(document.location.pathname, true);

window.onpopstate = function (event) {
    goTo(document.location.pathname, true, event.state);
};
