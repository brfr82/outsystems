import { getCharacter } from '../service/api.js';
import error from '../view/pages/error.js';
import characterDetails from '../view/pages/character-details.js';
import loading from '../view/pages/loading.js';

export default async function (rootEl, data) {
    const id = data.id;

    try {
        loading(rootEl);
        const data = await getCharacter(id);
        rootEl.innerHTML = '';
        characterDetails(rootEl, data);
    } catch (err) {
        error(rootEl, err.message);
    }
}
