const API_URL = 'http://localhost:8000/api';

const NOT_FOUND_ERROR = 'not found';

export async function getCharacterList(params = '') {
    return get(`${API_URL}/character${params}`);
}

export function getCharacter(id) {
    try {
        return get(`${API_URL}/character/${id}`);
    } catch ({ responseJSON }) {
        throw Error(responseJSON.error);
    }
}

async function get(url) {
    const response = await fetch(url);

    if (!response.ok) {
        throw Error(NOT_FOUND_ERROR);
    }

    const json = await response.json();
    return json;
}
